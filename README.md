HTP SurflexDock 0.6

HTP SurflexDock is a web tool that allows performing Structure Base Virtual Screening experiments using the ensemble docking protocol. Beside that, HTP SurflexDock allows a full qualitative analisys of ranked compounds by analysis of the interactions of promising compounds with receptor and so filtering those compounds that will lead to future drug discovery stages.

Instalation instructions:
If you are interested in installing HTP SurflexDock, please contact joaoluiz.af@gmail.com