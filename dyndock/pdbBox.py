from Bio.PDB import PDBParser
import sys

def main():
	input_pdb = sys.argv[1]
	chain = sys.argv[3]
	if chain == "":
		chain = " "
	residue_number = int(sys.argv[2])
	size = int(sys.argv[4])

	pdbBox = PDB(input_pdb)
	pdbBox.set_box_dimension(residue_number,chain, size )
	# print " ".join(pdbBox.get_list_of_residues_into_box())
	mystr = ""
	g = pdbBox.get_list_of_residues_into_box()
	for eachRes in g:
		mystr = mystr + str(eachRes) + " "
	print mystr
class PDB(object):
	"""docstring for PDB"""
	def __init__(self, pdbfile):
		super(PDB, self).__init__()
		self.pdbfile = pdbfile
		self.x0 = 0
		self.x1 = 0
		self.y0 = 0
		self.y1 = 0
		self.z0 = 0
		self.z1 = 0

	def get_center_CA_from_residue(self, residue_number,chain):
		if chain == "":
			chain = " "
		structure = PDBParser(QUIET=True).get_structure(None, self.pdbfile)
		try:
			atom = structure[0][chain][residue_number]['CA']
		except KeyError:
			# raise e
			listOfChains = structure.get_list()[0].get_list()
			for eachChain in listOfChains:
				try:
					letterOfChain = eachChain.get_id()
					atom = structure[0][letterOfChain][residue_number]['CA']
					break
				except KeyError:
					pass	
		residueX = atom.coord[0]
		residueY = atom.coord[1]
		residueZ = atom.coord[2]


		return [residueX,residueY,residueZ]

	def set_box_dimension(self, residue_number,chain, size, spacing=1):
		centerX,centerY,centerZ = self.get_center_CA_from_residue(residue_number, chain)
		self.x0 = centerX-size*spacing
		self.x1 = centerX+size*spacing
		self.y0 = centerY-size*spacing
		self.y1 = centerY+size*spacing
		self.z0 = centerZ-size*spacing
		self.z1 = centerZ+size*spacing



	def get_list_of_atoms_into_box(self):
		my_list =[]
		structure = PDBParser(QUIET=True).get_structure(None, self.pdbfile)

		for atom in structure.get_atoms():
			x = atom.coord[0]
			y = atom.coord[1]
			z = atom.coord[2]
			if self.x0 <= x <= self.x1 and \
			self.y0 <= y <= self.y1 and  \
			self.z0 <= z <= self.z1:
				my_list.append(atom)
		my_list.sort()
		return my_list


	def get_list_of_residues_into_box(self):
		list_of_residues = []
		for atom in self.get_list_of_atoms_into_box():
			list_of_residues.append((atom.get_parent().get_id()[1]))
		list_of_residues.sort()
		z = set(list_of_residues)
		ordened_list_of_residues = []
		for eachResidue in z:
			ordened_list_of_residues.append((eachResidue))

	
		return ordened_list_of_residues
		# print ordened_list_of_residues

def get_center():
	input_pdb = sys.argv[1]
	chain = sys.argv[3]
	if chain == "":
		chain = " "
	residue_number = int(sys.argv[2])
	structure = PDBParser(QUIET=True).get_structure(None, input_pdb)
	try:
		atom = structure[0][chain][residue_number]['CA']
	except KeyError:
		# raise e
		listOfChains = structure.get_list()[0].get_list()
		for eachChain in listOfChains:
			try:
				letterOfChain = eachChain.get_id()
				atom = structure[0][letterOfChain][residue_number]['CA']
				break
			except KeyError:
				pass

	
	residueX = atom.coord[0]
	residueY = atom.coord[1]
	residueZ = atom.coord[2]


	return [residueX,residueY,residueZ]

if __name__ == '__main__':
		main()
