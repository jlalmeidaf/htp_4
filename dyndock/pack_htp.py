import os, shutil, tarfile

here_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
temp_path = "/tmp" + os.sep + here_path.split(os.sep)[-1] 
# temp_path = "/tmp/htp_3"
blacklist_of_prefix_in_settings = ['MGLTOOLS', 'AUTODOCKFOLDER', 'TEMPORARYFOLDER', 'ALLOWED_HOSTS', 
'DEBUG', 'EMAIL_BACKEND', 'DEFAULT_FROM_EMAIL', 'EMAIL_USE_TLS', 'EMAIL_HOST', 'EMAIL_HOST_USER', 
'EMAIL_HOST_PASSWORD', 'EMAIL_PORT', 'ACPYPEFOLDER', 'NT_CPU', 'NB', 'USE_EMAIL_BACKEND', 'SHOW_VIEW_EXAMPLE', 'EXAMPLE_RESULT_LINK' ]
def prepare_settings():
	settings_path = temp_path + os.sep + "dyndock" + os.sep + "dyndock" + os.sep + "settings.py"
	old_settings_path =  settings_path + ".old" 
	shutil.move(settings_path, old_settings_path)
	settings_path_old_file = file(old_settings_path, 'r')
	lines = settings_path_old_file.readlines()
	settings_path_old_file.close()
	new_settings_file = file(settings_path, "w")
	for eachLine in lines:
		prefix = eachLine.split(" ")[0]
		if not prefix in blacklist_of_prefix_in_settings:
			new_settings_file.write(eachLine)

	new_settings_file.write("ALLOWED_HOSTS = ['127.0.0.1']\n")
	new_settings_file.write("DEBUG = True\n")
	new_settings_file.write("SHOW_VIEW_EXAMPLE = False\n")
	new_settings_file.write("EXAMPLE_RESULT_LINK = 'static/HTP_SurflexDock_basic_Tutorial.pdf'\n")
	new_settings_file.close()

	os.remove(old_settings_path)

def generate_run():
	pass

def remove_pycs():
	pass

def copy_folder():
	
	
	shutil.copytree(here_path, temp_path)

def compress():
	with tarfile.open(here_path + os.sep + "htp_4.tar.gz", "w:gz") as tar:
		tar.add(temp_path, arcname=os.path.basename(temp_path))

def copy_compress_file():
	pass

def remove_folder():
	shutil.rmtree(temp_path)

def remove_unused_parts():
	shutil.rmtree(temp_path + os.sep + ".git")
	os.remove(temp_path + os.sep + ".gitignore")
	dyndock_engine_test_folders = ['control_env']

	for eachFolder in dyndock_engine_test_folders:
		shutil.rmtree(temp_path + os.sep + 'dyndock' + os.sep + 'dyndock_engine' + os.sep + eachFolder)

	os.remove(temp_path + os.sep + 'dyndock' + os.sep + 'pack_htp.py')

def pack():
	copy_folder()
	remove_unused_parts()
	prepare_settings()	
	generate_run()
	remove_pycs()
	compress()
	remove_folder()


if __name__ == '__main__':
	pack()
