
args = commandArgs(trailingOnly=TRUE)
MyData <- read.csv(args[1], header=TRUE, sep="," ,check.names=FALSE)
png(filename=paste(args[1],"boxplot.png"))
boxplot(MyData,data=MyData, log = "y", range = 0, xlab="Cluster", ylab="Ki (molar)")
dev.off()
