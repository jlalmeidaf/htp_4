# -*- coding: utf-8 -*-

from django import forms

from multiupload.fields import MultiFileField
from .models import LigandFile as Process
from dyndock import settings
COLOR_CHOICES = (
    ('10','10'), #2500000 
    ('20', '20'), #25000000
    ('30','30'), #25000000
)
CLUSTER_CHOICES = (
    ('0.10','0.10'), #2500000 
    ('0.15', '0.15'), #25000000
    ('0.20','0.20'), #25000000
)


class FakeDockingForm(forms.Form):
    if settings.USE_EMAIL_BACKEND:
     email = forms.EmailField(
        label='Your Email (to send the results)',
        # max_length=1,
        required = True,
    )
    projectName = forms.CharField(label="Project Name",
        required = True,)

    structureFile = forms.CharField(
        label='Receptor (in .pdb format)',
)

    workdir = forms.CharField(label="Workdir",
        required = True,)

    # URLfiled = forms.URLField(
    #     label='Select a structure file (*.pdb)',
        

    # )
    residue_number = forms.DecimalField(
        label="Residue number (Center of the active site) (a)",
        required = True,
        # initial = "100"    
        
    )
    chain = forms.CharField(
        label='Residue Chain (b)',
        max_length=1,
        required = False,
        initial = "A"
    )

    size = forms.DecimalField(
        label='Size of the active site (in nm): (r)',
        required = True,
        initial = "1",
        # tip = "sfds"
    )
    ligandFile = forms.CharField( label='Ligand library in .tar.gz or .zip extensions',
    )

    cluster_cutoff = forms.ChoiceField(
        label="Cutoff for clustering (in nm)",
        # required = True,
        initial = '0.10',
        choices = CLUSTER_CHOICES
        ) 

class DockingForm(forms.Form):
    if settings.USE_EMAIL_BACKEND:
     email = forms.EmailField(
        label='Your Email (to send the results)',
        # max_length=1,
        required = True,
    )
    projectName = forms.CharField(label="Project Name",
        required = True,)

    structureFile = forms.FileField(
        label='Receptor (in .pdb format)',


    )
    # URLfiled = forms.URLField(
    #     label='Select a structure file (*.pdb)',
        

    # )
    residue_number = forms.DecimalField(
        label="Residue number (Center of the active site) (a)",
        required = True,
        # initial = "100"    
        
    )
    chain = forms.CharField(
    	label='Residue Chain (b)',
    	max_length=1,
    	required = False,
        initial = "A"
    )

    size = forms.DecimalField(
    	label='Size of the active site (in nm): (r)',
    	required = True,
    	initial = "1",
        # tip = "sfds"
    )
    ligandFile = MultiFileField(min_num=1, max_num=10, max_file_size=1024*1024*5, 
        label='Ligand library in .tar.gz or .zip extensions'
    )
    # type_docking = forms.ChoiceField(
    #     label="Number of docking calculations for each ligand",
    #     # required = True,
    #     initial = '10',
    #     choices = COLOR_CHOICES
    #     )
    cluster_cutoff = forms.ChoiceField(
        label="Cutoff for clustering (in nm)",
        # required = True,
        initial = '0.10',
        choices = CLUSTER_CHOICES
        ) 

class DockingFormStage2(forms.Form):
    # email = forms.EmailField(
    #     label='Enter with your E-mail',
    #     # max_length=1,
    #     required = True,
    # )
    # projectName = forms.CharField(label="Enter with project name",
    #     required = True,)

    # structureFile = forms.FileField(
    #     label='Select a structure file (*.pdb)'
    # )
    # residue_number = forms.DecimalField(
    #     label='Enter with center residue number',
    #     required = True,
    #     # initial = "100"    
        
    # )
    # chain = forms.CharField(
    #     label='Enter with chain of center residue',
    #     max_length=1,
    #     required = False,
    #     initial = "A"
    # )

    # size = forms.DecimalField(
    #     label='Enter with size of box',
    #     required = True,
    #     initial = "100"
    # )
    # ligandFile = MultiFileField(min_num=1, max_num=10, max_file_size=1024*1024*5, 
    #     label='Select one or more ligand files (*.pdb - *.pdbqt)'
    # )
    type_docking = forms.ChoiceField(
        label="Number of docking calculations for each ligand",
        required = True,
        initial = '25000000',
        choices = COLOR_CHOICES
        )

# new in ver 3.11

class ConvertForm(forms.Form):
     mol2File = forms.FileField(
        label='Upload mol2 file to convert',


    )   

class FixReceptorForm(forms.Form):
     receptorFile = forms.FileField(
        label='Upload Receptor file to be fixed',

        required = True,
    )   
