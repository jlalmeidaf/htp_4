# from __future__ import absolute_import
from smosh.ModelingStep.FindTemplatesStep import FindTemplatesStep
from smosh.ModelingStep.AlignStep import AlignStep
from smosh.ModelingStep.BuildModelStep import BuildModelStep
from smosh.ModelingStep.MakeProfile import MakeProfile
from smosh.ModelingStep.LoopRefinementStep import LoopRefinementStep
from smosh.ModelingStep.EvaluateEnergy import EvaluateEnergy
from smosh.ModelingStep.EvaluateEnergyofLoopRefinementStep import EvaluateEnergyofLoopRefinementStep
from celery import shared_task

@shared_task
def findTemplates(sequence_file_name):
	findTemplatesManager = FindTemplatesStep(sequence_file_name, 'fasta')
	findTemplatesManager.execute()
	return findTemplatesManager

@shared_task
def alig(unaligned_sequence):
    alignment_manager = AlignStep(unaligned_sequence)
    alignment_manager.execute()
    return alignment_manager

@shared_task
def buildModel(alignment_path, downloaded_pdb_path):
	modeling_manager = BuildModelStep(alignment_path, downloaded_pdb_path)
	modeling_manager.execute()
	return modeling_manager

@shared_task
def buildProfile(structure):
	profile_manager = MakeProfile(structure)
	profile_manager.execute()
	return profile_manager
@shared_task
def evaluateEnergy(alignment_path, profile2, profile1):
	evaluate_energy_manager = EvaluateEnergy(alignment_path,
		profile2,
		 profile1)
	evaluate_energy_manager.execute()
	return evaluate_energy_manager

@shared_task
def loopRefStep(better_result, start_residue, end_residue):
	loop_refinement_step = LoopRefinementStep(better_result, start_residue, end_residue)
	loop_refinement_step.execute()
	return loop_refinement_step

@shared_task
def evalEnergyofLoopRef(alignment_path, profile2, profile1, profile3):
	evaluate_energy_manager = EvaluateEnergyofLoopRefinementStep(alignment_path,
		profile2,
		 profile1, profile3)
	evaluate_energy_manager.execute()
	return evaluate_energy_manager