# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import subprocess #, psutil

from django.shortcuts import render, get_object_or_404
from .forms import FixReceptorForm, FakeDockingForm
import  shutil #, tempfile
from django_q.tasks import async, result
# from django_q.tasks import Async
# from .hooks import sleep_task, sleep_task2, print_task, sleep_task3, sleep_task4
from .models import Process, LigandFile, ActiveLog
from django.http import  HttpResponse, FileResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
# from django.utils import timezone
# from django_q.monitor import Stat

#############
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, inch, landscape
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle,  Paragraph, NextPageTemplate, PageTemplate, Frame
from reportlab.lib.styles import getSampleStyleSheet
#############

import os, sys, math
import zipfile
import uuid

# from django.core.mail import send_mail
# from wsgiref.util import FileWrapper
import dyndock.settings as settings
# Create your views here.

import django_tables2 as tables
from django_tables2.utils import A 
from django_tables2 import RequestConfig

# import random
# from django.contrib.sites.models import Site
from PIL import Image
# import datetime
from dyndock import settings
os.environ['MGLTOOLS'] = settings.MGLTOOLS
# os.environ['MGLTOOLS'] = "/home/joao/MGLTools-1.5.6/"

sys.path.append(os.environ['MGLTOOLS'] + '/MGLToolsPckgs/')
from AutoDockTools.DlgParser import DlgParser

TEMPORARYFOLDER=settings.TEMPORARYFOLDER
# TEMPORARYFOLDER="/mnt/sda5/htp_temp"

from .ExperimentController import FixReceptorTask



def manual_task(request):
	if request.method == 'POST':
		form = FakeDockingForm(request.POST, request.FILES)
		if form.is_valid():
				myemail = "noemail@noemail.com"
				projectName = form.cleaned_data['projectName']
				filename = form.cleaned_data['structureFile']
				type_docking = "10"
				chainData = form.cleaned_data.get('chain')
				workdir = TEMPORARYFOLDER + os.sep + form.cleaned_data['workdir']
				residue_number = int(form.cleaned_data['residue_number'])
				size = float(form.cleaned_data['size'])	
				cluster_cutoff = form.cleaned_data['cluster_cutoff']	
				task_id = str(uuid.uuid4()).replace("-","")
				process = Process.objects.create(name= task_id,
					alive = True,
					folder = workdir,
					protein = filename,
					chain = chainData,
					residue_number = residue_number,
					type_docking = type_docking,
					size = size,
					fail = False,
					user_email = myemail,
					projectName = projectName,
					cluster_cutoff = cluster_cutoff,
					site=request.META['HTTP_HOST'] + reverse("experiment_detail",  kwargs={'name': task_id}))

					# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
				process.publish()
				url = "http://" + request.META['HTTP_HOST'] + reverse("task_detail",  kwargs={'name': task_id})
				return HttpResponseRedirect(url)
	else:
			form = FakeDockingForm() 

			return render(
				request,
				'dyna/manual_task.html',
				{'form': form, 'use_email_backend' : settings.USE_EMAIL_BACKEND, 'example_result_link' :  settings.EXAMPLE_RESULT_LINK, 
				'show_view_example' : settings.SHOW_VIEW_EXAMPLE}
		)


def stop(request, name):
	post = get_object_or_404(Process, name=name)
	post.alive = True	
	post.save()
	workdir = post.folder

	stop_file = file(workdir + os.sep + 'venon.txt', 'w')
	stop_file.close()
	return HttpResponse('Stopping task ' + name)



def download_table(request, name):
		post = get_object_or_404(Process, name=name)
		list_of_compounds = request.session['mylist']
		dict_of_conformations = {}
		compounds = Compound(data=list_of_compounds, order_by='binding_energy')	

		for compound in compounds.as_values():
			if compound[0] != "Pose number":
				if compound[3] not in dict_of_conformations.keys():

					dict_of_conformations[compound[3]] = [  ["Conformation " + compound[3]], ['Compound' , '\u0394G (kcal/mol)'] ]
					
				if len(dict_of_conformations[compound[3]]) <= 21:
					compound_name =  compound[1][:-6]
					dict_of_conformations[compound[3]].append( [compound_name, compound[4]])
				else:
					pass
		# new_table = None
		# for eachCompound in dict_of_conformations:
		# 	if new_table==None:
		# 		new_table=dict_of_conformations
		# 	else:
		# 		new_table = new_table + dict_of_conformations[eachCompound]


		doc = SimpleDocTemplate(post.folder + os.sep + post.name + ".pdf", pagesize=letter)
		styleSheet = getSampleStyleSheet()
		p1 = Paragraph('''
    <para align=center spaceb=3>HTP SurflexDock results</para>''',
    styleSheet["BodyText"])
		p2 = Paragraph('''
    <para align=center spaceb=3>Top 20 Results Generated for Each Protein Conformation of ''' + os.path.basename(post.protein) + '''</para>''',
    styleSheet["BodyText"])


		data = []
		data_internal = []
		elements = []

		frame1 = Frame(doc.leftMargin, doc.bottomMargin,
		                doc.width, doc.height,
		                leftPadding = 170, rightPadding = 0,
		                topPadding = 120, bottomPadding = 0,
		                id='frame1')
		ltemplate = PageTemplate(id='landscape',frames =[frame1], onPage=make_landscape)
		doc.addPageTemplates([ltemplate])


		elements.append(NextPageTemplate('landscape'))
		elements.append(p1)
		elements.append(p2)
		for compound in dict_of_conformations:
			table_data = dict_of_conformations[compound]

			t1 = Table(table_data, 2 * [1.3 * inch], len(table_data) * [0.2 * inch])	
			t1.setStyle(TableStyle([
	('SPAN', (0, 0), ((1,0))),
    ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
    ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
]))	
			# if(len(data_internal)<=2):
			data_internal.append(t1)

		# 	if(len(data_internal)==2):
		# 		data = [data_internal]
		# 		t1_w = 2.6 * inch
		# 		t2_w = 3.2 * inch
		# 		shell_table = Table(data, colWidths=[t1_w, t2_w])
		# 		elements.append(shell_table)
		# 		data_internal = []
		# 		data = []
		# if(len(data_internal) != 0):
		# 	data = [data_internal]
		# 	shell_table = Table(data, colWidths=[t1_w, t2_w])
		# 	elements.append(shell_table)

		t1_w = 2.6 * inch
		t2_w = 2.6 * inch
		t3_w = 2.6 * inch
		t4_w = 2.6 * inch
		data = [data_internal]
		shell_table = Table(data, colWidths=[t1_w, t2_w,t3_w, t4_w ])
		shell_table.setStyle(TableStyle([
    ('VALIGN', (0, 0), (-1, -1), 'TOP')
]))
		elements.append(shell_table)

		doc.build(elements)
		response = FileResponse(open(post.folder + os.sep + post.name + ".pdf", 'r'), 'rb')
		response['Content-Disposition'] = 'attachment; filename=' + name + '.pdf'
		# response.write()

		return response
def make_landscape(canvas,doc):
    canvas.setPageSize(landscape(letter))


def download_table_from_mdr(request, name):
		post = get_object_or_404(Process, name=name)
		list_of_compounds = request.session['mylist']
		dict_of_conformations = {}
		compounds = Compound(data=list_of_compounds, order_by='binding_energy')	

		for compound in compounds.as_values():
			if compound[0] != "Pose number":
				if compound[3] not in dict_of_conformations.keys():

					dict_of_conformations[compound[3]] = [  ["Conformation " + compound[3]], ['Compound' ,'Pose' ,'Binding Energy'] ]
					
				if len(dict_of_conformations[compound[3]]) <= 21:
					compound_name =  compound[1][:-6]
					dict_of_conformations[compound[3]].append( [compound_name,  compound[0], compound[4]])
				else:
					pass

		doc = SimpleDocTemplate(post.folder + os.sep + post.name + ".pdf", pagesize=letter)
		styleSheet = getSampleStyleSheet()
		p1 = Paragraph('''
    <para align=center spaceb=3>MDR SurflexDock results</para>''',
    styleSheet["BodyText"])
		p2 = Paragraph('''
    <para align=center spaceb=3>Top 20 Results Generated for Each Protein Conformation of ''' + os.path.basename(post.protein) + '''</para>''',
    styleSheet["BodyText"])


		data = []
		data_internal = []
		elements = []
		elements.append(p1)
		elements.append(p2)
		for compound in dict_of_conformations:
			table_data = dict_of_conformations[compound]

			t1 = Table(table_data, 2 * [1.2 * inch], len(table_data) * [0.2 * inch])	
			t1.setStyle(TableStyle([
	('SPAN', (0, 0), ((1,0))),
    ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
    ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
]))
			data_internal.append(t1)

			if(len(data_internal)==2):
				data = [data_internal]
				t1_w = 4 * inch
				t2_w = 3.2 * inch
				shell_table = Table(data, colWidths=[t1_w, t2_w])
				elements.append(shell_table)
				data_internal = []
				data = []
		if(len(data_internal) != 0):
			data = [data_internal]
			shell_table = Table(data, colWidths=[t1_w, t2_w])
			elements.append(shell_table)
		doc.build(elements)
		response = FileResponse(open(post.folder + os.sep + post.name + ".pdf", 'r'), 'rb')
		response['Content-Disposition'] = 'attachment; filename=' + name + '.pdf'
		# response.write()

		return response


def debug_mmpbsa(request, name):
	post = get_object_or_404(Process, name=name)
	free_space = subprocess.check_output(['df','-h', TEMPORARYFOLDER]).split('\n')[1].split()[3].decode('utf-8')

	workdir = post.folder
	list_of_files = ['Ligand.acpype', 'complex.pdb', 'energy_MM.xvg']
	list_of_logs = ['em.log', 'md_0_10.log', 'npt.log', 'nvt.log']
	list_of_finded_logs = {}
	for eachLogFile in list_of_logs:
		filename = workdir + eachLogFile
		if os.path.exists(filename):
			list_of_finded_logs[eachLogFile.replace(".", "_")] = True
		else:
			list_of_finded_logs[eachLogFile.replace(".", "_")] = False
	list_of_finded_files = {}
	for eachFile in list_of_files:
		filename = workdir + eachFile
		if os.path.exists(filename):
			list_of_finded_files[eachFile.replace(".", "_")] = True
		else:
			list_of_finded_files[eachFile.replace(".", "_")] = False

	return render(request, 'dyna/mmpbsa_debug.html', { 
		'Ligand_acpype': list_of_finded_files['Ligand_acpype'],
		'complex_pdb': list_of_finded_files['complex_pdb'],
		'energy_MM_xvg': list_of_finded_files['energy_MM_xvg'],

		'em_log': list_of_finded_logs['em_log'],
		'md_0_10_log': list_of_finded_logs['md_0_10_log'],
		'npt_log': list_of_finded_logs['npt_log'],
		'nvt_log': list_of_finded_logs['nvt_log'],
		'post' : post,
		'free_space': free_space
		})


def task_list(request):	
	posts = Process.objects.all()
	for eachPost in posts:
		eachPost.b = False
		# if result(eachPost.name) != None and not eachPost.alive:
		# 	eachPost.alive = True
		# 	eachPost.save()
		if os.path.exists(eachPost.folder):
			size = subprocess.check_output(['du','-sh', eachPost.folder]).split()[0].decode('utf-8')
			eachPost.b = True
			eachPost.size = size
			eachPost.site = eachPost.site.split("/",1)[1]
		else:
	#		eachPost.delete()
			pass

	return render(
			request,
			'dyna/task_list.html',
			{'posts': posts}
		)


def task_detail(request,name):
		
		post = get_object_or_404(Process, name=name)
		ligands = LigandFile.objects.all().filter(name_of_Process=name)
		nresult = result(name)
		if nresult != None and not post.alive:
			post.alive = True
			post.save()


		return render(
			request,
			'dyna/task.html',
			{'post': post, 'ligands' : ligands,'result' : nresult}
		)

def download_detail_table(request, name):
		post = get_object_or_404(Process, name=name)
		list_of_compounds = []
		has_cluster = False
		ligands = LigandFile.objects.all().filter(name_of_Process=name)
		subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',os.path.dirname(post.protein)])
		
		# subprocess.call(["gs","-sDEVICE=jpeg","-dJPEGQ=100","-dNOPAUSE","-dBATCH","-dSAFER","-r300","-sOutputFile=" + os.path.dirname(post.protein) + os.sep + "output.jpg", os.path.dirname(post.protein) + os.sep + "cluster100ns.eps"])
		# if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
		# 	subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
		# 	has_cluster = True

		
		for eachFolder in os.listdir(post.folder):
			path_name = os.path.dirname(post.protein) + os.sep + eachFolder
			if os.path.isdir(path_name) and eachFolder != "Logs":
				for eachFile in os.listdir(path_name):
					int_path = path_name + os.sep + eachFile

					if os.path.isdir(int_path):
						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


									dlg_file = int_path + os.sep + "ligant_receptor.dlg"
									list_of_folder_name =  dlg_file.split(os.sep)
									ligand_name = list_of_folder_name[-2]
									time_name = list_of_folder_name[-3]
									parser = DlgParser(dlg_file)
									mylist = {}
									print dlg_file
									for eachStruct in parser.clist:
										if 'inhib_constant_units' in eachStruct:
											if eachStruct['inhib_constant_units'] == 'uM':
												mag = math.pow(10,-6)
									 		elif eachStruct['inhib_constant_units'] == 'mM':
												mag = math.pow(10,-3)
											elif eachStruct['inhib_constant_units'] == 'nM':
												mag = math.pow(10,-9)
											elif eachStruct['inhib_constant_units'] == 'pM':
												mag = math.pow(10,-12)
											elif eachStruct['inhib_constant_units'] == 'aM':
												mag = math.pow(10,-18)
											mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
									better_Ki = math.pow(10,0)
									docking_id_better_Ki = None
									better_binding_energy = 100
									for eachElem in mylist:

										ki = mylist[eachElem][0]
										binding_energy = mylist[eachElem][1]

										if binding_energy <  better_binding_energy:
											docking_id_better_Ki = eachElem
											better_Ki = ki
											better_binding_energy = binding_energy
									if binding_energy != None:
										# print os.path.basename(int_path)
										text = text + os.path.basename(int_path)[9:-6] + "   " + os.path.basename(int_path) + "   " + eachFolder + "   " + str(better_Ki) + "   " + "0" + "   " + str(better_binding_energy) + "   " + "   " + "0" + "   "  + "0" + "   " + str(better_binding_energy) + "\n"
										# z = {'ki': ki, "time": time_name[0:-7], "compound": os.path.basename(int_path),   'status':False, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'folder' : int_path, 'binding_energy': binding_energy}
										# list_of_compounds.append(z)


		response = HttpResponse((text), content_type='application/force-download')
		# response['Content-Disposition'] = 'attachment; filename=receptor.pdb'
		return response


def download_protein_str(request, experiment_id,time, compound, pose):


		post = get_object_or_404(Process, name=experiment_id)
		best_model = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
		# if time != "Control_folder":
		# 	best_model = best_model + "_em.pdb"
		zip_file = open(best_model, 'r')
		x = zip_file.readlines()

		response = HttpResponse("".join(x), content_type='text/plain')
		# response['Content-Disposition'] = 'attachment; filename=receptor.pdb'
		return response
def download_protein_ligand(request, experiment_id,time, compound, pose):
		post = get_object_or_404(Process, name=experiment_id)

		best_model_path = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
		best_model = open(best_model_path, 'r')
		list_receptor = best_model.readlines()

		# print post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg"
		parser = DlgParser(post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg")
		list_compound = parser.run_models[int(pose)]
		text_compoundPDB =  "\n"
		text_compound =  "\n"
		atom_num = 1

		for eachLine in list_receptor:
			atom_serial_number = str(atom_num).rjust(5)
			text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:21] + "R" + eachLine[22:54] + "\n"
			atom_num = atom_num + 1

		for eachLine in list_compound:
			text_compound = text_compound + eachLine + "\n"
			if eachLine.startswith("ATOM"):
				text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:21] + "L" + eachLine[22:54] + "\n"
		response = HttpResponse("".join(text_compoundPDB), content_type='text/plain')
		response['Content-Disposition'] = 'attachment; filename=complex_' + compound.split(".")[0] + "_" + time.split('.')[0] + "_" + pose + '.pdb'
		return response
def download_ligand(request, experiment_id,time, compound, pose):
		post = get_object_or_404(Process, name=experiment_id)

		best_model_path = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
		# best_model = open(best_model_path, 'r')
		# list_receptor = best_model.readlines()

		# print post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg"
		parser = DlgParser(post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg")
		list_compound = parser.run_models[int(pose)]
		text_compoundPDB =  "\n"
		text_compound =  "\n"
		atom_num = 1

		# for eachLine in list_receptor:
		# 	atom_serial_number = str(atom_num).rjust(5)
		# 	text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:21] + "R" + eachLine[22:54] + "\n"
		# 	atom_num = atom_num + 1

		for eachLine in list_compound:
			text_compound = text_compound + eachLine + "\n"
			if eachLine.startswith("ATOM"):
				text_compoundPDB =  text_compoundPDB  + eachLine[0:54] + "\n"
		response = HttpResponse("".join(text_compoundPDB), content_type='text/plain')
		# response['Content-Disposition'] = 'attachment; filename=complex_' + compound.split(".")[0] + "_" + time.split('.')[0] + "_" + pose + '.pdb'
		return response
def download(request, name):
		
		temp = "/tmp/" + name + ".zip"
		post = get_object_or_404(Process, name=name)
		# subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',os.path.dirname(post.protein)])
		subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'cluster100ns.png'])
		z = zipfile.ZipFile(temp, "w", zipfile.ZIP_DEFLATED)

		for eachFolder in os.listdir(os.path.dirname(post.protein)):
			path_name = os.path.dirname(post.protein) + os.sep + eachFolder
			if eachFolder.endswith("cluster100ns.png"):
				z.write(path_name,  eachFolder)
			# if eachFolder.endswith("analisys.pdf"):
			# 	z.write(path_name,  eachFolder)
			if eachFolder.endswith("telemetry.txt"):
				z.write(path_name,  eachFolder)
			if eachFolder.endswith("commands.txt"):
				z.write(path_name,  eachFolder)
			if os.path.isdir(path_name) and eachFolder != "Logs":
				for eachFile in os.listdir(path_name):
					int_path = path_name + os.sep + eachFile
					if eachFile.endswith("_em.pdb") or eachFile.endswith(os.path.basename(post.protein)) :
						if os.path.exists(int_path):
							z.write( int_path,  eachFolder + os.sep + eachFile)
					if eachFile.endswith("rmsf.xvg"):
							z.write( int_path ,  eachFolder + os.sep + eachFile)
					if os.path.isdir(int_path):
						if os.path.exists(int_path + os.sep + os.path.basename(int_path)):
							z.write(int_path + os.sep + os.path.basename(int_path), eachFolder + os.sep + os.path.basename(int_path)+ os.sep + os.path.basename(int_path))
						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):
							z.write(int_path + os.sep +  os.sep + "ligant_receptor.dlg",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "ligant_receptor.dlg")
						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dpf"):
							z.write(int_path + os.sep +  os.sep + "ligant_receptor.dpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "ligant_receptor.dpf")
						if os.path.exists(int_path + os.sep +  os.sep + "grid.gpf"):
							z.write(int_path + os.sep +  os.sep + "grid.gpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "grid.gpf")
						if os.path.exists(int_path + os.sep +  os.sep + "receptor.gpf"):
							z.write(int_path + os.sep +  os.sep + "receptor.gpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "receptor.gpf")
						if os.path.exists(int_path + os.sep +  os.sep + "receptor.pdbqt"):
							z.write(int_path + os.sep +  os.sep + "receptor.pdbqt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "receptor.pdbqt")			
						if os.path.exists(int_path + os.sep +  os.sep + "docking_log.txt"):
							z.write(int_path + os.sep +  os.sep + "docking_log.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "docking_log.txt")
						if os.path.exists(int_path + os.sep +  os.sep + "docking_output.txt"):
							z.write(int_path + os.sep +  os.sep + "docking_output.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "docking_output.txt")
						if os.path.exists(int_path + os.sep +  os.sep + "telemetry.txt"):
							z.write(int_path + os.sep +  os.sep + "telemetry.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "telemetry.txt")
						if os.path.exists(int_path + os.sep +  os.sep + "commands.txt"):
							z.write(int_path + os.sep +  os.sep + "commands.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "telemetry.txt")
			if eachFolder == "Logs":
				for eachFile in os.listdir(path_name):
					z.write(path_name + os.sep + eachFile, "Logs" + os.sep + eachFile)

		z.close()
		zip_file = open(temp, 'r')
		response = HttpResponse(zip_file, content_type='application/force-download')
		response['Content-Disposition'] = 'attachment; filename=' + name + ".zip"
		return response

def download_refinement(request, name):		
		temp = "/tmp/" + name + ".zip"
		post = get_object_or_404(Process, name=name)
		subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',os.path.dirname(post.folder)])
		# subprocess.call(['convert', '-density','100', os.path.dirname(post.folder) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.folder) + os.sep +'cluster100ns.png'])
		z = zipfile.ZipFile(temp, "w", zipfile.ZIP_DEFLATED)

		for eachFolder in os.listdir(os.path.dirname(post.folder)):
			path_name = os.path.dirname(post.folder) + os.sep + eachFolder
			# if eachFolder.endswith("cluster100ns.png"):
			# 	z.write(path_name,  eachFolder)
			if eachFolder.endswith("analisys.pdf"):
				z.write(path_name,  eachFolder)
			if eachFolder.endswith("telemetry.txt"):
				z.write(path_name,  eachFolder)
			if eachFolder.endswith("commands.txt"):
				z.write(path_name,  eachFolder)
			if os.path.isdir(path_name) and eachFolder != "Logs":
				for eachFile in os.listdir(path_name):
					int_path = path_name + os.sep + eachFile
					if eachFile.endswith("_em.pdb") or eachFile.endswith(os.path.basename(post.folder)) :
						if os.path.exists(int_path):
							z.write( int_path,  eachFolder + os.sep + eachFile)
					if eachFile.endswith("rmsf.xvg"):
							z.write( int_path ,  eachFolder + os.sep + eachFile)
					if os.path.isdir(int_path):
						if os.path.exists(int_path + os.sep + os.path.basename(int_path)):
							z.write(int_path + os.sep + os.path.basename(int_path), eachFolder + os.sep + os.path.basename(int_path)+ os.sep + os.path.basename(int_path))
						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):
							z.write(int_path + os.sep +  os.sep + "ligant_receptor.dlg",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "ligant_receptor.dlg")
						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dpf"):
							z.write(int_path + os.sep +  os.sep + "ligant_receptor.dpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "ligant_receptor.dpf")
						if os.path.exists(int_path + os.sep +  os.sep + "grid.gpf"):
							z.write(int_path + os.sep +  os.sep + "grid.gpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "grid.gpf")
						if os.path.exists(int_path + os.sep +  os.sep + "receptor.gpf"):
							z.write(int_path + os.sep +  os.sep + "receptor.gpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "receptor.gpf")
						if os.path.exists(int_path + os.sep +  os.sep + "receptor.pdbqt"):
							z.write(int_path + os.sep +  os.sep + "receptor.pdbqt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "receptor.pdbqt")			
						if os.path.exists(int_path + os.sep +  os.sep + "docking_log.txt"):
							z.write(int_path + os.sep +  os.sep + "docking_log.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "docking_log.txt")
						if os.path.exists(int_path + os.sep +  os.sep + "docking_output.txt"):
							z.write(int_path + os.sep +  os.sep + "docking_output.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "docking_output.txt")
						if os.path.exists(int_path + os.sep +  os.sep + "telemetry.txt"):
							z.write(int_path + os.sep +  os.sep + "telemetry.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "telemetry.txt")
						if os.path.exists(int_path + os.sep +  os.sep + "commands.txt"):
							z.write(int_path + os.sep +  os.sep + "commands.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "telemetry.txt")
			if eachFolder == "Logs":
				for eachFile in os.listdir(path_name):
					z.write(path_name + os.sep + eachFile, "Logs" + os.sep + eachFile)

		z.close()
		zip_file = open(temp, 'r')
		response = HttpResponse(zip_file, content_type='application/force-download')
		response['Content-Disposition'] = 'attachment; filename=' + name + ".zip"
		return response

def download_mmpbsa(request, name):		
		temp = "/tmp/" + name + ".zip"
		post = get_object_or_404(Process, name=name)
		# subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',os.path.dirname(post.folder)])
		# subprocess.call(['convert', '-density','100', os.path.dirname(post.folder) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.folder) + os.sep +'cluster100ns.png'])
		z = zipfile.ZipFile(temp, "w", zipfile.ZIP_DEFLATED)

		for eachFile in os.listdir(os.path.dirname(post.folder)):
			path_name = os.path.dirname(post.folder) + os.sep + eachFile
			# if eachFile.endswith("cluster100ns.png"):
			# 	z.write(path_name,  eachFile)
			if eachFile.endswith("complex.pdb"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("binding_energy.png"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("commands.txt"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("contrib_apol.dat"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("contrib_MM.dat"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("energy_MM.xvg"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("full_energy.dat"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("receptor.pdb"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("summary_energy.dat"):
				z.write(path_name,  eachFile)
			if eachFile.endswith("ligand.pdb"):
				z.write(path_name,  eachFile)
			

		z.close()
		zip_file = open(temp, 'r')
		response = HttpResponse(zip_file, content_type='application/force-download')
		response['Content-Disposition'] = 'attachment; filename=' + name + ".zip"
		return response

def show_pose(request, experiment_id,time, compound, pose):
		print experiment_id


		post = get_object_or_404(Process, name=experiment_id)
		best_model = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
		# if time != "Control":
		# 	best_model = best_model + "_em.pdb"


		receptor_text = file(best_model,'r')
		text_ = "".join(receptor_text.readlines()[1:])
		receptor_text.close()



		context = {'experiment_id': experiment_id,'time': time, 'compound': compound, 'pose': pose}
		return render(request, 'dyna/show_pose.html', context)

def log(request, name):
	alog = get_object_or_404(Process, name=name)
	x = alog.folder + os.sep + "commands.txt"
	# print x
	zz = file(x,'r')
	k = "\n".join(zz.readlines())
	zz.close()
	context = {'experiment_id': k}
	return render(request, 'dyna/activeLog.html', context)



def activeLog(request, name):
	alog = get_object_or_404(ActiveLog, name=name)
	x = alog.path
	print x
	zz = file(x,'r')
	k = "\n".join(zz.readlines())
	context = {'experiment_id': k}
	return render(request, 'dyna/activeLog.html', context)


def delete_task(request, name):
	post = get_object_or_404(Process, name=name)
	post.alive = True	
	workdir = post.folder
	post.save()
	shutil.rmtree(workdir)
	return render(request,'dyna/delete_task.html', {'name' : name} )


def clusterimg(request, name):
	post = get_object_or_404(Process, name=name)
	workdir = post.folder
	mycluster_img_eps_format = workdir + os.sep + "cluster100ns.eps"
	mycluster_img = workdir + os.sep + "output.png"
	if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
		subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
		# has_cluster = True
	response = HttpResponse(content_type="image/png")
	# request.session['dopeimg'] = workdir + evaluate_energy_manager.__output_files__[0]
	im = Image.open(mycluster_img)
	im.save(response, "PNG")
	return response


# def get_pdbqts(name):
# 	mylist = []
# 	post = get_object_or_404(Process, name=name)
# 	workdir = post.folder
# 	for eachFile in os.walk(workdir):
# 			if eachFile[0].endswith(".pdbqt"):
# 				mylist.append(os.path.basename(eachFile[0]))

# 	return mylist



def debug(request, name):
	instance = get_object_or_404(Process, name=name)
	last_command_list = os.path.dirname(instance.protein) + os.sep + "commands.txt"
	last_command = None
	number_of_runned_steps = 0
	if os.path.exists(last_command_list):
		last_command_stub = file(last_command_list,'r')
		lines_of_command_file = last_command_stub.readlines()
		last_command_stub.close()
		last_command = lines_of_command_file[-1]
		number_of_runned_steps = len(lines_of_command_file)
	
	count_ligands = 0
	count_processed_ligands = 0
	clusters = []
	cluster_todos = []
	count_ligands_in_each_cluster = []
	error_ligands_in_each_cluster = []
	for eachFile in os.listdir(instance.folder):
		if os.path.isdir(instance.folder + os.sep + eachFile) and (not eachFile ==  "Control_folder"):
			clusters.append(eachFile)
		if eachFile.endswith(".pdbqt"):
			count_ligands += 1
		if os.path.isdir(instance.folder + os.sep + eachFile):
			cluster_count = 0
			error_ligands = 0
			cluster_todos.append(eachFile)
			for eachF in os.listdir(instance.folder + os.sep + eachFile):
				if os.path.exists(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'ligant_receptor.dlg'):
					count_processed_ligands += 1
					cluster_count += 1
				if os.path.exists(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'last_output.txt'):
					myfile = file(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'last_output.txt', 'r')
					try:
						erro_list = myfile.readlines()[0]
						
						# print erro_list
						if erro_list != '/usr/local/bin//autodock4: WARNING: Using autodock4.0 unbound extended model in autodock4.2!\n':
							error_ligands +=1
						pass
					except Exception as e:
						pass
					myfile.close()
			count_ligands_in_each_cluster.append(cluster_count)
			error_ligands_in_each_cluster.append(error_ligands)

	missing_simulation_files = check_simulation_files(instance.folder, os.path.basename(instance.protein), clusters)
	
	twi = zip(cluster_todos, count_ligands_in_each_cluster, error_ligands_in_each_cluster)

	return render(request, 'dyna/debug.html', {'twi' : twi, 'clusterB' : cluster_todos, 'count_ligands_in_each_cluster' : count_ligands_in_each_cluster, 'count_processed_ligands' : count_processed_ligands, 'count_ligands': count_ligands, 'clusters': clusters, 'number_of_runned_steps' : number_of_runned_steps , 'missing_files' : missing_simulation_files,'last_command' : last_command, 'post' : instance})

def dockingLogFiles(request, name, time, compound):
	instance = get_object_or_404(Process, name=name)
	real_path_commands = instance.folder + os.sep + time	 + os.sep + compound + os.sep + 'commands.txt'
	real_path = instance.folder + os.sep + time	 + os.sep + compound + os.sep + 'last_output.txt'
	k = ""
	if os.path.exists(real_path_commands):
		commands_file = file(real_path_commands, 'r')
		k = ">>>> Commands executed: \n\n" + "\n".join(commands_file.readlines())

	logfile = file(real_path, 'r')
	k = k + "\n\n\n>>>> The last command caused the following error:\n\n" + "\n".join(logfile.readlines())
	logfile.close()
	context = {'experiment_id': k}
	return render(request, 'dyna/activeLog.html', context)


def dockingLog(request, name, time):
	instance = get_object_or_404(Process, name=name)
	real_path = instance.folder + os.sep + time
	docking_with_erros = []
	for eachFile in os.listdir(real_path):

		if os.path.isdir(real_path + os.sep + eachFile):
			full_path = real_path + os.sep + eachFile
			if os.path.exists(full_path + os.sep + 'last_output.txt'):
				myfile = file(full_path + os.sep +  'last_output.txt', 'r')

				erro_list = myfile.readlines()
				first_line = erro_list[0]
				myfile.close()
				
				if first_line != '/usr/local/bin//autodock4: WARNING: Using autodock4.0 unbound extended model in autodock4.2!\n':
					docking_with_erros.append(eachFile)

	return render(request, 'dyna/dockingLog.html', {'name': name, 'time': time, 'docking_with_erros' : docking_with_erros})
def simulationLog(request, name):
	instance = get_object_or_404(Process, name=name)
	list_of_logs = ['_em.log', '_md.log', '_npt.log', '_nvt.log']
	list_of_finded_logs = []
	for eachLogFile in list_of_logs:
		filename = instance.protein + eachLogFile
		if os.path.exists(filename):
			list_of_finded_logs.append(os.path.basename(filename))

	list_of_finded_logs.append('cluster.log')
	return render(request, 'dyna/show_logs.html', { 'list_of_files' : list_of_finded_logs, 'post' : instance})

def readLog(request, name, logname):
	if logname in ["em_log", "npt_log", "nvt_log", "md_0_10_log", "energy_MM_xvg"  ]:
		logname = logname[:-4] + "." + logname[-3:]
	# if logname.endswith(".log"):
	instance = get_object_or_404(Process, name=name)
	folder = instance.folder 
	logfilename = folder + os.sep + logname
	logfile = file(logfilename, 'r')
	k = "\n".join(logfile.readlines())
	logfile.close()
	context = {'experiment_id': k}
	return render(request, 'dyna/activeLog.html', context)
	# else:
	# 	return None

def check_simulation_files(workdir, protein, cluster):
	missing_files = []
	sufix_of_necessary_files = ['.gro', '_s.gro',
	'_ion.tpr', '_ion.gro', '_em.tpr', '_em.gro', '_nvt.tpr', '_nvt.gro',
	'_md.tpr', '_md.trr', '_md.xtc', '_em.trr', '_nvt.trr', '_md.trr', '_cluster.pdb']
	other_necessay_files = ['Integrin.ndx', 'output.ndx', 'ions.mdp', 'topol.top',
	'trajout.xtc', 'rmsd-clust.xpm', 'cluster100ns.eps',
	]
	# print cluster
	for eachFolder in cluster:
		folder = workdir + os.sep + eachFolder
		sufix_of_necessary_filesB = ['_md.tpr', '_md.pdb', '_em.gro', '_em.top',
		 '_em.tpr']
		other_files = ['l-bfgs_after_md.mdp', 'Integrin.ndx']
		for eachFile in sufix_of_necessary_filesB:
			path_name = folder + os.sep + protein + eachFile
			if not os.path.exists(path_name):
				missing_files.append(path_name)
		for eachFile in other_necessay_files:
			path_name = workdir + os.sep + eachFile
			if not os.path.exists(path_name):
				missing_files.append(path_name)

	for eachFile in sufix_of_necessary_files:
		path_name = workdir + os.sep + protein + eachFile
		if not os.path.exists(path_name):
			missing_files.append(path_name)

	for eachFile in other_necessay_files:
		path_name = workdir + os.sep + eachFile
		if not os.path.exists(path_name):
			missing_files.append(path_name)
	return missing_files

def fix_receptor(request):
	form = FixReceptorForm(request.POST, request.FILES)
	if request.method == 'POST':
		fixReceptor = FixReceptorTask()
		fixReceptor.prepare_workdir()
		receptorFileFilename = request.FILES['receptorFile'].name
		receptorFileChunks = request.FILES['receptorFile'].chunks()
		fixReceptor.write_file(receptorFileFilename, receptorFileChunks)
		fixReceptor.set_parameters(receptorFileFilename)
		fixed_pdb_path = fixReceptor.run()

		fixed_pdb_output = open(fixed_pdb_path, 'r')
		response = HttpResponse(fixed_pdb_output, content_type='application/force-download')
		response['Content-Disposition'] = 'attachment; filename=' + receptorFileFilename
		return response

		# return render(request, 'dyna/hello_world.html')
	else:
		return render(request,
			'dyna/fix_receptor.html',
			{'form': form}
			)
def fix_receptor_error(request):
	form = FixReceptorForm(request.POST, request.FILES)
	if request.method == 'POST':
		fixReceptor = FixReceptorTask()
		fixReceptor.prepare_workdir()
		receptorFileFilename = request.FILES['receptorFile'].name
		receptorFileChunks = request.FILES['receptorFile'].chunks()
		fixReceptor.write_file(receptorFileFilename, receptorFileChunks)
		fixReceptor.set_parameters(receptorFileFilename)
		fixed_pdb_path = fixReceptor.run()

		fixed_pdb_output = open(fixed_pdb_path, 'r')
		response = HttpResponse(fixed_pdb_output, content_type='application/force-download')
		response['Content-Disposition'] = 'attachment; filename=' + receptorFileFilename
		return response

		# return render(request, 'dyna/hello_world.html')
	else:
		return render(request,
			'dyna/fix_receptor_error.html',
			{'form': form}
			)
class Compound(tables.Table):


		experiment_id = tables.Column(visible=False)
		pose_number = tables.LinkColumn(viewname='show_pose', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
		compound = tables.Column()
		ki = tables.Column()
		conformation = tables.Column()
		binding_energy = tables.Column()

		template = '<input type="checkbox" name="bbb" value="{{ record.compound }}' + '*' + '{{ record.conformation }}" />'


		download_complex = tables.LinkColumn(text = "Download!", viewname='download_protein_ligand', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
		refine = tables.TemplateColumn(template)
		class Meta:
			template_name = 'django_tables2/bootstrap.html'
			table_pagination = False
			# # model = vars
			# fields = ('idvars')
			attrs = {
			'th' : {
					'_ordering': {
						'orderable': 'sortable', # Instead of `orderable`
						'ascending': 'ascend',   # Instead of `asc`
						'descending': 'descend'  # Instead of `desc`
					}
				}
			}

# if __name__ == '__main__':

