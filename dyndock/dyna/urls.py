from django.conf.urls import url
from django.conf.urls.static import static
from dyndock import settings
from . import views_admin, views_surflexdock_common, views_htp_surflexdock, views_mdr_surflexdock

#para gambiarra
from .models import Process, LigandFile #, ActiveLog
import os, shutil
from django_q.tasks import async
from .hooks import sleep_task, sleep_task3, sleep_task4 

urlpatterns = [

# Admin
    url(r'^stop_queue_monitor/(?P<id_server>\w+)/$', views_admin.stop_queue_monitor, name='stop_queue_monitor'),
    url(r'^stop_all_queue_monitor/$', views_admin.stop_all_queue_monitor, name='stop_all_queue_monitor'),
    url(r'^start_queue_monitor/$', views_admin.start_queue_monitor, name='start_queue_monitor'),
    url(r'^queue_monitor/$', views_admin.queue_monitor, name='queue_monitor'),

# Common SurflexDock
    url(r'^manual_task$', views_surflexdock_common.manual_task, name='manual_task'),
    url(r'^stop/(?P<name>\w+)/$', views_surflexdock_common.stop, name='stop'),
    url(r'download_table/(?P<name>\w+)/$', views_surflexdock_common.download_table, name = 'download_table'),
    url(r'download_table_from_mdr/(?P<name>\w+)/$', views_surflexdock_common.download_table_from_mdr, name = 'download_table_from_mdr'),
    url(r'^task_list$', views_surflexdock_common.task_list, name='task_list'),
    url(r'download_detail_table/(?P<name>\w+)/$', views_surflexdock_common.download_detail_table, name = 'download_detail_table'),
    url(r'^download_protein_str/(?P<experiment_id>\w+)/(?P<time>\w+.\w+)/(?P<compound>\w+.\w+)/(?P<pose>\w+)$', views_surflexdock_common.download_protein_str, name = 'download_protein_str'),
    url(r'^download_protein_ligand/(?P<experiment_id>\w+)/(?P<time>\w+.\w+)/(?P<compound>.+\..+)/(?P<pose>\w+)$', views_surflexdock_common.download_protein_ligand, name = 'download_protein_ligand'),
    url(r'^download_ligand/(?P<experiment_id>\w+)/(?P<time>\w+.\w+)/(?P<compound>\w+.\w+)/(?P<pose>\w+)$', views_surflexdock_common.download_ligand, name = 'download_ligand'),
    url(r'download/(?P<name>\w+)/$', views_surflexdock_common.download, name = 'download'),
    url(r'download_mmpbsa/(?P<name>\w+)/$', views_surflexdock_common.download_mmpbsa, name = 'download_mmpbsa'),
    url(r'download_refinement/(?P<name>\w+)/$', views_surflexdock_common.download_refinement, name = 'download_refinement'),
    url(r'^show_pose/(?P<experiment_id>\w+)/(?P<time>\w+.\w+)/(?P<compound>.+\..+)/(?P<pose>\w+)$', views_surflexdock_common.show_pose, name = 'show_pose'),
    url(r'^log/(?P<name>\w+)/$', views_surflexdock_common.log, name = 'log'),
    url(r'^activeLog/(?P<name>\w+)/$', views_surflexdock_common.activeLog, name = 'activeLog'),
    url(r'^delete_task/(?P<name>\w+)/$', views_surflexdock_common.delete_task, name='delete_task'),
    url(r'^clusterimg/(?P<name>\w+)/$', views_surflexdock_common.clusterimg, name='clusterimg'),
    url(r'debug/(?P<name>\w+)/$', views_surflexdock_common.debug, name = 'debug'),

    url(r'^dockingLogFiles/(?P<name>\w+)/(?P<time>\w+.\w+)/(?P<compound>.+\..+)$', views_surflexdock_common.dockingLogFiles, name = 'dockingLogFiles'),
    url(r'^dockingLog/(?P<name>\w+)/(?P<time>\w+.\w+)$', views_surflexdock_common.dockingLog, name = 'dockingLog'),
    url(r'simulationLog/(?P<name>\w+)/$', views_surflexdock_common.simulationLog, name = 'simulationLog'),
    url(r'^readLog/(?P<name>\w+)/(?P<logname>\w+.\w+.\w+)$', views_surflexdock_common.readLog, name = 'readLog'),
   
    url(r'^task/(?P<name>\w+)/$', views_surflexdock_common.task_detail, name='task_detail'),
    url(r'^fix_receptor/$', views_surflexdock_common.fix_receptor, name='fix_receptor'),
    url(r'^fix_receptor_error/$', views_surflexdock_common.fix_receptor_error, name='fix_receptor_error'),

    url(r'^debug_mmpbsa/(?P<name>\w+)/$', views_surflexdock_common.debug_mmpbsa, name = 'debug_mmpbsa'),

# MDR SurflexDock
    url(r'^experiment_detail2/(?P<name>\w+)/$', views_mdr_surflexdock.experiment_detail2, name = 'experiment_detail2'),
    url(r'^boxplot/(?P<name>\w+)/(?P<ligand>\w+.\w+)/$', views_mdr_surflexdock.boxplot, name='boxplot'),
    url(r'^download_mdr_analisys/(?P<name>\w+)/$', views_mdr_surflexdock.download_mdr_analisys, name = 'download_mdr_analisys'),

# HTP SurflexDock
    # url(r'^$', views_htp_surflexdock.load_files, name='load_files'),
    url(r'^$', views_htp_surflexdock.load_files, name='load_files'),
    # url(r'^basic_tutorial/$', views_htp_surflexdock.basic_tutorial, name='basic_tutorial'),
    url(r'redo_docking/(?P<name>\w+)/$', views_htp_surflexdock.redo_docking, name = 'redo_docking'),
    url(r'^download_roc_tables/(?P<name>\w+)/$', views_htp_surflexdock.download_roc_tables, name = 'download_roc_tables'),
    url(r'^download_roc_table/(?P<name>\w+)/(?P<time>\w+.\w+)$', views_htp_surflexdock.download_roc_table, name = 'download_roc_table'),
    url(r'^experiment_detailn/(?P<name>\w+)/$', views_htp_surflexdock.experiment_detailn, name = 'experiment_detailn'),    
    url(r'^experiment_detail3/(?P<name>\w+)/$', views_htp_surflexdock.experiment_detail3, name = 'experiment_detail3'),
    url(r'^second_stage/(?P<name>\w+)/$', views_htp_surflexdock.second_stage, name='second_stage'),
    url(r'^task_list_user/(?P<name>\w+)/$', views_htp_surflexdock.task_list_user, name='task_list_user'),
    
    url(r'^generate_table/(?P<name>\w+)/$', views_htp_surflexdock.generate_table, name='generate_table'),
    url(r'^generate_table_old/(?P<name>\w+)/$', views_htp_surflexdock.generate_table_old, name='generate_table_old'),

    url(r'^experiment_detail/(?P<name>\w+)/$', views_htp_surflexdock.experiment_detail3, name = 'experiment_detail'),
   #new in 3_11
    url(r'^convert_mol2_to_pdbqt/$', views_htp_surflexdock.convert_mol2_to_pdbqt, name='convert_mol2_to_pdbqt'),
    url(r'^receptor_error/$', views_htp_surflexdock.receptor_error, name='receptor_error'),
   #new in 4_0
    url(r'^mmpbsa_analisys/(?P<name>\w+)/$', views_htp_surflexdock.mmpbsa_analisys, name='mmpbsa_analisys'),
    url(r'^mmpbsa_result/(?P<name>\w+)/$', views_htp_surflexdock.mmpbsa_result, name='mmpbsa_result'),
    url(r'^mmpbsaimg/(?P<name>\w+)/$', views_htp_surflexdock.mmpbsaimg, name='mmpbsaimg'),
# - 
   
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


#gambiarra para reiniciar as tarefas

# print "Oi"
#if os.environ.get('RUN_MAIN', None) != 'true':
#    posts = Process.objects.all()
#    # print posts
#    for eachPost in posts:
#            # eachPost.b = False
#            if not eachPost.alive:
#                experiment = eachPost.name
#                filename =  eachPost.protein
#                workdir = os.path.dirname(filename)
#                for eachFolder in os.listdir(workdir):
#                    path = workdir + os.sep + eachFolder
 #                   if os.path.isdir(path):
 #                       shutil.rmtree(path)
                
#                type_docking = eachPost.type_docking
#                chainData = eachPost.chain
#                residue_number = int(eachPost.residue_number)
#                size = float(eachPost.size) 
#                cluster_cutoff = eachPost.cluster_cutoff

#                ligandList = []
                    
                    
                # print chainData

#                for eachLigand in LigandFile.objects.all().filter(name_of_Process=experiment):
#                    ligandList.append(eachLigand.filename)

#                if (os.path.exists(filename)):
#                    task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)
#                else:
#                    task_id = async(sleep_task3, workdir,ligandList,chainData,post.residue_number, post.size, type_docking)
