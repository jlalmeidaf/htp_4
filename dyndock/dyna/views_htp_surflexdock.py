# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import subprocess #, psutil
import tarfile

from django.shortcuts import render, get_object_or_404
from .forms import DockingForm, DockingFormStage2, ConvertForm
import tempfile, shutil
from django_q.tasks import async, result, fetch
# from django_q.brokers import get_broker
# from django_q.tasks import Async
from .hooks import sleep_task, sleep_task3, sleep_task4, mmpbsa #, sleep_task2, print_task, 
from .models import Process, LigandFile #, ActiveLog
from django.http import HttpResponseRedirect, HttpResponse #, FileResponse
from django.core.urlresolvers import reverse
# from django.utils import timezone
# from django_q.monitor import Stat
from django.views.decorators.csrf import csrf_protect
from contextlib import closing
from PIL import Image


from .ExperimentController import ExperimentController, SampleExperimentController, ManyLigandsException, ParameterNotSet, BadPDBException, ConvertMol2Task, ReceptorDontStartInResidueOneException, ANISOUException, ResidueMissingException, BadPDBStructureException


import os, sys, math
from zipfile import ZipFile

from django.contrib.staticfiles.storage import staticfiles_storage

# Create your views here.

import django_tables2 as tables
from django_tables2.utils import A 
from django_tables2 import RequestConfig

import random
import datetime
from dyndock import settings

os.environ['MGLTOOLS'] = settings.MGLTOOLS

sys.path.append(os.environ['MGLTOOLS'] + '/MGLToolsPckgs/')
from AutoDockTools.DlgParser import DlgParser

TEMPORARYFOLDER=settings.TEMPORARYFOLDER


#verificar a necessidade disto
def redo_docking(self, name):
	post = get_object_or_404(Process, name=name)
	workdir = post.folder
	times = []

	#remove compound dirs

	for each_files_and_folders in os.listdir(workdir):
		selected_file = workdir + os.sep + each_files_and_folders
		if os.path.isdir(selected_file):
					for each_file in os.listdir(selected_file):
						compound_folder = selected_file + os.sep + each_file
						if os.path.isdir(compound_folder):
							shutil.rmtree(compound_folder)


	task_id = async(sleep_task4, post.protein,post.chain,post.residue_number, post.size, post.type_docking, post.cluster_cutoff)


def download_roc_tables(request, name):
	post = get_object_or_404(Process, name=name)
	times = []
	list_of_compounds = request.session['mylist']
	compounds = Compound(data=list_of_compounds, order_by='binding_energy')
	for compound in compounds.as_values():
		 if compound[4] not in times:
		 	times.append(compound[4])
	context = {'name': name, 'times' : times}
	return render(request, 'dyna/roc_tables.html', context)



def download_roc_table(request,name,time):
		post = get_object_or_404(Process, name=name)
		list_of_compounds = request.session['mylist']
		dict_of_conformations = {}
		compounds = Compound(data=list_of_compounds, order_by='binding_energy')	

		text = " time: " + time + "\n"
		for compound in compounds.as_values():
			if compound[4] == time:
				text = text + str(compound[2]) + " " + str(compound[5]) + "\n"

		response = HttpResponse((text), content_type='application/force-download')
		return response

		return response


@csrf_protect
def load_files(request):
	if request.method == 'POST':
		form = DockingForm(request.POST, request.FILES)
		if form.is_valid():
			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep

			filename = workdir + request.FILES['structureFile'].name
			structureFile = file(filename, "wb+")
			for chunk in request.FILES['structureFile'].chunks():
				structureFile.write(chunk)
			structureFile.close()
			ligandList = []
			for eachLigandFile in form.cleaned_data['ligandFile']:
				fileLigand = eachLigandFile.name
				ligandList.append(fileLigand)
				ligandFile = file(workdir + fileLigand, "wb+")
				for chunk in eachLigandFile.chunks():
					ligandFile.write(chunk)
				ligandFile.close()
			count_ligands_files = 0

			#check if the compound library contain less to 100 files 
			if fileLigand.endswith(".zip"):
				archive = ZipFile(workdir + fileLigand)
				count_ligands_files = len(archive.infolist()) -1

			else:
				t = tarfile.open(workdir + fileLigand,'r')
				count_ligands_files = len(t.getmembers()) - 1

			if count_ligands_files > 10000: #aqui
				return render(
			request,
			'dyna/moreThan100.html'
		)
			else:
				if settings.USE_EMAIL_BACKEND:
					myemail = form.cleaned_data['email']
				else:
					myemail = "example@example.com"
				projectName = form.cleaned_data['projectName']
				type_docking = '10'
				chainData = form.cleaned_data.get('chain')
				residue_number = int(form.cleaned_data['residue_number'])
				size = float(form.cleaned_data['size'])	
				cluster_cutoff = form.cleaned_data['cluster_cutoff']
				exp_control = ExperimentController()
				try:
					exp_control.set_parameters(filename, myemail,
							projectName, type_docking, chainData, residue_number, 
							size, cluster_cutoff, None)
					exp_control.workdir = workdir
					exp_control.check_receptor_integrity()
					task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)
					process = Process.objects.create(name= task_id,
						alive = False,
						folder = workdir,
						protein = filename,
						chain = chainData,
						residue_number = residue_number,
						type_docking = type_docking,
						size = size,
						fail = False,
						user_email = myemail,
						projectName = projectName,
						cluster_cutoff = cluster_cutoff,
						site=request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id}))

						# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
					process.publish()

					for eachLigandFile in ligandList:
						ligand = LigandFile.objects.create(name_of_Process = task_id,
							filename = eachLigandFile)
						ligand.publish()
					
					url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id})
					# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

					return HttpResponseRedirect(url)

				except ANISOUException as e:
					request.session['error_msg'] = "ANISOUException"
					url = reverse("receptor_error")					
					return HttpResponseRedirect(url)

				except ResidueMissingException as e:
					request.session['error_msg'] = "ResidueMissingException"
					url = reverse("receptor_error")					
					return HttpResponseRedirect(url)
				except BadPDBStructureException as e:
					request.session['error_msg'] = "BadPDBStructureException"
					url = reverse("receptor_error")					
					return HttpResponseRedirect(url)
				except ReceptorDontStartInResidueOneException as e:
					url = reverse("fix_receptor_error")
					return HttpResponseRedirect(url)
		else:
			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep
			shutil.copyfile( staticfiles_storage.path('example/hACEcOk.pdb'), workdir + os.sep + 'hACEcOk.pdb')
			shutil.copyfile( staticfiles_storage.path('example/sample_15_lig_85_dec_hace_c.tar.gz'), workdir + os.sep + 'sample_15_lig_85_dec_hace_c.tar.gz')

			filename = workdir + 'hACEcOk.pdb'

			ligandList = ['sample_15_lig_85_dec_hace_c.tar.gz']

			if settings.USE_EMAIL_BACKEND:
				myemail = form.cleaned_data['email']
			else:
				myemail = "example@example.com"
			projectName = form.cleaned_data['projectName']
			type_docking = '10'
			chainData = form.cleaned_data.get('chain')
			residue_number = int(form.cleaned_data['residue_number'])
			size = float(form.cleaned_data['size'])	
			cluster_cutoff = form.cleaned_data['cluster_cutoff']	

			task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)
			process = Process.objects.create(name= task_id,
				alive = False,
				folder = workdir,
				protein = filename,
				chain = chainData,
				residue_number = residue_number,
				type_docking = type_docking,
				size = size,
				fail = False,
				user_email = myemail,
				projectName = projectName,
				cluster_cutoff = cluster_cutoff,
				site=request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id}))

				# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
			process.publish()

			for eachLigandFile in ligandList:
				ligand = LigandFile.objects.create(name_of_Process = task_id,
					filename = eachLigandFile)
				ligand.publish()
			
# 			send_mail(
# 	'HTP SurflexDock Task Created',
# 	'Your request to the server was accepted. You can follow the process on ' + "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id}) ,
# 	'devlab230uenf@gmail.com',
# 	[myemail],
# 	fail_silently=False,
# )
			# url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id})
			url = "http://" + request.META['HTTP_HOST'] + reverse("task_list_user",  kwargs={'name': task_id})
			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

			return HttpResponseRedirect(url)
	else:
		form = DockingForm()  # A empty, unbound form
		# form.fields["projectName"].initial = "oiasd@sfd.com"

		return render(
			request,
			'dyna/load_files.html',
			{'form': form, 'use_email_backend' : settings.USE_EMAIL_BACKEND, 'example_result_link' :  settings.EXAMPLE_RESULT_LINK, 
			'show_view_example' : settings.SHOW_VIEW_EXAMPLE}
		)

@csrf_protect
def load_files_without_email(request):
	if request.method == 'POST':
		form = DockingForm(request.POST, request.FILES)
		if form.is_valid():
			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep

			filename = workdir + request.FILES['structureFile'].name
			structureFile = file(filename, "wb+")
			for chunk in request.FILES['structureFile'].chunks():
				structureFile.write(chunk)
			structureFile.close()
			ligandList = []
			for eachLigandFile in form.cleaned_data['ligandFile']:
				fileLigand = eachLigandFile.name
				ligandList.append(fileLigand)
				ligandFile = file(workdir + fileLigand, "wb+")
				for chunk in eachLigandFile.chunks():
					ligandFile.write(chunk)
				ligandFile.close()
			count_ligands_files = 0

			#check if the compound library contain less to 100 files 
			if fileLigand.endswith(".zip"):
				archive = ZipFile(workdir + fileLigand)
				count_ligands_files = len(archive.infolist()) -1

			else:
				t = tarfile.open(workdir + fileLigand,'r')
				count_ligands_files = len(t.getmembers()) - 1

			if count_ligands_files > 100:
				return render(
			request,
			'dyna/moreThan100.html'
		)
			else:

				# myemail = form.cleaned_data['email']
				myemail = "jlalmeidaf@hotmail.com"
				projectName = form.cleaned_data['projectName']
				type_docking = form.cleaned_data['type_docking']
				chainData = form.cleaned_data.get('chain')
				residue_number = int(form.cleaned_data['residue_number'])
				size = float(form.cleaned_data['size'])	
				cluster_cutoff = form.cleaned_data['cluster_cutoff']	
				task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)
				process = Process.objects.create(name= task_id,
					alive = False,
					folder = workdir,
					protein = filename,
					chain = chainData,
					residue_number = residue_number,
					type_docking = type_docking,
					size = size,
					fail = False,
					user_email = myemail,
					projectName = projectName,
					cluster_cutoff = cluster_cutoff,
					site=request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id}))

					# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
				process.publish()

				for eachLigandFile in ligandList:
					ligand = LigandFile.objects.create(name_of_Process = task_id,
						filename = eachLigandFile)
					ligand.publish()
				
	# 			send_mail(
	# 	'HTP SurflexDock Task Created',
	# 	'Your request to the server was accepted. You can follow the process on ' + "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id}) ,
	# 	'devlab230uenf@gmail.com',
	# 	[myemail],
	# 	fail_silently=False,
	# )
				url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id})
				# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

				return HttpResponseRedirect(url)
		else:
			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep
			shutil.copyfile( staticfiles_storage.path('example/hACEcOk.pdb'), workdir + os.sep + 'hACEcOk.pdb')
			shutil.copyfile( staticfiles_storage.path('example/sample_15_lig_85_dec_hace_c.tar.gz'), workdir + os.sep + 'sample_15_lig_85_dec_hace_c.tar.gz')

			filename = workdir + 'hACEcOk.pdb'

			ligandList = ['sample_15_lig_85_dec_hace_c.tar.gz']

			# myemail = form.cleaned_data['email']
			myemail = 'jlalmeidaf@hotmail.com'
			projectName = form.cleaned_data['projectName']
			type_docking = form.cleaned_data['type_docking']
			chainData = form.cleaned_data.get('chain')
			residue_number = int(form.cleaned_data['residue_number'])
			size = float(form.cleaned_data['size'])	
			cluster_cutoff = form.cleaned_data['cluster_cutoff']	

			task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)
			process = Process.objects.create(name= task_id,
				alive = False,
				folder = workdir,
				protein = filename,
				chain = chainData,
				residue_number = residue_number,
				type_docking = type_docking,
				size = size,
				fail = False,
				user_email = myemail,
				projectName = projectName,
				cluster_cutoff = cluster_cutoff,
				site=request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id}))

				# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
			process.publish()

			for eachLigandFile in ligandList:
				ligand = LigandFile.objects.create(name_of_Process = task_id,
					filename = eachLigandFile)
				ligand.publish()
			
# 			send_mail(
# 	'HTP SurflexDock Task Created',
# 	'Your request to the server was accepted. You can follow the process on ' + "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id}) ,
# 	'devlab230uenf@gmail.com',
# 	[myemail],
# 	fail_silently=False,
# )
			# url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id})
			url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id})
			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

			return HttpResponseRedirect(url)
	else:
		form = DockingForm()  # A empty, unbound form
		# form.fields["projectName"].initial = "oiasd@sfd.com"

		return render(
			request,
			'dyna/load_files_without_email.html',
			{'form': form}
		)
def receptor_error(request):
			error_msg = request.session['error_msg']
			return render(
			request,
			'dyna/receptor_error.html',
			{'error_msg': error_msg}
		)

def experiment_detailn(request,name):
	if request.method == 'POST':

		mylist = request.session['mylist']


		list_of_input_ids=request.POST.getlist("bbb")

		new_list = []
		for eachTag in list_of_input_ids:
			for eachN in mylist:
				if eachN['compound'] == eachTag.split('*')[0] and eachN['conformation'] == eachTag.split('*')[1]:
					new_list.append(eachN)
		# print list_of_input_ids
		request.session['mylist2'] = new_list
		url = "http://" + request.META['HTTP_HOST'] + reverse("second_stage",  kwargs={'name': name})
			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

		return HttpResponseRedirect(url)
	else:
		free_space = subprocess.check_output(['df','-h', TEMPORARYFOLDER]).split('\n')[1].split()[3].decode('utf-8')
		post = get_object_or_404(Process, name=name)
		has_cluster = False
		if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
			subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
			has_cluster = True

		try:
			if request.session['name'] != name:
				raise KeyError() 
			mytime = int(datetime.datetime.now().strftime('%s'))
			if mytime - request.session['time'] > 600:
				raise KeyError()
			list_of_compounds = request.session['mylist']
		except KeyError as e:
			request.session['name'] = name
			request.session['time'] = int(datetime.datetime.now().strftime('%s'))
			list_of_compounds = []

			
			for eachFolder in os.listdir(post.folder):
				# print post.folder
				path_name = post.folder + os.sep + eachFolder
				if os.path.isdir(path_name) and eachFolder != "Logs":
					for eachFile in os.listdir(path_name):
						int_path = path_name + os.sep + eachFile

						if os.path.isdir(int_path):
							if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
										list_of_folder_name =  dlg_file.split(os.sep)
										ligand_name = list_of_folder_name[-2]
										time_name = list_of_folder_name[-3]
										parser = DlgParser(dlg_file)
										mylist = {}
										for eachStruct in parser.clist:
											if 'inhib_constant_units' in eachStruct:
												if eachStruct['inhib_constant_units'] == 'uM':
													mag = math.pow(10,-6)
										 		elif eachStruct['inhib_constant_units'] == 'mM':
													mag = math.pow(10,-3)
												elif eachStruct['inhib_constant_units'] == 'nM':
													mag = math.pow(10,-9)
												elif eachStruct['inhib_constant_units'] == 'pM':
													mag = math.pow(10,-12)
												elif eachStruct['inhib_constant_units'] == 'fM':
													mag = math.pow(10,-15)
												elif eachStruct['inhib_constant_units'] == 'aM':
													mag = math.pow(10,-18)
												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
										better_Ki = math.pow(10,0)
										docking_id_better_Ki = None
										better_binding_energy = 100
										for eachElem in mylist:

											ki = mylist[eachElem][0]
											binding_energy = mylist[eachElem][1]

											if binding_energy <  better_binding_energy:
												docking_id_better_Ki = eachElem
												better_Ki = ki
												better_binding_energy = binding_energy
										if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"):
											z = {'ki': better_Ki, "time": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'folder' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"}
										else:
											z = {'ki': better_Ki, "time": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'folder' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein)}
										list_of_compounds.append(z)

				request.session['mylist'] = list_of_compounds


		compounds = Compound(list_of_compounds)
		# compounds.paginate = False
		RequestConfig(request, paginate=False).configure(compounds)
		# for eachElem in compounds:
		# 	print eachElem
		return render(
			request,
			'dyna/experiment_detail.html',
			{'free_space': free_space, 'post' : post, 'compounds': compounds, 'has_cluster' : has_cluster}
		)


def generate_table_old(request, name):
    post = get_object_or_404(Process, name=name)
    list_of_compounds = []
    for eachFolder in os.listdir(post.folder):
        path_name = post.folder + os.sep + eachFolder
        # print eachFolder
        if os.path.isdir(path_name) and eachFolder != "Logs":
            for eachFile in os.listdir(path_name):
                int_path = path_name + os.sep + eachFile
                if os.path.isdir(int_path):
                    if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):
                        dlg_file = int_path + os.sep + "ligant_receptor.dlg"
                        list_of_folder_name =  dlg_file.split(os.sep)
                        ligand_name = list_of_folder_name[-2]
                        time_name = list_of_folder_name[-3]
                        parser = DlgParser(dlg_file)
                        mylist = {}
                        for eachStruct in parser.clist:
                            if 'inhib_constant_units' in eachStruct:
                                if eachStruct['inhib_constant_units'] == 'uM':
                                        mag = math.pow(10,-6)
                                elif eachStruct['inhib_constant_units'] == 'mM':
                                        mag = math.pow(10,-3)
                                elif eachStruct['inhib_constant_units'] == 'nM':
                                        mag = math.pow(10,-9)
                                elif eachStruct['inhib_constant_units'] == 'pM':
                                        mag = math.pow(10,-12)
                                elif eachStruct['inhib_constant_units'] == 'fM':
                                        mag = math.pow(10,-15)
                                elif eachStruct['inhib_constant_units'] == 'aM':
                                        mag = math.pow(10,-18)
                                mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
                        better_Ki = math.pow(10,0)
                        docking_id_better_Ki = None
                        better_binding_energy = 100
                        for eachElem in mylist:
                            ki = mylist[eachElem][0]
                            binding_energy = mylist[eachElem][1]
                            docking_id_better_Ki = eachElem
                            better_Ki = ki
                            better_binding_energy = binding_energy
                            if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"):
                                z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"}
                            else:   
                                z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein)}
                            list_of_compounds.append(z)

                        compound_dict = {}
                        for eachCompound in list_of_compounds:
                            if eachCompound["compound"] not in compound_dict:
                                compound_list[eachCompound["compound"]] = []
                            else:
                                compound_list[eachCompound["compound"]].append(eachCompound["binding_energy"])

                        compound_list = file(post.folder + os.sep + "compound_list.csv", 'w')
                        compound_listFull = []
                        for eachCompound in compound_dict:
                        	compound_listFull.append([eachCompound] + compound_list[eachCompound])

                        t_compound_listFull = zip(*compound_listFull)

                        for eachLine in t_compound_listFull:
                        	compound_list.write(eachLine)
           

                        compound_list.close()

def generate_table_old2(request, name):
			post = get_object_or_404(Process, name=name)
			list_of_compounds = []
			for eachFolder in os.listdir(post.folder):
				path_name = post.folder + os.sep + eachFolder
                                # print eachFolder
				if os.path.isdir(path_name) and eachFolder != "Logs":
					for eachFile in os.listdir(path_name):
						int_path = path_name + os.sep + eachFile

						if os.path.isdir(int_path):
							if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
										list_of_folder_name =  dlg_file.split(os.sep)
										ligand_name = list_of_folder_name[-2]
										time_name = list_of_folder_name[-3]
										parser = DlgParser(dlg_file)
										mylist = {}
										for eachStruct in parser.clist:
											if 'inhib_constant_units' in eachStruct:
												if eachStruct['inhib_constant_units'] == 'uM':
													mag = math.pow(10,-6)
										 		elif eachStruct['inhib_constant_units'] == 'mM':
													mag = math.pow(10,-3)
												elif eachStruct['inhib_constant_units'] == 'nM':
													mag = math.pow(10,-9)
												elif eachStruct['inhib_constant_units'] == 'pM':
													mag = math.pow(10,-12)
												elif eachStruct['inhib_constant_units'] == 'fM':
													mag = math.pow(10,-15)
												elif eachStruct['inhib_constant_units'] == 'aM':
													mag = math.pow(10,-18)
												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
										better_Ki = math.pow(10,0)
										docking_id_better_Ki = None
										better_binding_energy = 100
										for eachElem in mylist:

											ki = mylist[eachElem][0]
											binding_energy = mylist[eachElem][1]

											# if binding_energy <  better_binding_energy:
											docking_id_better_Ki = eachElem
											better_Ki = ki
											better_binding_energy = binding_energy
											if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"):
												z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"}
											else:
												z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein)}
											list_of_compounds.append(z)

										compound_list = file(post.folder + os.sep + "compound_list.csv", 'w')
										for eachCompound in list_of_compounds:
											compound_list.write(eachCompound["compound"] + "\t" + str(eachCompound["ki"]) + "\t" + 
											str(eachCompound["select"]) + "\t" +  eachCompound["experiment_id"] + "\t" +  str(eachCompound["pose_number"]) + "\t" + 
											eachCompound["download_complex"] + "\t" +  eachCompound["receptor"] + "\t" +  eachCompound["conformation"] + "\t" +  str(eachCompound["binding_energy"]) + "\n")


										compound_list.close()



def generate_table(request, name):
			post = get_object_or_404(Process, name=name)
			list_of_compounds = []
			for eachFolder in os.listdir(post.folder):
				path_name = post.folder + os.sep + eachFolder
                                # print eachFolder
				if os.path.isdir(path_name) and eachFolder != "Logs":
					for eachFile in os.listdir(path_name):
						int_path = path_name + os.sep + eachFile

						if os.path.isdir(int_path):
							if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
										list_of_folder_name =  dlg_file.split(os.sep)
										ligand_name = list_of_folder_name[-2]
										time_name = list_of_folder_name[-3]
										parser = DlgParser(dlg_file)
										mylist = {}
										for eachStruct in parser.clist:
											if 'inhib_constant_units' in eachStruct:
												if eachStruct['inhib_constant_units'] == 'uM':
													mag = math.pow(10,-6)
										 		elif eachStruct['inhib_constant_units'] == 'mM':
													mag = math.pow(10,-3)
												elif eachStruct['inhib_constant_units'] == 'nM':
													mag = math.pow(10,-9)
												elif eachStruct['inhib_constant_units'] == 'pM':
													mag = math.pow(10,-12)
												elif eachStruct['inhib_constant_units'] == 'fM':
													mag = math.pow(10,-15)
												elif eachStruct['inhib_constant_units'] == 'aM':
													mag = math.pow(10,-18)
												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
										better_Ki = math.pow(10,0)
										docking_id_better_Ki = None
										better_binding_energy = 100
										for eachElem in mylist:

											ki = mylist[eachElem][0]
											binding_energy = mylist[eachElem][1]

											if binding_energy <  better_binding_energy:
												docking_id_better_Ki = eachElem
												better_Ki = ki
												better_binding_energy = binding_energy
										if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"):
											z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"}
										else:
											z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein)}
										list_of_compounds.append(z)

										compound_list = file(post.folder + os.sep + "compound_list.csv", 'w')
										for eachCompound in list_of_compounds:
											compound_list.write(eachCompound["compound"] + "\t" + str(eachCompound["ki"]) + "\t" + 
											str(eachCompound["select"]) + "\t" +  eachCompound["experiment_id"] + "\t" +  str(eachCompound["pose_number"]) + "\t" + 
											eachCompound["download_complex"] + "\t" +  eachCompound["receptor"] + "\t" +  eachCompound["conformation"] + "\t" +  str(eachCompound["binding_energy"]) + "\n")


										compound_list.close()
				


def experiment_detail3(request,name):
	if request.method == 'POST':

		mylist = request.session['mylist']


		list_of_input_ids=request.POST.getlist("bbb")

		new_list = []
		for eachTag in list_of_input_ids:
			for eachN in mylist:
				if eachN['compound'] == eachTag.split('*')[0] and eachN['conformation'] == eachTag.split('*')[1]:
					new_list.append(eachN)
		# print list_of_input_ids
		request.session['mylist2'] = new_list
		# print new_list
		if 'refining' in request.POST:
				if not len(new_list) > 10 and len(new_list) != 0:
					url = "http://" + request.META['HTTP_HOST'] + reverse("second_stage",  kwargs={'mmpbsa_analisysmmpbsa_analisysname': name})
				else:
					return render(request,'dyna/moreThan10.html',)
		elif 'mmpbsa' in request.POST:
				if len(new_list) == 1:
					url = "http://" + request.META['HTTP_HOST'] + reverse("mmpbsa_analisys",  kwargs={'name': name})
				else:
					return render(request,'dyna/moreThan1.html',)
		return HttpResponseRedirect(url)
	else:
		free_space = subprocess.check_output(['df','-h', TEMPORARYFOLDER]).split('\n')[1].split()[3].decode('utf-8')
		post = get_object_or_404(Process, name=name)
		has_cluster = False
		if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
			subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
			has_cluster = True

		try:
			if request.session['name'] != name:
				raise KeyError() 
			mytime = int(datetime.datetime.now().strftime('%s'))
			if mytime - request.session['time'] > 600:
				raise KeyError()
			list_of_compounds = request.session['mylist']
		except KeyError as e:
			request.session['name'] = name
			request.session['time'] = int(datetime.datetime.now().strftime('%s'))
			list_of_compounds = []

			if(os.path.exists(post.folder + os.sep + "compound_list.csv")):
				compound_list = file(post.folder + os.sep + "compound_list.csv", 'r')
				lines = compound_list.readlines()
				compound_list.close()

				for eachLine in lines:
					line_in_table = eachLine.split("\t")

					z = {'ki': float(line_in_table[1]), "conformation": line_in_table[7], "compound": line_in_table[0],   'select':line_in_table[2], 'experiment_id': line_in_table[3], 
					'pose_number' : line_in_table[4], 'download_complex' : line_in_table[5], 'binding_energy': float(line_in_table[8]), 'receptor' : line_in_table[6]}

					list_of_compounds.append(z)

				request.session['mylist'] = list_of_compounds
			else:	

				for eachFolder in os.listdir(post.folder):
					# print post.folder
					path_name = post.folder + os.sep + eachFolder
					if os.path.isdir(path_name) and eachFolder != "Logs":
						for eachFile in os.listdir(path_name):
							int_path = path_name + os.sep + eachFile

							if os.path.isdir(int_path):
								if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


											dlg_file = int_path + os.sep + "ligant_receptor.dlg"
											list_of_folder_name =  dlg_file.split(os.sep)
											ligand_name = list_of_folder_name[-2]
											time_name = list_of_folder_name[-3]
											parser = DlgParser(dlg_file)
											mylist = {}
											for eachStruct in parser.clist:
												if 'inhib_constant_units' in eachStruct:
													if eachStruct['inhib_constant_units'] == 'uM':
														mag = math.pow(10,-6)
											 		elif eachStruct['inhib_constant_units'] == 'mM':
														mag = math.pow(10,-3)
													elif eachStruct['inhib_constant_units'] == 'nM':
														mag = math.pow(10,-9)
													elif eachStruct['inhib_constant_units'] == 'pM':
														mag = math.pow(10,-12)
													elif eachStruct['inhib_constant_units'] == 'fM':
														mag = math.pow(10,-15)
													elif eachStruct['inhib_constant_units'] == 'aM':
														mag = math.pow(10,-18)
													mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
											better_Ki = math.pow(10,0)
											docking_id_better_Ki = None
											better_binding_energy = 100
											for eachElem in mylist:

												ki = mylist[eachElem][0]
												binding_energy = mylist[eachElem][1]

												if binding_energy <  better_binding_energy:
													docking_id_better_Ki = eachElem
													better_Ki = ki
													better_binding_energy = binding_energy
											if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"):
												z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"}
											else:
												z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein)}
											list_of_compounds.append(z)

					request.session['mylist'] = list_of_compounds


		compounds = Compound(list_of_compounds)
		# compounds.paginate = False
		RequestConfig(request, paginate=False).configure(compounds)
		# for eachElem in compounds:
		# 	print eachElem
		return render(
			request,
			'dyna/experiment_detail_n.html',
			{'free_space': free_space, 'post' : post, 'compounds': compounds, 'has_cluster' : has_cluster}
		)

def second_stage(request, name):
	if request.method == 'POST':

		form = DockingFormStage2(request.POST, request.FILES)
		if form.is_valid():
			type_docking = form.cleaned_data['type_docking']
			mylist = request.session['mylist2']
			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep
			ligandList = []
			for eachLigand in mylist:
				time_folder = workdir + os.sep + os.path.basename(os.path.dirname(eachLigand['download_complex']))
				if not os.path.exists(time_folder):
					os.mkdir(time_folder)
					shutil.copyfile(eachLigand['receptor'], time_folder + os.sep + os.path.basename(eachLigand['receptor']))
				os.mkdir(time_folder + os.sep + eachLigand['compound'])
				ligand_path = time_folder + os.sep + eachLigand['compound'] + os.sep + eachLigand['compound']
				shutil.copyfile(eachLigand['download_complex'] + os.sep + eachLigand['compound'], time_folder + os.sep + eachLigand['compound'] + os.sep + eachLigand['compound'])
				ligandList.append({ 'ligand' : ligand_path, 'receptor' : time_folder + os.sep + os.path.basename(eachLigand['receptor'] )})

			post = get_object_or_404(Process, name=name)
			task_id = async(sleep_task3, workdir,ligandList,post.chain,post.residue_number, post.size, type_docking)
			process = Process.objects.create(name= task_id,
				alive = False,
				folder = workdir,
				protein = os.path.basename(post.protein),
				chain = post.chain,
				residue_number = post.residue_number,
				type_docking = type_docking,
				size = post.size,
				fail = False,
				user_email = post.user_email,
				projectName = post.projectName,
				site=request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id}))
				# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
			process.publish()
			url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id})
			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

			return HttpResponseRedirect(url)
	else:
		form = DockingFormStage2()  # A empty, unbound form
		new_list = request.session['mylist2']
		# print new_list
		compounds = CompoundLite(new_list)
		RequestConfig(request, paginate=False).configure(compounds)
		return render(
			request,
			'dyna/experiment_detail3B.html',
			{'compounds' : compounds, 'form' : form, 'name' : name}
		)

def task_list_user(request, name):	
	posts = Process.objects.all()
	list_waiting = []
	list_processed = []
	for eachPost in posts:
		eachPost.site = "/" + eachPost.site.split("/",1)[1]
		# print eachPost.site
		eachPost.b = False
		if not eachPost.alive and os.path.exists(eachPost.folder) and fetch(eachPost.name) == None:
			list_waiting.append(eachPost)
		elif (eachPost.name == name):
			list_processed.append(eachPost)
	return render(
			request,
			'dyna/task_list_user.html',
			{'posts': list_waiting, 'name' : name, 'processed' : list_processed}
		)
def _get_complex(workdir, experiment_id,time, compound, pose):
		post = get_object_or_404(Process, name=experiment_id)

		best_model_path = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
		best_model = open(best_model_path, 'r')
		list_receptor = best_model.readlines()

		# print post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg"
		parser = DlgParser(post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg")
		list_compound = parser.run_models[int(pose)]
		text_compoundPDB =  "\n"
		text_compound =  "\n"
		atom_num = 1

		for eachLine in list_receptor:
			atom_serial_number = str(atom_num).rjust(5)
#			text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:54] + "\n"
			text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:21] + "R" + eachLine[22:54] + "\n"
			atom_num = atom_num + 1

		for eachLine in list_compound:
			text_compound = text_compound + eachLine + "\n"
			if eachLine.startswith("ATOM"):
				text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:17] + "<0> L" + eachLine[22:54] + "\n"

		complex_file = file(workdir + os.sep + 'complex.pdb', "w")
		complex_file.write(text_compoundPDB)
		complex_file.close()
		return workdir + os.sep + 'complex.pdb'


		# response = HttpResponse("".join(text_compoundPDB), content_type='text/plain')
		# response['Content-Disposition'] = 'attachment; filename=complex_' + compound.split(".")[0] + "_" + time.split('.')[0] + "_" + pose + '.pdb'
		# return response

def mmpbsa_analisys(request, name):
	workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep
	mylist = request.session['mylist2']
	# post = get_object_or_404(Process, name=name)
	experiment_id = name
	selected_pose = mylist[0]
	print selected_pose
	time = selected_pose['conformation']
	print time
	compound = selected_pose['compound']
	print compound
	pose = selected_pose['pose_number']
	print pose

    # path = _get_complex(workdir, experiment_id,'Control', 'trialxx2113.pdbqt', '17')

	path = _get_complex(workdir, experiment_id,time, compound, pose)
	post = get_object_or_404(Process, name=experiment_id)
	# mmpbsa(path)
	task_id = async(mmpbsa, path)
	process = Process.objects.create(name= task_id,
		alive = False,
		folder = workdir,
		protein = os.path.basename(path),
		chain = post.chain,
		residue_number = post.residue_number,
		type_docking = post.type_docking,
		size = post.size,
		fail = False,
		user_email = post.user_email,
		projectName = post.projectName,
		site=request.META['HTTP_HOST'] + reverse("mmpbsa_result",  kwargs={'name': task_id}))
	process.publish()
	url = "http://" + request.META['HTTP_HOST'] + reverse("mmpbsa_result",  kwargs={'name': task_id})

	return HttpResponseRedirect(url)

def mmpbsaimg(request, name):
	post = get_object_or_404(Process, name=name)
	workdir = post.folder
	mycluster_img = workdir + os.sep + "binding_energy.png"
	response = HttpResponse(content_type="image/png")
	im = Image.open(mycluster_img)
	im.save(response, "PNG")
	return response

def mmpbsa_result(request, name):
		post = get_object_or_404(Process, name=name)
		# free_space = subprocess.check_output(['df','-h', TEMPORARYFOLDER]).split('\n')[1].split()[3].decode('utf-8')
		compounds = []
		has_mmpbsaimg=False
		has_summary=False
		sumary_lines = ""
		if os.path.exists(post.folder + os.sep + "summary_energy.dat"):
			sumary_file = file(post.folder + os.sep + "summary_energy.dat", 'r')
			lines = sumary_file.readlines()
			sumary_lines = "\n".join(lines[7:-4])
			sumary_file.close()
			has_summary=True

		if os.path.exists(post.folder + os.sep + "binding_energy.png"):
			has_mmpbsaimg = True
		return render(
			request,
			'dyna/mmpbsa_analisys.html',
			{ 'post' : post, 'sumary_lines': sumary_lines, 'has_mmpbsaimg' : has_mmpbsaimg, 'has_summary' : has_summary}
		)
	
# def mmpbsa_debug(request, name):
# 	post = get_object_or_404(Process, name=name)
	


class Compound(tables.Table):


		experiment_id = tables.Column(visible=False)
		View_Complex = tables.LinkColumn(text = "view",viewname='show_pose', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
		pose_number = tables.Column()
		compound = tables.Column()
		ki = tables.Column(verbose_name='KI (Molar)')
		conformation = tables.Column()
		binding_energy = tables.Column(verbose_name = "∆G (Kcal/mol)")

		template = '<input type="checkbox" name="bbb" value="{{ record.compound }}' + '*' + '{{ record.conformation }}" />'


		download_complex = tables.LinkColumn(text = "Download!", viewname='download_protein_ligand', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
		refine = tables.TemplateColumn(template, verbose_name='Post-Processing')
		class Meta:
			template_name = 'django_tables2/bootstrap.html'
			table_pagination = False
			# # model = vars
			# fields = ('idvars')
			attrs = {
			'th' : {
					'_ordering': {
						'orderable': 'sortable', # Instead of `orderable`
						'ascending': 'ascend',   # Instead of `asc`
						'descending': 'descend'  # Instead of `desc`
					}
				}
			}



# def start_experiments_after_start():
# 	posts = Process.objects.all()
# 	for eachPost in posts:
# 		# eachPost.b = False
# 		if not eachPost.alive:

# 			filename = eachFolder.folder + os.sep + eachPost.protein

# 			# ligandList = ['sample_15_lig_85_dec_hace_c.tar.gz']
# 			name = eachPost.name

# 			type_docking = eachPost.type_docking
# 			chainData = eachPost.chain
# 			residue_number = int(eachPost.residue_number)
# 			size = float(eachPost.size)	
# 			cluster_cutoff = eachPost.cluster_cutoff

# 			ligandList = []

# 			for eachLigand in LigandFile.objects.all().filter(name_of_Process=name):
# 				ligandList.append(eachLigand.filename)


# 			task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)


class CompoundLite(tables.Table):


		experiment_id = tables.Column(visible=False)
		pose_number = tables.LinkColumn(viewname='show_pose', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
		compound = tables.Column()
		ki = tables.Column(verbose_name='KI (Molar)')
		conformation = tables.Column()
		binding_energy = tables.Column(verbose_name = "∆G (Kcal/mol)")

		# template = '<input type="checkbox" name="bbb" value="{{ record.compound }}' + '*' + '{{ record.conformation }}" />'

		# refine = tables.TemplateColumn(template)

		download_complex = tables.LinkColumn(text = "Download!", viewname='download_protein_ligand', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
		class Meta:
			template_name = 'django_tables2/bootstrap.html'
			table_pagination = False
			# # model = vars
			# fields = ('idvars')
			attrs = {
			'th' : {
					'_ordering': {
						'orderable': 'sortable', # Instead of `orderable`
						'ascending': 'ascend',   # Instead of `asc`
						'descending': 'descend'  # Instead of `desc`
					}
				}
			}

def convert_mol2_to_pdbqt(request):
	form = ConvertForm(request.POST, request.FILES)
	if request.method == 'POST':
		convertTask = ConvertMol2Task()
		convertTask.prepare_workdir()
		mol2Filename = request.FILES['mol2File'].name
		mol2Chunks = request.FILES['mol2File'].chunks()
		convertTask.write_file(mol2Filename, mol2Chunks)
		convertTask.set_parameters(mol2Filename)
		zip_path = convertTask.run()
		zip_file = open(zip_path, 'r')
		response = HttpResponse(zip_file, content_type='application/force-download')
		response['Content-Disposition'] = 'attachment; filename=' + "compound.zip"
		return response



		return render(request, 'dyna/hello_world.html')
	else:
		return render(request,
			'dyna/convert_mol2_to_pdbqt.html',
			{'form': form}
			)
