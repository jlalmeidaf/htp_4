import os, shutil, tempfile
from datetime import date, datetime
from .hooks import sleep_task, sleep_task3, sleep_task4 
from .models import Process, LigandFile 
from django_q.tasks import async
from zipfile import ZipFile
from Bio.PDB import PDBParser
import tarfile, uuid
from django.contrib.staticfiles.storage import staticfiles_storage
from dyndock_engine import Docking
import zipfile

from dyndock import settings

TEMPORARYFOLDER=settings.TEMPORARYFOLDER
# TEMPORARYFOLDER="/mnt/sda5/htp_temp"


#Exceptions 
class ManyLigandsException(Exception):
	pass

class ParameterNotSet(Exception):
	pass

class BadPDBException(Exception):
	pass

class BadPDBStructureException(BadPDBException):
	pass		

class ReceptorDontStartInResidueOneException(BadPDBException):
	pass

class ANISOUException(BadPDBException):
	pass

class ResidueMissingException(BadPDBException):
	pass		

#Main classes

class ExperimentController(object):
	def __init__(self):
		# super(ExperimentController, self).__init__()
		# self.post = post
		self.workdir = None
		self.email = None
		self.projectName = None
		self.type_docking = None
		self.chainData = None
		self.residue_number = None
		self.size = None
		self.cluster_cutoff = None
		self.receptor = None
		self.task_id = None
		self.ligandList = None
		self.alive = None
		self.site = "www.google.com"
		self.created_date = None
		self.terminated_date = None

	def prepare_workdir(self):
		self.workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep
	
	def set_parameters(self, receptor ,email, projectName, type_docking, chainData, residue_number, size, cluster_cutoff, ligandList):
		self.email = email
		self.projectName = projectName
		self.type_docking = type_docking
		self.chainData = chainData
		self.residue_number = residue_number
		self.size = size
		self.cluster_cutoff = cluster_cutoff
		self.receptor = os.path.basename(receptor)
		self.ligandList = ligandList

	def set_site(self, site):
		self.site = site

	def load(self, post):
		self.email = post.user_email
		self.task_id = post.name
		self.type_docking = post.type_docking
		self.chainData = post.chain
		self.residue_number = post.residue_number
		self.size = post.size
		self.cluster_cutoff = post.cluster_cutoff
		self.receptor = os.path.basename(post.protein)
		self.workdir = post.folder
		self.alive = post.alive
		self.fail = post.fail
		self.site = post.site
		self.projectName = post.projectName
		self.created_date = post.created_date
		self.terminated_date = post.terminated_date
	def save_parameters(self): 
		if self.task_id == None or self.workdir == None:
			raise ParameterNotSet()
		else:
			process = Process.objects.create(name= self.task_id,
						alive = False,
						folder = self.workdir,
						protein = self.workdir + os.sep + self.receptor,
						chain = self.chainData,
						residue_number = self.residue_number,
						type_docking = self.type_docking,
						size = self.size,
						fail = False,
						user_email = self.email,
						projectName = self.projectName,
						cluster_cutoff = self.cluster_cutoff,
						site = self.site
						)
			process.publish()

			for eachLigandFile in self.ligandList:
					ligand = LigandFile.objects.create(name_of_Process = self.task_id,
						filename = eachLigandFile)
					ligand.publish()

	def write_file(self, filename, chunks):
		full_path_file_name = self.workdir + filename
		structureFile = file(full_path_file_name, "wb+")

		for chunk in chunks:
			structureFile.write(chunk)
		structureFile.close()

	def write_ligands(self, ligandList):
			ligand_count = 0


			for eachLigand in ligandList:
				filename = self.workdir + os.path.basename(eachLigand.name)
				chunks = eachLigand.chunks()
				self.write_file(eachLigand.name, chunks)
				if filename.endswith(".zip"):
					archive = ZipFile(filename)
					ligand_count = ligand_count + len(archive.infolist()) -1

				elif filename.endswith("gz"):
					t = tarfile.open(filename,'r')
					ligand_count = ligand_count + len(t.getmembers()) -1
				else:
					ligand_count = ligand_count + 1
			if ligand_count > 100:
				raise ManyLigandsException("Ligand Count exceeds limit")

	def run_experiment(self):
		self.task_id = async(sleep_task, self.workdir + os.sep + self.receptor,self.ligandList,self.chainData,self.residue_number, self.size, self.type_docking, self.cluster_cutoff)


	def stop_experiment(self):
		pass

	def run_refinement_experiment(self, type_docking, compound_list):

		refinementExperiment = RefinementExperimentController()
		refinementExperiment.prepare_workdir()
		list_of_compounds = refinementExperiment.copy_files(compound_list)
		refinementExperiment.set_parameters(self.receptor, self.email, self.projectName, type_docking, self.chainData, self.residue_number, self.size, self.cluster_cutoff, list_of_compounds)
		refinementExperiment.run_experiment()
		refinementExperiment.set_site(self.site)
		refinementExperiment.save_parameters()

		return refinementExperiment.task_id

	def get_data_from_dockings(self):
		list_of_compounds = []

		if(os.path.exists(self.workdir + os.sep + "compound_list.csv")):
			compound_list = file(self.workdir + os.sep + "compound_list.csv", 'r')
			lines = compound_list.readlines()
			compound_list.close()

			for eachLine in lines:
				line_in_table = eachLine.split("\t")

				z = {'ki': float(line_in_table[1]), "conformation": line_in_table[7], "compound": line_in_table[0],   'select':line_in_table[2], 'experiment_id': line_in_table[3], 
				'pose_number' : int(line_in_table[4]), 'download_complex' : line_in_table[5], 'binding_energy': float(line_in_table[8]), 'receptor' : line_in_table[6]}

				list_of_compounds.append(z)
		return list_of_compounds

	def delete(self):
		pass

	def size_of_workdir(self):
		pass

	def has_cluster(self):
		return os.path.exists(self.workdir + os.sep + 'cluster100ns.eps')

	def check_receptor_integrity(self):
		#Checa atomos duplicados
		#Checa estrutura correta
		self._check_anisotropy()
		self._check_receptor_structure()
		self._check_if_receptor_residue_start_one()
		self._check_if_any_residues_are_missing()


	def _check_anisotropy(self):
		receptor_file = file(self.workdir + os.sep + self.receptor, 'r') 
		lines = receptor_file.readlines()

		receptor_file.close()
		new_lines = []
		for eachLine in lines:
			if eachLine.startswith("ANISOU"):
				raise ANISOUException()

	def _check_if_any_residues_are_missing(self):
		lines = self._get_data_lines()
		chain = None
		residue = 1
		for eachLine in lines:
			print eachLine
			if chain == self._get_chain(eachLine):
				if self._get_residue_number(eachLine) != residue+1 and self._get_residue_number(eachLine) != residue:
					raise ResidueMissingException()
				else:
					residue = self._get_residue_number(eachLine)
			else:
				residue = self._get_residue_number(eachLine)
				chain = self._get_chain(eachLine)

	def _check_receptor_structure(self):
		pdb_parser = PDBParser(PERMISSIVE=0)
		try:
			structure = pdb_parser.get_structure('htp', self.workdir + os.sep + self.receptor)
			if len(structure.get_list()) == 0:
				raise BadPDBStructureException()
		except Exception as e:
			raise BadPDBStructureException(e.message)

	def _check_if_receptor_residue_start_one(self):
		lines = self._get_data_lines()
		chain = None

		for eachLine in lines:
			if chain != self._get_chain(eachLine):
				chain = self._get_chain(eachLine)
				if self._get_residue_number(eachLine) != 1:
					# print eachLine
					raise ReceptorDontStartInResidueOneException()

	def _get_chain(self,line):
		# if not line.startswith("TER"):
			return line[21]
		# else:
			# return line[22]


	def _get_data_lines(self):
		receptor_file = file(self.workdir + os.sep + self.receptor, 'r') 
		lines = receptor_file.readlines()

		receptor_file.close()
		new_lines = []
		for eachLine in lines:
			# print eachLine
			if eachLine.startswith("ATOM") or eachLine.startswith("HETATOM") or eachLine.startswith("TER"):
			# if eachLine.startswith("ATOM") or eachLine.startswith("HETATOM"):

				new_lines.append(eachLine)
		return new_lines
	def _get_residue_number(self, line):
			return int(line[23:26].split()[0])

# class FixReceptorController(ExperimentController):
# 	"""docstring for ClassName"""
# 	def __init__(self, arg):
# 		super(ClassName, self).__init__()
# 		self.arg = arg
		

class FixReceptorTask(ExperimentController):
	def __init__(self):
		self.receptor = None

	def  set_parameters(self, receptor):
		self.receptor = receptor

	def run(self):
		self.__normalize_pdb_residues_number()
		return self.workdir + os.sep + self.receptor + ".new"


	def __normalize_pdb_residues_number(self):
		lines_in_pdb = self._get_data_lines()

		chain = ""

		# number_of_first_residue = 1
		new_lines = []
		diference = 0
		for eachLine in lines_in_pdb:
			if chain != self._get_chain(eachLine):
				chain = self._get_chain(eachLine)
				number_of_first_residue = self._get_residue_number(eachLine)
				# print number_of_first_residue
				if not number_of_first_residue == 1:
					diference = number_of_first_residue - 1

			number_residue = self._get_residue_number(eachLine) - diference 
			new_lines.append(self._fix_residue_number(eachLine, number_residue))

		self._build_new_pdb(new_lines)


	

	# def _get_chain(self,line):
	# 	if not line.startswith("TER"):
	# 		return line[21]
	# 	else:
	# 		return line.split()[3]

	def _build_new_pdb(self, lines):
		header = ['EXPDATA	NORMALIZED DIGITAL STRUCTURE, HTP SURFLEXDOCK 3.11' + " " + datetime.now().strftime("%d/%m/%Y %H:%M:%S") + "\n"]
		receptor_file = file(self.workdir + os.sep + self.receptor + ".new", 'w') 
		receptor_file.writelines(header + lines)
		receptor_file.close()

	def _fix_residue_number(self, line, new_number):
			residue_number = str(new_number).rjust(4)
			text_compoundPDB =  line[0:22] + residue_number + line[26:]
			return text_compoundPDB

	# def _get_data_lines(self):
	# 	receptor_file = file(self.workdir + os.sep + self.receptor, 'r') 
	# 	lines = receptor_file.readlines()

	# 	receptor_file.close()
	# 	new_lines = []
	# 	for eachLine in lines:
	# 		# print eachLine
	# 		if eachLine.startswith("ATOM") or eachLine.startswith("HETATOM") or eachLine.startswith("TER"):
	# 		# if eachLine.startswith("ATOM") or eachLine.startswith("HETATOM"):

	# 			new_lines.append(eachLine)
	# 	return new_lines

		
class ConvertMol2Task(ExperimentController):
	def __init__(self):
		# super(ConvertMol2Task, self).__init__()
		self.mol2File = None

	def run(self):
		list_of_molecules = self.splitMol2()
		list_of_pdbqts = self.convertPDBQT(list_of_molecules)
		zip_path = self.compact_pdbqts(list_of_pdbqts)
		return zip_path

	def splitMol2(self):
		list_of_molecules = []
		mol2file_path = self.workdir + os.sep + self.mol2File
		mol2file = file(mol2file_path, 'r')
		data = mol2file.readlines()
		mol2file.close()
		new_data = "\n".join(data)
		molecules = new_data.split("@<TRIPOS>MOLECULE\n")
		for eachMolecule in molecules[1:]:
			new_file_mol2_filename = self.workdir + os.sep + eachMolecule.split('\n')[1] + '.mol2'
			list_of_molecules.append(new_file_mol2_filename)
			new_file_mol2 = file(new_file_mol2_filename, 'w')
			new_file_mol2.write('@<TRIPOS>MOLECULE\n')
			new_file_mol2.write(eachMolecule)
			new_file_mol2.close()
		return list_of_molecules

	def convertPDBQT(self, list_of_molecules):
		list_of_pdbqts = []
		for eachMolecule in list_of_molecules:
			self.runPrepareLigand(eachMolecule)
			# print eachMolecule.split[:-3] + ".pdbqt"
			list_of_pdbqts.append(eachMolecule[:-5] + ".pdbqt")
		return list_of_pdbqts

	def runPrepareLigand(self,ligand):
		path = self.workdir + self.mol2File
		dockingStub = Docking.Docking(path, path)
		dockingStub.convert_mol2_to_pdbqt(ligand)

	def compact_pdbqts(self,list_of_pdbqts):
		temp = self.workdir + os.sep + "compounds.zip"
		z = zipfile.ZipFile(temp, "w", zipfile.ZIP_DEFLATED)
		for eachLigand in list_of_pdbqts:
			z.write(eachLigand, os.path.basename(eachLigand))
		return temp


	def set_parameters(self, mol2File):
		self.mol2File = mol2File
		

class RefinementExperimentController(ExperimentController):
	def __init__(self):
		super(RefinementExperimentController, self).__init__()

	def copy_files(self, ligand_list):
		ligandList = []
		for eachLigand in ligand_list:
			time_folder = self.workdir + os.sep + os.path.basename(os.path.dirname(eachLigand['download_complex']))
			if not os.path.exists(time_folder):
				os.mkdir(time_folder)
			shutil.copyfile(eachLigand['receptor'], time_folder + os.sep + os.path.basename(eachLigand['receptor']))
			ligand_path = time_folder + os.sep + eachLigand['compound'] + os.sep + eachLigand['compound']
			if not os.path.exists(time_folder + os.sep + eachLigand['compound']):
				os.mkdir(time_folder + os.sep + eachLigand['compound'])
			shutil.copyfile(eachLigand['download_complex'] + os.sep + eachLigand['compound'], time_folder + os.sep + eachLigand['compound'] + os.sep + eachLigand['compound'])
			ligandList.append({ 'ligand' : ligand_path, 'receptor' : time_folder + os.sep + os.path.basename(eachLigand['receptor'] )})
		return ligandList

	def set_parameters(self, receptor ,email, projectName, type_docking, chainData, residue_number, size, cluster_cutoff, ligandList):
		new_path_receptor = self.workdir + os.sep + os.path.basename(receptor)
		super(RefinementExperimentController, self).set_parameters(new_path_receptor ,email, projectName, type_docking, chainData, residue_number, size, cluster_cutoff, ligandList)

	def run_experiment(self):
		self.task_id = async(sleep_task3, self.workdir, self.ligandList, self.chainData, self.residue_number, self.size, self.type_docking)
		# self.task_id = uuid.uuid1().hex

class SampleExperimentController(ExperimentController):
	def __init__(self):
		super(SampleExperimentController, self).__init__()

	def copy_sample_str(self):
		self.receptor = 'hACEcOk.pdb'
		shutil.copyfile( staticfiles_storage.path('example/hACEcOk.pdb'), self.workdir + os.sep + self.receptor)
		return self.receptor
		
	def copy_sample_ligands(self):
		self.ligand_count = [ "sample_15_lig_85_dec_hace_c.tar.gz" ]
		shutil.copyfile( staticfiles_storage.path('example/sample_15_lig_85_dec_hace_c.tar.gz'), self.workdir + os.sep + 'sample_15_lig_85_dec_hace_c.tar.gz')
		return self.ligand_count


		
