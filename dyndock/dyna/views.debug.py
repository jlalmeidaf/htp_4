def log(request, name):
	alog = get_object_or_404(Process, name=name)
	x = alog.folder + os.sep + "commands.txt"
	print x
	zz = file(x,'r')
	k = "\n".join(zz.readlines())
	zz.close()
	context = {'experiment_id': k}
	return render(request, 'dyna/activeLog.html', context)

def activeLog(request, name):
	alog = get_object_or_404(ActiveLog, name=name)
	x = alog.path
	print x
	zz = file(x,'r')
	k = "\n".join(zz.readlines())
	context = {'experiment_id': k}
	return render(request, 'dyna/activeLog.html', context)


def debug(request, name):
	instance = get_object_or_404(Process, name=name)
	last_command_list = os.path.dirname(instance.protein) + os.sep + "commands.txt"
	last_command = None
	number_of_runned_steps = 0
	if os.path.exists(last_command_list):
		last_command_stub = file(last_command_list,'r')
		lines_of_command_file = last_command_stub.readlines()
		last_command_stub.close()
		last_command = lines_of_command_file[-1]
		number_of_runned_steps = len(lines_of_command_file)
	
	count_ligands = 0
	count_processed_ligands = 0
	clusters = []
	cluster_todos = []
	count_ligands_in_each_cluster = []
	error_ligands_in_each_cluster = []
	for eachFile in os.listdir(instance.folder):
		if os.path.isdir(instance.folder + os.sep + eachFile) and (not eachFile ==  "Control_folder"):
			clusters.append(eachFile)
		if eachFile.endswith(".pdbqt"):
			count_ligands += 1
		if os.path.isdir(instance.folder + os.sep + eachFile):
			cluster_count = 0
			error_ligands = 0
			cluster_todos.append(eachFile)
			for eachF in os.listdir(instance.folder + os.sep + eachFile):
				if os.path.exists(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'ligant_receptor.dlg'):
					count_processed_ligands += 1
					cluster_count += 1
				if os.path.exists(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'last_output.txt'):
					myfile = file(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'last_output.txt', 'r')
					try:
						erro_list = myfile.readlines()[0]
						
						# print erro_list
						if erro_list != '/usr/local/bin//autodock4: WARNING: Using autodock4.0 unbound extended model in autodock4.2!\n':
							error_ligands +=1
						pass
					except Exception as e:
						pass
					myfile.close()
			count_ligands_in_each_cluster.append(cluster_count)
			error_ligands_in_each_cluster.append(error_ligands)

	missing_simulation_files = check_simulation_files(instance.folder, os.path.basename(instance.protein), clusters)
	
	twi = zip(cluster_todos, count_ligands_in_each_cluster, error_ligands_in_each_cluster)

	return render(request, 'dyna/debug.html', {'twi' : twi, 'clusterB' : cluster_todos, 'count_ligands_in_each_cluster' : count_ligands_in_each_cluster, 'count_processed_ligands' : count_processed_ligands, 'count_ligands': count_ligands, 'clusters': clusters, 'number_of_runned_steps' : number_of_runned_steps , 'missing_files' : missing_simulation_files,'last_command' : last_command, 'post' : instance})

def dockingLogFiles(request, name, time, compound):
	instance = get_object_or_404(Process, name=name)
	real_path_commands = instance.folder + os.sep + time	 + os.sep + compound + os.sep + 'commands.txt'
	real_path = instance.folder + os.sep + time	 + os.sep + compound + os.sep + 'last_output.txt'
	k = ""
	if os.path.exists(real_path_commands):
		commands_file = file(real_path_commands, 'r')
		k = ">>>> Commands executed: \n\n" + "\n".join(commands_file.readlines())

	logfile = file(real_path, 'r')
	k = k + "\n\n\n>>>> The last command caused the following error:\n\n" + "\n".join(logfile.readlines())
	logfile.close()
	context = {'experiment_id': k}
	return render(request, 'dyna/activeLog.html', context)


def dockingLog(request, name, time):
	instance = get_object_or_404(Process, name=name)
	real_path = instance.folder + os.sep + time
	docking_with_erros = []
	for eachFile in os.listdir(real_path):

		if os.path.isdir(real_path + os.sep + eachFile):
			full_path = real_path + os.sep + eachFile
			if os.path.exists(full_path + os.sep + 'last_output.txt'):
				myfile = file(full_path + os.sep +  'last_output.txt', 'r')

				erro_list = myfile.readlines()
				first_line = erro_list[0]
				myfile.close()
				
				if first_line != '/usr/local/bin//autodock4: WARNING: Using autodock4.0 unbound extended model in autodock4.2!\n':
					docking_with_erros.append(eachFile)

	return render(request, 'dyna/dockingLog.html', {'name': name, 'time': time, 'docking_with_erros' : docking_with_erros})
def simulationLog(request, name):
	instance = get_object_or_404(Process, name=name)
	list_of_logs = ['_em.log', '_md.log', '_npt.log']
	list_of_finded_logs = []
	for eachLogFile in list_of_logs:
		filename = instance.protein + eachLogFile
		if os.path.exists(filename):
			list_of_finded_logs.append(os.path.basename(filename))

	list_of_finded_logs.append('cluster.log')
	return render(request, 'dyna/show_logs.html', { 'list_of_files' : list_of_finded_logs, 'post' : instance})

def readLog(request, name, logname):
	if logname.endswith(".log"):
		instance = get_object_or_404(Process, name=name)
		folder = instance.folder 
		logfilename = folder + os.sep + logname
		logfile = file(logfilename, 'r')
		k = "\n".join(logfile.readlines())
		logfile.close()
		context = {'experiment_id': k}
		return render(request, 'dyna/activeLog.html', context)
	else:
		return None

def check_simulation_files(workdir, protein, cluster):
	missing_files = []
	sufix_of_necessary_files = ['.gro', '_s.gro',
	'_ion.tpr', '_ion.gro', '_em.tpr', '_em.gro', '_nvt.tpr', '_nvt.gro',
	'_md.tpr', '_md.trr', '_md.xtc', '_em.trr', '_nvt.trr', '_md.trr', '_cluster.pdb']
	other_necessay_files = ['Integrin.ndx', 'output.ndx', 'ions.mdp', 'topol.top',
	'trajout.xtc', 'rmsd-clust.xpm', 'cluster100ns.eps',
	]
	# print cluster
	for eachFolder in cluster:
		folder = workdir + os.sep + eachFolder
		sufix_of_necessary_filesB = ['_md.tpr', '_md.pdb', '_em.gro', '_em.top',
		 '_em.tpr']
		other_files = ['l-bfgs_after_md.mdp', 'Integrin.ndx']
		for eachFile in sufix_of_necessary_filesB:
			path_name = folder + os.sep + protein + eachFile
			if not os.path.exists(path_name):
				missing_files.append(path_name)
		for eachFile in other_necessay_files:
			path_name = workdir + os.sep + eachFile
			if not os.path.exists(path_name):
				missing_files.append(path_name)

	for eachFile in sufix_of_necessary_files:
		path_name = workdir + os.sep + protein + eachFile
		if not os.path.exists(path_name):
			missing_files.append(path_name)

	for eachFile in other_necessay_files:
		path_name = workdir + os.sep + eachFile
		if not os.path.exists(path_name):
			missing_files.append(path_name)
	return missing_files

def log(request, name):
	alog = get_object_or_404(Process, name=name)
	x = os.path.dirname(alog.protein) + os.sep + "commands.txt"
	# print x
	zz = file(x,'r')
	k = "\n".join(zz.readlines())
	zz.close()
	context = {'experiment_id': k}
	return render(request, 'dyna/activeLog.html', context)