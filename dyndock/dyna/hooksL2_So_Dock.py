from dyndock_engine import Dynamics,Docking
import os, shutil,  tarfile
from billiard import Process as Processing
import billiard as multiprocessing
from dyndock import settings
NT_CPU = settings.NT_CPU

os.environ['MGLTOOLS'] = settings.MGLTOOLS
os.environ['AUTODOCKFOLDER'] = settings.AUTODOCKFOLDER

def docking_with_receptor(args):
	workdir = args[0]
	md_receptor = args[1]
	eachLigand = args[2]
	chain = args[3]
	residue_number = args[4]
	size = args[5]
	type_docking = args[6]
	print workdir
	# print eachLigand
	# print md_receptor

	docking_workdir = os.path.dirname(eachLigand)
	print docking_workdir
	# docking_receptor = docking_workdir + os.sep + os.path.basename(protein_pdb)
	# os.mkdir(docking_workdir)

	shutil.copyfile(md_receptor, docking_workdir + os.sep + os.path.basename(md_receptor))
	# workdir + os.sep + eachLigand

	# shutil.copyfile(os.path.dirname(workdir) + os.sep + eachLigand, docking_workdir + os.sep + eachLigand)
	md_receptr_in_docking_workdir = docking_workdir + os.sep + os.path.basename(md_receptor)
	try:
		dockingTask = Docking.Docking(md_receptr_in_docking_workdir, eachLigand)
		dockingTask.remove_heteroatoms()
		dockingTask.prepare_ligand()
		
		dockingTask.prepare_receptor()
		dockingTask.add_hetatms_pdbqt(docking_workdir + os.sep + "receptor.pdbqt")
		# print residue_number
		pos = dockingTask._get_Pos_from_residue(residue_number, chain)
		dockingTask.prepare_GPF(size, ",".join(pos))
		dockingTask.prepare_GPF2(size, ",".join(pos))
		dockingTask.run_autogrid()
		dockingTask.prepare_DPF(type_docking)
		dockingTask.run_autodock()

	except Exception as e:
		print e

def run_task(workdir, ligandList,chain,residue_number, size,type_docking):
	# protein_pdbfather_dir = workdir


	process_list = []

	process_list = prepare_control(workdir, ligandList,chain,residue_number, size,type_docking) #comment for diag
	# times = termalize_receptor(protein_pdb, chain, residue_number, size) #comment for diag
	
	# times = ["100.000"] #uncomment for diag
	number_processes = int(NT_CPU)		
	pool = multiprocessing.Pool(number_processes)
	

	# for eachTime in times:
		
	# 	# os.mkdir(father_dir + os.sep + eachTime + "_folder")  #uncomment for diag

	# 	workdir = os.path.dirname(protein_pdb) + os.sep + eachTime + "_folder"
	# 	md_receptor = workdir + os.sep + os.path.basename(protein_pdb) + "_em.pdb" #comment for diag
	# 	# md_receptor = protein_pdb #uncomment for diag



	# 	for eachFile in os.listdir(father_dir):
	# 		if eachFile.endswith(".pdbqt"):
	# 			ligand = eachFile
	# 			process_list.append([workdir, md_receptor, ligand,chain, residue_number, size, type_docking])
	# 		# z = Processing(target=docking_with_receptor, args=(workdir, md_receptor, eachLigand,chain,residue_number, size,type_docking))
	# 		# z.start()
	# 		# docking_with_receptor(workdir, md_receptor, eachLigand,chain,residue_number, size,type_docking)
			
	results = pool.map_async(docking_with_receptor, process_list)
	pool.close()
	pool.join()		


	# for eachLigand in range(0,len(ligandList)):
	# 		# z = process_list[eachLigand]
	# 	z.join()

def prepare_control(workdir, ligandList,chain,residue_number, size,type_docking):
	# original_str_folder =  os.path.dirname(protein_pdb) + os.sep + "Control_folder"

	# os.mkdir(original_str_folder)
	# number_processes = 25		
	# pool = multiprocessing.Pool(number_processes)
	tasks = []
	for eachFile in ligandList:
		# print eachFile
		# if eachFile.endswith(".pdbqt"):
			ligand = eachFile['ligand']
			my_protein_pdb = eachFile['receptor']
			# workdir = original_str_folder 
			# my_protein_pdb = workdir + os.sep + os.path.basename(protein_pdb)
			# shutil.copy(protein_pdb, my_protein_pdb)
			# print ligand
			# print my_protein_pdb
			tasks.append([workdir, my_protein_pdb, ligand,chain, residue_number, size, type_docking])
			# docking_with_receptor(workdir, protein_pdb, eachLigand,chain,residue_number, size,type_docking)
			# docking_with_receptor(tasks[0])
	# results = pool.map_async(docking_with_receptor, tasks)
	# pool.close()
	# pool.join()			

	return tasks


# def termalize_receptor(protein_pdb, chain, residue_number, size):
# 	termalization_phase = Dynamics.Dynamic(protein_pdb)
# 	termalization_phase.generate_topology()
# 	termalization_phase.generate_ndx_of_receptor()
# 	termalization_phase.generate_ndx_of_active_site_from_gro(str(residue_number),size)
# 	termalization_phase.rename_ndx_of_active_site_from_gro()
# 	termalization_phase.prepare_solvatation_box()
# 	termalization_phase.generate_box()	
# 	termalization_phase.prepare_for_generate_ions()
# 	termalization_phase.generate_ions()
# 	termalization_phase.prepare_for_em()
# 	termalization_phase.run_em()
# 	termalization_phase.prepare_termalization_at("285")
# 	termalization_phase.run_termalization()
# 	termalization_phase.prepare_continuation_of_termalization_at("300")
# 	termalization_phase.run_termalization()
# 	termalization_phase.prepare_for_npt()
# 	termalization_phase.run_npt()
# 	termalization_phase.prepare_for_md()
# 	termalization_phase.run_md()
# 	termalization_phase.fix_no_jump()
# 	termalization_phase.concatenate_em_termalization_npt_and_md()
# 	termalization_phase.fix_no_rotate()
# 	# termalization_phase.prepare_ndx_of_active_site(str(residue_number))
# 	# termalization_phase.xtc_file = termalization_phase.protein + "_md.xtc"
# 	# termalization_phase.md_gro_file = termalization_phase.protein + "_md.gro"
# 	# termalization_phase.output_ndx = termalization_phase.workdir + os.sep + "output.ndx"
# 	# termalization_phase.prepare_ndx_of_active_site2()
# 	# termalization_phase.prepare_ndx_of_active_site3()
# 	# termalization_phase.prepare_ndx_of_active_site4()
# 	termalization_phase.clustering()
# 	termalization_phase.convert_xpm_to_ps()
# 	times = termalization_phase.get_pdbs_from_cluster()
# 	for eachTime in times:
# 		print eachTime
# 		termalization_phase.nao_sei(eachTime)

# 	return times




# if __name__ == '__main__':
# 	run_task('/home/jorgehf/joao/softwares/testcamp/test_hooks/BatroxModelMonomerSemAcidoGraxoESulfato.pdb', ['isolated2.pdbqt'], "A", 92, "20", "10")
