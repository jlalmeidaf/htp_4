from smosh.ModelingStep.FindTemplatesStep import FindTemplatesStep
from smosh.ModelingStep.AlignStep import AlignStep
from smosh.ModelingStep.BuildModelStep import BuildModelStep

from smosh.ModelingTools.TemplateProfile import TemplateProfile
from smosh.ModelingTools.GetDataFromPDB import GetDataFromPDB
from smosh.ModelingTools.pdb import pdb
from smosh.ModelingTools.AlignFile import AlignFile
from smosh.ModelingStep.GetSequenceFromPDBStep import GetSequenceFromPDBStep
import shutil, tempfile, os, time
from multiprocessing import Pool

from Bio import SeqIO

def main():
	my_list = []
	with open("sequence.fasta", "rU") as input_handle:
	    sequences = SeqIO.parse(input_handle, "fasta")
	    for eachseq in sequences:
	    	filename = "unpack" + os.sep + eachseq.name.split("|")[1] + ".fasta"

	    	with open(filename,'w') as output_handle:

	        	SeqIO.write(eachseq, output_handle, "fasta")
	        # modeling(filename)
	        my_list.append(filename)
	p = Pool(10)
	p.map(modeling,my_list)


def modeling2(sequence):
	time.sleep(10000)
	print sequence
def get_results(workdir,sequence_name):
		folder = workdir
		mof = 0
		filemof = ""
		for files in os.listdir(folder):
			if (files.startswith(sequence_name + '.B') and files.endswith(".pdb")):
					arqr = file(folder + '/' + files, 'r')
					arqr.readline()
					linha = arqr.readline()
					if (mof == 0) or (linha.partition(':	  ')[2] < mof):
							mof = linha.partition(':	  ')[2]
							filemof = folder + '/' + files
							arqr.close()
		return filemof

def modeling(sequence):
		
	try:
		workdir = tempfile.mkdtemp() + "/"
		shutil.copy(sequence, workdir)
		sequence_file_name = workdir + os.path.basename(sequence)
		findTemplatesManager = FindTemplatesStep(sequence_file_name, 'fasta')
		findTemplatesManager.execute()

		profile_of_templates = TemplateProfile(workdir + 'build_profile.prf')
		list_of_profiles = [] 
		for eachProfile in profile_of_templates.list_of_sequences:
			if len(eachProfile.name()) == 5 and eachProfile.name().endswith("A"):
				list_of_profiles.append(eachProfile)

		if len(list_of_profiles) > 0:


			getdataFromPDB = GetDataFromPDB(workdir, list_of_profiles[0].name())

			getdataFromPDB.getPDB_File()

			downloaded_pdb_path = workdir + os.sep + getdataFromPDB.__onlyTemplateName__() + ".pdb"


			downloaded_pdb = pdb(downloaded_pdb_path)  #bad smell

			new_pdb = downloaded_pdb_path + "m"
			modified_pdb = file(new_pdb, "w")
			chain = "A" #fix
			modified_pdb.write(downloaded_pdb.const({chain: downloaded_pdb.HetatomsInPDB()}, chain))
			modified_pdb.close()
			os.rename(new_pdb, downloaded_pdb_path)


			template_sequence_manager = GetSequenceFromPDBStep(downloaded_pdb_path, chain, chain)
			template_sequence_manager.execute()
			template_sequence = template_sequence_manager.__output_files__[0] #template output

			unaligned_sequence = workdir + 'unaligned_sequence.pir'

			with open(unaligned_sequence, 'w') as outfile:
				with open(sequence_file_name) as sequence_file_in_fasta:
					outfile.write(">P1;seq\nsequence:seq:::::::0.00: 0.00\n")
					for line in sequence_file_in_fasta.readlines()[1:]:
						outfile.write(line)
					outfile.write("*")

				
				with open(workdir + template_sequence) as pdb_sequence_file:
						for line in pdb_sequence_file:
							outfile.write(line)					


			sequence_Manager = AlignFile(unaligned_sequence) #verify
			sequence_Manager.copy_heteroatoms(1,0)
			sequence_Manager.write_changes()
			sequence_Manager.close()


			alignment_manager = AlignStep(unaligned_sequence)
			alignment_manager.execute()


			alignment_path = workdir + alignment_manager.__output_files__[0]

			file_ = file(alignment_path,'r') #ali.ali
			text_ = file_.read()
			file_.close()

			#modeling step


			downloaded_pdb_path = downloaded_pdb_path

			modeling_manager = BuildModelStep(alignment_path, downloaded_pdb_path)
			modeling_manager.execute()

			starting_name_of_files = modeling_manager.__output_files__[0].split(".")[0]
			better_result = get_results(workdir,starting_name_of_files)

			file_ = file(better_result,'r')
			text_ = "\n".join(file_.readlines()[1:])
			file_.close()
		else:
			shutil.copy(sequence, "noTemplate/")
			print "No template"
			
		print "task finished"
	except Exception as e:
		with open("error" + os.sep + os.path.basename(sequence),"w") as error_handle:
			error_handle.write(str(e) + "\n")
			error_handle.write(workdir + "\n")
			error_handle.write(sequence + "\n")
			print "erro"


if __name__ == '__main__':
	main()

