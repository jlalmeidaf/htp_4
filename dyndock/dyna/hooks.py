from time import sleep
import subprocess, shutil
import os, shutil, tempfile
from .models import LigandFile,Process, ActiveLog
from django.utils import timezone
import dyndock.settings as settings
from distutils.dir_util import copy_tree

from billiard import Process as Processing
from dyndock_engine.LogException import LogException 

from .hooksL2_So_Dock import run_task as process2
from .hooksL2 import run_task as process
from .hooksL2 import run_taskD as processD
from .hooksL2 import mmpbsa_analisys
from dyndock import settings
from django.core.urlresolvers import reverse
from django.core.mail import send_mail
import sys
reload(sys)
import time
sys.setdefaultencoding('utf8')

import math


from Bio.PDB import PDBParser
from dyndock import settings

os.environ['MGLTOOLS'] = settings.MGLTOOLS
os.environ['AUTODOCKFOLDER'] = settings.AUTODOCKFOLDER
sys.path.append(os.environ['MGLTOOLS'] + '/MGLToolsPckgs/')
from AutoDockTools.DlgParser import DlgParser

def generate_table(workdir, receptor_path, name):
			# post = get_object_or_404(Process, name=name)
			# name = "None"
			list_of_compounds = []
			for eachFolder in os.listdir(workdir):
				path_name = workdir + os.sep + eachFolder
				if os.path.isdir(path_name) and eachFolder != "Logs":
					for eachFile in os.listdir(path_name):
						int_path = path_name + os.sep + eachFile

						if os.path.isdir(int_path):
							if os.path.exists(int_path  + os.sep + "ligant_receptor.dlg"):

										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
										list_of_folder_name =  dlg_file.split(os.sep)
										ligand_name = list_of_folder_name[-2]
										time_name = list_of_folder_name[-3]
										parser = DlgParser(dlg_file)
										mylist = {}
										for eachStruct in parser.clist:
											if 'inhib_constant_units' in eachStruct:
												if eachStruct['inhib_constant_units'] == 'uM':
													mag = math.pow(10,-6)
										 		elif eachStruct['inhib_constant_units'] == 'mM':
													mag = math.pow(10,-3)
												elif eachStruct['inhib_constant_units'] == 'nM':
													mag = math.pow(10,-9)
												elif eachStruct['inhib_constant_units'] == 'pM':
													mag = math.pow(10,-12)
												elif eachStruct['inhib_constant_units'] == 'fM':
													mag = math.pow(10,-15)
												elif eachStruct['inhib_constant_units'] == 'aM':
													mag = math.pow(10,-18)
												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
										better_Ki = math.pow(10,0)
										docking_id_better_Ki = None
										better_binding_energy = 100
										for eachElem in mylist:

											ki = mylist[eachElem][0]
											binding_energy = mylist[eachElem][1]

											if binding_energy <  better_binding_energy:
												docking_id_better_Ki = eachElem
												better_Ki = ki
												better_binding_energy = binding_energy
										if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(receptor_path) + "_em.pdb"):
											z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(receptor_path) + "_em.pdb"}
										else:
											z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(receptor_path)}
										list_of_compounds.append(z)

										compound_list = file(workdir + os.sep + "compound_list.csv", 'w')
										for eachCompound in list_of_compounds:
											compound_list.write(eachCompound["compound"] + "\t" + str(eachCompound["ki"]) + "\t" + 
											str(eachCompound["select"]) + "\t" +  eachCompound["experiment_id"] + "\t" +  str(eachCompound["pose_number"]) + "\t" + 
											eachCompound["download_complex"] + "\t" +  eachCompound["receptor"] + "\t" +  eachCompound["conformation"] + "\t" +  str(eachCompound["binding_energy"]) + "\n")


										compound_list.close()
	# for eachLigand in range(0,len(ligandList)):
	# 		# z = process_list[eachLigand]
	# 	z.join()

def sleep_task(protein_pdb, ligandList,chain,residue_number, size,type_docking, cluster_cutoff):
	time.sleep(2)
	processDB = Process.objects.get(folder=os.path.dirname(protein_pdb) + "/")
	try:
		# print settings.EMAIL_HOST_USER
		if settings.USE_EMAIL_BACKEND:
			send_mail(
	'HTP SurflexDock Task Created',
	'Your request to the server was accepted. You can follow the process on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)
		# print cluster_cutoff
		process(protein_pdb, ligandList,chain,int(residue_number), str(size),type_docking, cluster_cutoff)
		generate_table(processDB.folder, protein_pdb, processDB.name)
	except LogException as e:
		processDB.fail = True
		alog = ActiveLog.objects.create(name= processDB.name,
				path=e.path)
		alog.save()
		if settings.USE_EMAIL_BACKEND:
			send_mail(
	    'HTP SurflexDock Task Failed',
	    'Your request was failed. You can access the results on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)

	except Exception as e:
		print "erro"
		print e

		processDB.fail = True
		alog = ActiveLog.objects.create(name= processDB.name,
				path=processDB.folder + os.sep + "error_file.txt")
		alog.save()
		my_file = file(os.path.dirname(protein_pdb) + os.sep + "error_file.txt","w")
		my_file.write(e)
		my_file.close()

		if settings.USE_EMAIL_BACKEND:
			send_mail(
	    'HTP SurflexDock Task Failed',
	    'Your request was failed. You can access the results on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)

	processDB.terminated_date = timezone.now()
	processDB.alive = True

	processDB.publish()
	if settings.USE_EMAIL_BACKEND:
		send_mail(
	    'HTP SurflexDock Task Finished',
	    'Your request was finished. You can access the results on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,
)
	# return True

def print_task():
	pass
#		send_mail(
#	    'HTP SurflexDock Task Finished',
#	    'Your request was finished. You can access the results on ' + "http://" + processDB.site,
#	    settings.EMAIL_HOST_USER,
#	    [processDB.user_email],
#	    fail_silently=False,
#)
# def process(protein_pdb, ligandList,chain,residue_number, size,type_docking):
# 	sleep(100)
# 	raise Exception("Resultado diferente 0") 

def sleep_task2():
#		send_mail(
#	    'HTP SurflexDock Task Finished',
#	    'Your request was finished. You can access the results on ' + "http://" + processDB.site,
#	    settings.EMAIL_HOST_USER,
#	    [processDB.user_email],
#	    fail_silently=False,
#)
	pass
def sleep_task3(workdir,ligandList, chain,residue_number, size,type_docking):
	time.sleep(2)
	processDB = Process.objects.get(folder=workdir)
	try:
		if settings.USE_EMAIL_BACKEND:
			send_mail(
	'HTP SurflexDock Task Created',
	'Your request to the server was accepted. You can follow the process on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)
		process2(workdir, ligandList,chain,int(residue_number), str(size),type_docking)
	except LogException as e:
		processDB.fail = True
		alog = ActiveLog.objects.create(name= processDB.name,
				path=e.path)
		alog.save()
		if settings.USE_EMAIL_BACKEND:
			send_mail(
	    'HTP SurflexDock Task Failed',
	    'Your request was failed. You can access the results on ' + "http://" + processDB.site  + ' The error is caused by: \n' + e,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)

	except Exception as e:
		print "erro"
		print e

		processDB.fail = True
		alog = ActiveLog.objects.create(name= processDB.name,
				path=processDB.folder + os.sep + "error_file.txt")
		my_file = file(os.path.dirname(protein_pdb) + os.sep + "error_file.txt","w")
		my_file.write(e)
		my_file.close()
		alog.save()

		if settings.USE_EMAIL_BACKEND:
			send_mail(
	    'HTP SurflexDock Task Failed',
	    'Your request was failed. You can access the results on ' + "http://" + processDB.site + ' The erro is caused by: \n' + e,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)

	processDB.terminated_date = timezone.now()
	processDB.alive = True

	processDB.publish()

	if settings.USE_EMAIL_BACKEND:
		send_mail(
	    'HTP SurflexDock Task Finished',
	    'Your request was finished. You can access the results on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,
)
	# return True
def sleep_task4(protein_pdb,chain,residue_number, size,type_docking, cluster_cutoff):
	time.sleep(2)
	processDB = Process.objects.get(folder=os.path.dirname(protein_pdb) + "/")
	try:
		if settings.USE_EMAIL_BACKEND:
			send_mail(
	'HTP SurflexDock Task Created',
	'Your request to the server was accepted. You can follow the process on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)
		# print cluster_cutoff
		processD(protein_pdb,chain,int(residue_number), str(size),type_docking, cluster_cutoff)
	except LogException as e:
		processDB.fail = True
		alog = ActiveLog.objects.create(name= processDB.name,
				path=e.path)
		alog.save()

		if settings.USE_EMAIL_BACKEND:
			send_mail(
	    'HTP SurflexDock Task Failed',
	    'Your request was failed. You can access the results on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)

def mmpbsa(complex):


	time.sleep(2)
	processDB = Process.objects.get(folder=os.path.dirname(complex) + "/")
	try:
		# print settings.EMAIL_HOST_USER
		if settings.USE_EMAIL_BACKEND:
			send_mail(
	'HTP SurflexDock Task Created',
	'Your request to the server was accepted. You can follow the process on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)
		# print cluster_cutoff
		mmpbsa_analisys(complex)
	except LogException as e:
		processDB.fail = True
		alog = ActiveLog.objects.create(name= processDB.name,
				path=e.path)
		alog.save()
		if settings.USE_EMAIL_BACKEND:
			send_mail(
	    'HTP SurflexDock Task Failed',
	    'Your request was failed. You can access the results on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)

	except Exception as e:
		print "erro"
		print e

		processDB.fail = True
		alog = ActiveLog.objects.create(name= processDB.name,
				path=processDB.folder + os.sep + "error_file.txt")
		my_file = file(os.path.dirname(complex) + os.sep + "error_file.txt","w")
		my_file.write(e)
		my_file.close()
		alog.save()

		if settings.USE_EMAIL_BACKEND:
			send_mail(
	    'HTP SurflexDock Task Failed',
	    'Your request was failed. You can access the results on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,)

	processDB.terminated_date = timezone.now()
	processDB.alive = True

	processDB.publish()
	if settings.USE_EMAIL_BACKEND:
		send_mail(
	    'HTP SurflexDock Task Finished',
	    'Your request was finished. You can access the results on ' + "http://" + processDB.site,
	    settings.EMAIL_HOST_USER,
	    [processDB.user_email],
	    fail_silently=False,
)