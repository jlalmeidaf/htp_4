# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import subprocess, psutil

from django.shortcuts import render
# from .forms import DockingForm, DockingFormStage2
# import tempfile, shutil
# from django_q.tasks import async, result
# from django_q.tasks import Async
# from .hooks import sleep_task, sleep_task2, print_task, sleep_task3, sleep_task4
# from .models import Process, LigandFile, ActiveLog
from django.http import HttpResponseRedirect, HttpResponse #,  FileResponse
from django.core.urlresolvers import reverse
# from django.utils import timezone
from django_q.monitor import Stat
# from django.views.decorators.csrf import csrf_protect

#############
# from reportlab.lib import colors
# from reportlab.lib.pagesizes import letter, inch, landscape
# from reportlab.platypus import SimpleDocTemplate, Table, TableStyle,  Paragraph, NextPageTemplate, PageTemplate, Frame
# from reportlab.lib.styles import getSampleStyleSheet
#############

import os, sys #, math
# import zipfile

from django.core.mail import send_mail
from wsgiref.util import FileWrapper
import dyndock.settings as settings
# Create your views here.

# import django_tables2 as tables
# from django_tables2.utils import A 
# from django_tables2 import RequestConfig

import random
# from django.contrib.sites.models import Site
# from PIL import Image
# import datetime
# os.environ['MGLTOOLS'] = "/home/jorgehf/MGLTools-1.5.6/"

# sys.path.append(os.environ['MGLTOOLS'] + '/MGLToolsPckgs/')
# from AutoDockTools.DlgParser import DlgParser

# TEMPORARYFOLDER="/home/jorgehf/joao_htp_test/tmp"
# TEMPORARYFOLDER="/mnt/sda5/htp_temp"


def stop_queue_monitor(request, id_server):
	queue_servers = Stat.get_all()
	print queue_servers
	os.kill(int(id_server),2)
	if int(id_server) in queue_servers:
		print id_server
		os.kill(int(id_server),2)
	url = "http://" + request.META['HTTP_HOST'] + reverse("queue_monitor")
	# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

	return HttpResponseRedirect(url)

def stop_all_queue_monitor(request):
	
	pinfo = []
	for proc in psutil.process_iter():
		pinfo.append(proc.as_dict(attrs=['pid', 'cmdline']))

	for eachP in pinfo:
		 if 'qcluster' in eachP['cmdline']:
		 	os.kill(int(eachP['pid']),9)


	url = "http://" + request.META['HTTP_HOST'] + reverse("queue_monitor")
	# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

	return HttpResponseRedirect(url)

def start_queue_monitor(request):
	command_line = ["python", settings.BASE_DIR + os.sep + "manage.py" ,"qcluster"]
	process = subprocess.Popen(command_line)
	
	url = "http://" + request.META['HTTP_HOST'] + reverse("queue_monitor")
	# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

	return HttpResponseRedirect(url)



# def admin(request):
# 	pass


def queue_monitor(request):
		number_of_cluster = len(Stat.get_all())
		return render(
			request,
			'dyna/queue_monitor.html',
			{'number_of_cluster' : number_of_cluster, 'clusters' : Stat.get_all()}
		)	



