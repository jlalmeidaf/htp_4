# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.sites.models import Site

from django.db import models
from django.utils import timezone
import os
COLOR_CHOICES = (
    ('2500000','SHORT'), #2500000 
    ('25000000', 'MEDIUM'), #25000000
    ('250000000','LONG'), #25000000
)

class Process(models.Model):
	username = models.TextField()
	user_email = models.EmailField()
	name = models.TextField()
	projectName = models.TextField()
	alive = models.BooleanField()
	fail = models.BooleanField()
	folder = models.TextField()
	created_date = models.DateTimeField(
            default=timezone.now)
	terminated_date = models.DateTimeField(null=True, blank=True)
	protein = models.TextField()
	chain = models.TextField()
	residue_number = models.TextField()
	size = models.TextField()
	type_docking = models.TextField(choices=COLOR_CHOICES, 
		default='MEDIUM')
	site = models.TextField()
	cluster_cutoff = models.TextField()


	def publish(self):
	 	self.save()

class LigandFile(models.Model):
	filename = models.TextField()
	name_of_Process = models.TextField()
	
	folder = models.TextField()
	def publish(self):
	 	self.save()

	def get_name(self):
		return self.folder + os.path.basename(self.filename)

class ActiveLog(models.Model):
		name = models.TextField()
		path = models.TextField()


class vars(models.Model):
    idvar=models.IntegerField('Var.ID', primary_key=True)