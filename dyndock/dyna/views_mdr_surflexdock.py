# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import subprocess, psutil

from django.shortcuts import render, get_object_or_404
from .forms import DockingForm, DockingFormStage2
import tempfile, shutil
from django_q.tasks import async, result
from django_q.tasks import Async
from .hooks import sleep_task, sleep_task2, print_task, sleep_task3, sleep_task4
from .models import Process, LigandFile, ActiveLog
from django.http import HttpResponseRedirect, HttpResponse, FileResponse
from django.core.urlresolvers import reverse
from django.utils import timezone
from django_q.monitor import Stat
from django.views.decorators.csrf import csrf_protect

#############
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, inch, landscape
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle,  Paragraph, NextPageTemplate, PageTemplate, Frame
from reportlab.lib.styles import getSampleStyleSheet
#############

import os, sys, math
import zipfile

from django.core.mail import send_mail
from wsgiref.util import FileWrapper
import dyndock.settings as settings
# Create your views here.

import django_tables2 as tables
from django_tables2.utils import A 
from django_tables2 import RequestConfig

import random
from django.contrib.sites.models import Site
from PIL import Image
import datetime
os.environ['MGLTOOLS'] = settings.MGLTOOLS

sys.path.append(os.environ['MGLTOOLS'] + '/MGLToolsPckgs/')
from AutoDockTools.DlgParser import DlgParser

TEMPORARYFOLDER=settings.TEMPORARYFOLDER
# TEMPORARYFOLDER="/mnt/sda5/htp_temp"



# def experiment_detail(request,name):
# 		post = get_object_or_404(Process, name=name)
# 		list_of_compounds = []

	
# 		for eachFolder in os.listdir(os.path.dirname(post.protein)):
# 			path_name = os.path.dirname(post.protein) + os.sep + eachFolder
# 			if os.path.isdir(path_name) and eachFolder != "Logs":
# 				for eachFile in os.listdir(path_name):
# 					int_path = path_name + os.sep + eachFile

# 					if os.path.isdir(int_path):
# 						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


# 									dlg_file = int_path + os.sep + "ligant_receptor.dlg"
# 									list_of_folder_name =  dlg_file.split(os.sep)
# 									ligand_name = list_of_folder_name[-2]
# 									time_name = list_of_folder_name[-3][0:-7]
# 									parser = DlgParser(dlg_file)
# 									for eachStruct in parser.clist:
# 										if eachStruct['inhib_constant_units'] == 'uM':
# 											mag = math.pow(10,-6)
# 								 		else:
# 											mag = math.pow(10,-9)
# 										z = {'ki': eachStruct['inhib_constant']*mag, "time": 'os.path.basename(os.path.dirname(int_path))', "compound": os.path.basename(int_path),   'status':False, 'experiment_id': name, 'pose_number' : eachStruct['run'] }
# 										list_of_compounds.append(z)

# 		compounds = Compound(list_of_compounds)
# 		RequestConfig(request).configure(compounds)
# 		return render(
# 			request,
# 			'dyna/experiment_detail.html',
# 			{'post': post, 'compounds': compounds}
# 		# )

# )

def download_mdr_analisys(request, name):	
		post = get_object_or_404(Process, name=name)
		subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',os.path.dirname(post.folder)])
		response = FileResponse(open(post.folder + os.sep + "analisys.pdf", 'r'), 'rb')
		response['Content-Disposition'] = 'attachment; filename=' + name + '.pdf'
		# response.write()

		return response


def experiment_detail2(request,name):
	if request.method == 'POST':
		mylist = request.session['mylist']

		list_of_input_ids=request.POST.getlist("bbb") #like new_ligand_xx23.pdbqt*3220.000
		# print list_of_input_ids

		new_list = []
		
		for eachTag in list_of_input_ids:
			for eachN in mylist:
				if eachN['compound'] == eachTag.split('*')[0] and eachN['conformation'] == eachTag.split('*')[1] and str(eachN['pose_number']) == eachTag.split('*')[2]:
					# print eachTag.split('*')[2]
					# print eachN['pose_number']
					new_list.append(eachN)
		# print list_of_input_ids
		request.session['mylist2'] = new_list
		# print new_list
		if 'mmpbsa' in request.POST:
				if len(new_list) == 1:
					url = "http://" + request.META['HTTP_HOST'] + reverse("mmpbsa_analisys",  kwargs={'name': name})
				else:
					return render(request,'dyna/moreThan1.html',)
		return HttpResponseRedirect(url)

	else:

		post = get_object_or_404(Process, name=name)
		list_of_compounds = []
		has_cluster = False
		ligands = LigandFile.objects.all().filter(name_of_Process=name)
		print post.folder
		subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',post.folder])
		
		# subprocess.call(["gs","-sDEVICE=jpeg","-dJPEGQ=100","-dNOPAUSE","-dBATCH","-dSAFER","-r300","-sOutputFile=" + os.path.dirname(post.protein) + os.sep + "output.jpg", os.path.dirname(post.protein) + os.sep + "cluster100ns.eps"])
		if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
			subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
			has_cluster = True
		try:
			if request.session['name'] != name:
				raise KeyError() 
			mytime = int(datetime.datetime.now().strftime('%s'))
			if mytime - request.session['time'] > 600:
				raise KeyError()
			list_of_compounds = request.session['mylist']
		except KeyError as e:

			for eachFolder in os.listdir(post.folder):
				path_name = post.folder + os.sep + eachFolder
				if os.path.isdir(path_name) and eachFolder != "Logs":
					for eachFile in os.listdir(path_name):
						int_path = path_name + os.sep + eachFile

						if os.path.isdir(int_path):
							if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
										list_of_folder_name =  dlg_file.split(os.sep)
										ligand_name = list_of_folder_name[-2]
										time_name = list_of_folder_name[-3]
										parser = DlgParser(dlg_file)
										mylist = {}
										# print dlg_file
										for eachStruct in parser.clist:
											if 'inhib_constant_units' in eachStruct:
												if eachStruct['inhib_constant_units'] == 'uM':
													mag = math.pow(10,-6)
										 		elif eachStruct['inhib_constant_units'] == 'mM':
													mag = math.pow(10,-3)
												elif eachStruct['inhib_constant_units'] == 'nM':
													mag = math.pow(10,-9)
												elif eachStruct['inhib_constant_units'] == 'pM':
													mag = math.pow(10,-12)
												elif eachStruct['inhib_constant_units'] == 'fM':
													mag = math.pow(10,-15)
												elif eachStruct['inhib_constant_units'] == 'aM':
													mag = math.pow(10,-18)
												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
										better_Ki = math.pow(10,0)
										docking_id_better_Ki = None
										
										for eachElem in mylist:
											ki = mylist[eachElem][0]
											binding_energy = mylist[eachElem][1]

											# if ki <  better_Ki:
											docking_id_better_Ki = eachElem
											# 	better_Ki = ki
											z = {'ki': ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':False, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': binding_energy}
											list_of_compounds.append(z)
					request.session['mylist'] = list_of_compounds

		compounds = Compound(list_of_compounds)
		compounds.exclude = ('select')
		ligand_list = get_pdbqts(name)
		RequestConfig(request, paginate={'per_page': 50}).configure(compounds)
		return render(
			request,
			'dyna/experiment_detail3.html',
			{'post' : post, 'compounds': compounds, 'ligands': ligand_list, 'has_cluster' : has_cluster }
		)

def get_pdbqts(name):
	mylist = []
	post = get_object_or_404(Process, name=name)
	workdir = post.folder
	for eachFile in os.walk(workdir):
			if eachFile[0].endswith(".pdbqt"):
				mylist.append(os.path.basename(eachFile[0]))

	return mylist
def boxplot(request, name, ligand):
	post = get_object_or_404(Process, name=name)
	workdir = post.folder
	# print workdir + os.sep + ligand + "_" + post.type_docking + "_.csv boxplot.png"
	if (os.path.exists(workdir + os.sep + ligand + "_" + "10" + "_.csv boxplot.png")):
		boxplot_file = workdir + os.sep + ligand + "_" + "10" + "_.csv boxplot.png"
	else:
		boxplot_file = settings.BASE_DIR + os.sep + "withoutInf.png"
		print boxplot_file
	response = HttpResponse(content_type="image/png")
	im = Image.open(boxplot_file)
	im.save(response, "PNG")
	return response



class Compound(tables.Table):


		experiment_id = tables.Column(visible=False)
		View_Complex = tables.LinkColumn(text = "view",viewname='show_pose', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
		pose_number = tables.Column()
		compound = tables.Column()
		ki = tables.Column(verbose_name='KI (Molar)')
		conformation = tables.Column()
		binding_energy = tables.Column(verbose_name = "∆G (Kcal/mol)")

		template = '<input type="checkbox" name="bbb" value="{{ record.compound }}' + '*' + '{{ record.conformation }}' + '*' + '{{ record.pose_number }}" />'


		download_complex = tables.LinkColumn(text = "Download!", viewname='download_protein_ligand', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
		refine = tables.TemplateColumn(template, verbose_name='MM/PBSA')
		class Meta:
			template_name = 'django_tables2/bootstrap.html'
			table_pagination = False
			# # model = vars
			# fields = ('idvars')
			attrs = {
			'th' : {
					'_ordering': {
						'orderable': 'sortable', # Instead of `orderable`
						'ascending': 'ascend',   # Instead of `asc`
						'descending': 'descend'  # Instead of `desc`
					}
				}
			}


		
# class CompoundLite(tables.Table):


# 		experiment_id = tables.Column(visible=False)
# 		pose_number = tables.LinkColumn(viewname='show_pose', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
# 		compound = tables.Column()
# 		ki = tables.Column()
# 		conformation = tables.Column()
# 		binding_energy = tables.Column()

# 		# template = '<input type="checkbox" name="bbb" value="{{ record.compound }}' + '*' + '{{ record.conformation }}" />'

# 		# refine = tables.TemplateColumn(template)

# 		download_complex = tables.LinkColumn(text = "Download!", viewname='download_protein_ligand', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
# 		class Meta:
# 			template_name = 'django_tables2/bootstrap.html'
# 			table_pagination = False
# 			# # model = vars
# 			# fields = ('idvars')
# 			attrs = {
# 			'th' : {
# 					'_ordering': {
# 						'orderable': 'sortable', # Instead of `orderable`
# 						'ascending': 'ascend',   # Instead of `asc`
# 						'descending': 'descend'  # Instead of `desc`
# 					}
# 				}
			# }
