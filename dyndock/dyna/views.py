# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import subprocess, psutil

from django.shortcuts import render, get_object_or_404
from .forms import DockingForm, DockingFormStage2
import tempfile, shutil
from django_q.tasks import async, result
from django_q.tasks import Async
from .hooks import sleep_task, sleep_task2, print_task, sleep_task3, sleep_task4
from .models import Process, LigandFile, ActiveLog
from django.http import HttpResponseRedirect, HttpResponse, FileResponse
from django.core.urlresolvers import reverse
from django.utils import timezone
from django_q.monitor import Stat
from django.views.decorators.csrf import csrf_protect

#############
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter, inch, landscape
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle,  Paragraph, NextPageTemplate, PageTemplate, Frame
from reportlab.lib.styles import getSampleStyleSheet
#############

import os, sys, math
import zipfile

from django.core.mail import send_mail
from wsgiref.util import FileWrapper
import dyndock.settings as settings
# Create your views here.

import django_tables2 as tables
from django_tables2.utils import A 
from django_tables2 import RequestConfig

import random
from django.contrib.sites.models import Site
from PIL import Image
import datetime
from dyndock import settings
os.environ['MGLTOOLS'] = settings.MGLTOOLS

sys.path.append(os.environ['MGLTOOLS'] + '/MGLToolsPckgs/')
from AutoDockTools.DlgParser import DlgParser

TEMPORARYFOLDER=settings.TEMPORARYFOLDER
# TEMPORARYFOLDER="/mnt/sda5/htp_temp"

# def manual(request):
# 	return render(request, 'dyna/manual.html')

# def basic_tutorial(request):
# 	 # with open('/home/jorgehf/joao_htp_test/htp2.old/dyndock/HTP_SurflexDock_basic_Tutorial.pdf', 'r') as pdf:
# 	 # 	response = HttpResponse(pdf.read())
# 	 # 	response['Content-Disposition'] = 'inline;filename=HTP SurflexDock Basic Tutorial.pdf'
# 	 # 	return response
# 	 # pdf.close
# 	try:
# 		return FileResponse(open('/home/jorgehf/joao_htp_test/htp2.old/dyndock/HTP_SurflexDock_basic_Tutorial.pdf', 'rb'), content_type='application/pdf')
# 	except FileNotFoundError:
# 		raise Http404()


# def redo_docking(self, name):
# 	post = get_object_or_404(Process, name=name)
# 	workdir = post.folder
# 	times = []

# 	#remove compound dirs

# 	for each_files_and_folders in os.listdir(workdir):
# 		selected_file = workdir + os.sep + each_files_and_folders
# 		if os.path.isdir(selected_file):
# 					for each_file in os.listdir(selected_file):
# 						compound_folder = selected_file + os.sep + each_file
# 						if os.path.isdir(compound_folder):
# 							shutil.rmtree(compound_folder)


# 	task_id = async(sleep_task4, post.protein,post.chain,post.residue_number, post.size, post.type_docking, post.cluster_cutoff)




# def stop(request, name):
# 	post = get_object_or_404(Process, name=name)
# 	workdir = post.folder

# 	stop_file = file(workdir + os.sep + 'venon.txt', 'w')
# 	stop_file.close()
# 	return HttpResponse('Stopping task ' + name)

# def stop_queue_monitor(request, id_server):
# 	queue_servers = Stat.get_all()
# 	print queue_servers
# 	os.kill(int(id_server),2)
# 	if int(id_server) in queue_servers:
# 		print id_server
# 		os.kill(int(id_server),2)
# 	url = "http://" + request.META['HTTP_HOST'] + reverse("queue_monitor")
# 	# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 	return HttpResponseRedirect(url)

# def stop_all_queue_monitor(request):
	
# 	pinfo = []
# 	for proc in psutil.process_iter():
# 		pinfo.append(proc.as_dict(attrs=['pid', 'cmdline']))

# 	for eachP in pinfo:
# 		 if 'qcluster' in eachP['cmdline']:
# 		 	os.kill(int(eachP['pid']),9)


# 	url = "http://" + request.META['HTTP_HOST'] + reverse("queue_monitor")
# 	# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 	return HttpResponseRedirect(url)

# def start_queue_monitor(request):
# 	command_line = ["python", settings.BASE_DIR + os.sep + "manage.py" ,"qcluster"]
# 	process = subprocess.Popen(command_line)
	
# 	url = "http://" + request.META['HTTP_HOST'] + reverse("queue_monitor")
# 	# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 	return HttpResponseRedirect(url)

# def download_roc_tables(request, name):
# 	post = get_object_or_404(Process, name=name)
# 	times = []
# 	list_of_compounds = request.session['mylist']
# 	compounds = Compound(data=list_of_compounds, order_by='binding_energy')
# 	for compound in compounds.as_values():
# 		 if compound[3] not in times:
# 		 	times.append(compound[3])
# 	context = {'name': name, 'times' : times}
# 	return render(request, 'dyna/roc_tables.html', context)



# def download_roc_table(request,name,time):
# 		post = get_object_or_404(Process, name=name)
# 		list_of_compounds = request.session['mylist']
# 		dict_of_conformations = {}
# 		compounds = Compound(data=list_of_compounds, order_by='binding_energy')	

# 		text = " time: " + time + "\n"
# 		for compound in compounds.as_values():
# 			if compound[3] == time:
# 				text = text + str(compound[1]) + " " + str(compound[4]) + "\n"

# 		response = HttpResponse((text), content_type='application/force-download')
# 		return response

# 		return response

# def download_table(request, name):
# 		post = get_object_or_404(Process, name=name)
# 		list_of_compounds = request.session['mylist']
# 		dict_of_conformations = {}
# 		compounds = Compound(data=list_of_compounds, order_by='binding_energy')	

# 		for compound in compounds.as_values():
# 			if compound[0] != "Pose number":
# 				if compound[3] not in dict_of_conformations.keys():

# 					dict_of_conformations[compound[3]] = [  ["Conformation " + compound[3]], ['Compound' , '\u0394G (kcal/mol)'] ]
					
# 				if len(dict_of_conformations[compound[3]]) <= 21:
# 					compound_name =  compound[1][:-6]
# 					dict_of_conformations[compound[3]].append( [compound_name, compound[4]])
# 				else:
# 					pass
# 		# new_table = None
# 		# for eachCompound in dict_of_conformations:
# 		# 	if new_table==None:
# 		# 		new_table=dict_of_conformations
# 		# 	else:
# 		# 		new_table = new_table + dict_of_conformations[eachCompound]


# 		doc = SimpleDocTemplate(post.folder + os.sep + post.name + ".pdf", pagesize=letter)
# 		styleSheet = getSampleStyleSheet()
# 		p1 = Paragraph('''
#     <para align=center spaceb=3>HTP SurflexDock results</para>''',
#     styleSheet["BodyText"])
# 		p2 = Paragraph('''
#     <para align=center spaceb=3>Top 20 Results Generated for Each Protein Conformation of ''' + os.path.basename(post.protein) + '''</para>''',
#     styleSheet["BodyText"])


# 		data = []
# 		data_internal = []
# 		elements = []

# 		frame1 = Frame(doc.leftMargin, doc.bottomMargin,
# 		                doc.width, doc.height,
# 		                leftPadding = 170, rightPadding = 0,
# 		                topPadding = 120, bottomPadding = 0,
# 		                id='frame1')
# 		ltemplate = PageTemplate(id='landscape',frames =[frame1], onPage=make_landscape)
# 		doc.addPageTemplates([ltemplate])


# 		elements.append(NextPageTemplate('landscape'))
# 		elements.append(p1)
# 		elements.append(p2)
# 		for compound in dict_of_conformations:
# 			table_data = dict_of_conformations[compound]

# 			t1 = Table(table_data, 2 * [1.3 * inch], len(table_data) * [0.2 * inch])	
# 			t1.setStyle(TableStyle([
# 	('SPAN', (0, 0), ((1,0))),
#     ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
#     ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
# ]))	
# 			# if(len(data_internal)<=2):
# 			data_internal.append(t1)

# 		# 	if(len(data_internal)==2):
# 		# 		data = [data_internal]
# 		# 		t1_w = 2.6 * inch
# 		# 		t2_w = 3.2 * inch
# 		# 		shell_table = Table(data, colWidths=[t1_w, t2_w])
# 		# 		elements.append(shell_table)
# 		# 		data_internal = []
# 		# 		data = []
# 		# if(len(data_internal) != 0):
# 		# 	data = [data_internal]
# 		# 	shell_table = Table(data, colWidths=[t1_w, t2_w])
# 		# 	elements.append(shell_table)

# 		t1_w = 2.6 * inch
# 		t2_w = 2.6 * inch
# 		t3_w = 2.6 * inch
# 		t4_w = 2.6 * inch
# 		data = [data_internal]
# 		shell_table = Table(data, colWidths=[t1_w, t2_w,t3_w, t4_w ])
# 		shell_table.setStyle(TableStyle([
#     ('VALIGN', (0, 0), (-1, -1), 'TOP')
# ]))
# 		elements.append(shell_table)

# 		doc.build(elements)
# 		response = FileResponse(open(post.folder + os.sep + post.name + ".pdf", 'r'), 'rb')
# 		response['Content-Disposition'] = 'attachment; filename=' + name + '.pdf'
# 		# response.write()

# 		return response

# def download_table_from_mdr(request, name):
# 		post = get_object_or_404(Process, name=name)
# 		list_of_compounds = request.session['mylist']
# 		dict_of_conformations = {}
# 		compounds = Compound(data=list_of_compounds, order_by='binding_energy')	

# 		for compound in compounds.as_values():
# 			if compound[0] != "Pose number":
# 				if compound[3] not in dict_of_conformations.keys():

# 					dict_of_conformations[compound[3]] = [  ["Conformation " + compound[3]], ['Compound' ,'Pose' ,'Binding Energy'] ]
					
# 				if len(dict_of_conformations[compound[3]]) <= 21:
# 					compound_name =  compound[1][:-6]
# 					dict_of_conformations[compound[3]].append( [compound_name,  compound[0], compound[4]])
# 				else:
# 					pass

# 		doc = SimpleDocTemplate(post.folder + os.sep + post.name + ".pdf", pagesize=letter)
# 		styleSheet = getSampleStyleSheet()
# 		p1 = Paragraph('''
#     <para align=center spaceb=3>MDR SurflexDock results</para>''',
#     styleSheet["BodyText"])
# 		p2 = Paragraph('''
#     <para align=center spaceb=3>Top 20 Results Generated for Each Protein Conformation of ''' + os.path.basename(post.protein) + '''</para>''',
#     styleSheet["BodyText"])


# 		data = []
# 		data_internal = []
# 		elements = []
# 		elements.append(p1)
# 		elements.append(p2)
# 		for compound in dict_of_conformations:
# 			table_data = dict_of_conformations[compound]

# 			t1 = Table(table_data, 2 * [1.2 * inch], len(table_data) * [0.2 * inch])	
# 			t1.setStyle(TableStyle([
# 	('SPAN', (0, 0), ((1,0))),
#     ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
#     ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
# ]))
# 			data_internal.append(t1)

# 			if(len(data_internal)==2):
# 				data = [data_internal]
# 				t1_w = 4 * inch
# 				t2_w = 3.2 * inch
# 				shell_table = Table(data, colWidths=[t1_w, t2_w])
# 				elements.append(shell_table)
# 				data_internal = []
# 				data = []
# 		if(len(data_internal) != 0):
# 			data = [data_internal]
# 			shell_table = Table(data, colWidths=[t1_w, t2_w])
# 			elements.append(shell_table)
# 		doc.build(elements)
# 		response = FileResponse(open(post.folder + os.sep + post.name + ".pdf", 'r'), 'rb')
# 		response['Content-Disposition'] = 'attachment; filename=' + name + '.pdf'
# 		# response.write()

# 		return response

# def admin(request):
# 	pass
# @csrf_protect
# def load_files(request):
# 	if request.method == 'POST':
# 		form = DockingForm(request.POST, request.FILES)
# 		if form.is_valid():
# 			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep

# 			filename = workdir + request.FILES['structureFile'].name
# 			structureFile = file(filename, "wb+")
# 			for chunk in request.FILES['structureFile'].chunks():
# 				structureFile.write(chunk)
# 			structureFile.close()
# 			ligandList = []
# 			for eachLigandFile in form.cleaned_data['ligandFile']:
# 				fileLigand = eachLigandFile.name
# 				ligandList.append(fileLigand)
# 				ligandFile = file(workdir + fileLigand, "wb+")
# 				for chunk in eachLigandFile.chunks():
# 					ligandFile.write(chunk)
# 				ligandFile.close()
# 			myemail = form.cleaned_data['email']
# 			projectName = form.cleaned_data['projectName']
# 			type_docking = form.cleaned_data['type_docking']
# 			chainData = form.cleaned_data.get('chain')
# 			residue_number = int(form.cleaned_data['residue_number'])
# 			size = float(form.cleaned_data['size'])	
# 			cluster_cutoff = form.cleaned_data['cluster_cutoff']	

# 			task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)
# 			process = Process.objects.create(name= task_id,
# 				alive = False,
# 				folder = workdir,
# 				protein = filename,
# 				chain = chainData,
# 				residue_number = residue_number,
# 				type_docking = type_docking,
# 				size = size,
# 				fail = False,
# 				user_email = myemail,
# 				projectName = projectName,
# 				cluster_cutoff = cluster_cutoff,
# 				site=request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id}))

# 				# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
# 			process.publish()

# 			for eachLigandFile in ligandList:
# 				ligand = LigandFile.objects.create(name_of_Process = task_id,
# 					filename = eachLigandFile)
# 				ligand.publish()
			
# # 			send_mail(
# # 	'HTP SurflexDock Task Created',
# # 	'Your request to the server was accepted. You can follow the process on ' + "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id}) ,
# # 	'devlab230uenf@gmail.com',
# # 	[myemail],
# # 	fail_silently=False,
# # )
# 			url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id})
# 			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 			return HttpResponseRedirect(url)
# 		else:
# 			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep
# 			shutil.copyfile('/home/jorgehf/joao_htp_test/htp2.old/dyndock/static/example/hACEcOk.pdb', workdir + os.sep + 'hACEcOk.pdb')
# 			shutil.copyfile('/home/jorgehf/joao_htp_test/htp2.old/dyndock/static/example/sample_15_lig_85_dec_hace_c.tar.gz', workdir + os.sep + 'sample_15_lig_85_dec_hace_c.tar.gz')

# 			filename = workdir + 'hACEcOk.pdb'
# 			# structureFile = file(filename, "wb+")
# 			# for chunk in request.FILES['structureFile'].chunks():
# 			# 	structureFile.write(chunk)
# 			# structureFile.close()
# 			ligandList = ['sample_15_lig_85_dec_hace_c.tar.gz']
# 			# for eachLigandFile in form.cleaned_data['ligandFile']:
# 			# 	fileLigand = eachLigandFile.name
# 			# 	ligandList.append(fileLigand)
# 			# 	ligandFile = file(workdir + fileLigand, "wb+")
# 			# 	for chunk in eachLigandFile.chunks():
# 			# 		ligandFile.write(chunk)
# 			# 	ligandFile.close()
# 			myemail = form.cleaned_data['email']
# 			projectName = form.cleaned_data['projectName']
# 			type_docking = form.cleaned_data['type_docking']
# 			chainData = form.cleaned_data.get('chain')
# 			residue_number = int(form.cleaned_data['residue_number'])
# 			size = float(form.cleaned_data['size'])	
# 			cluster_cutoff = form.cleaned_data['cluster_cutoff']	

# 			task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)
# 			process = Process.objects.create(name= task_id,
# 				alive = False,
# 				folder = workdir,
# 				protein = filename,
# 				chain = chainData,
# 				residue_number = residue_number,
# 				type_docking = type_docking,
# 				size = size,
# 				fail = False,
# 				user_email = myemail,
# 				projectName = projectName,
# 				cluster_cutoff = cluster_cutoff,
# 				site=request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id}))

# 				# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
# 			process.publish()

# 			for eachLigandFile in ligandList:
# 				ligand = LigandFile.objects.create(name_of_Process = task_id,
# 					filename = eachLigandFile)
# 				ligand.publish()
			
# # 			send_mail(
# # 	'HTP SurflexDock Task Created',
# # 	'Your request to the server was accepted. You can follow the process on ' + "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id}) ,
# # 	'devlab230uenf@gmail.com',
# # 	[myemail],
# # 	fail_silently=False,
# # )
# 			url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id})
# 			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 			return HttpResponseRedirect(url)
# 	else:
# 		form = DockingForm()  # A empty, unbound form
# 		# form.fields["projectName"].initial = "oiasd@sfd.com"

# 		return render(
# 			request,
# 			'dyna/load_files.html',
# 			{'form': form}
# 		)

# def load_example(request):
# 	if request.method == 'POST':
# 		form = DockingForm(request.POST, request.FILES)
# 		if form.is_valid():
# 			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep

# 			filename = workdir + request.FILES['structureFile'].name
# 			structureFile = file(filename, "wb+")
# 			for chunk in request.FILES['structureFile'].chunks():
# 				structureFile.write(chunk)
# 			structureFile.close()
# 			ligandList = []
# 			for eachLigandFile in form.cleaned_data['ligandFile']:
# 				fileLigand = eachLigandFile.name
# 				ligandList.append(fileLigand)
# 				ligandFile = file(workdir + fileLigand, "wb+")
# 				for chunk in eachLigandFile.chunks():
# 					ligandFile.write(chunk)
# 				ligandFile.close()
# 			myemail = form.cleaned_data['email']
# 			projectName = form.cleaned_data['projectName']
# 			type_docking = form.cleaned_data['type_docking']
# 			chainData = form.cleaned_data.get('chain')
# 			residue_number = int(form.cleaned_data['residue_number'])
# 			size = float(form.cleaned_data['size'])	
# 			cluster_cutoff = form.cleaned_data['cluster_cutoff']	

# 			task_id = async(sleep_task, filename,ligandList,chainData,residue_number, size, type_docking, cluster_cutoff)
# 			process = Process.objects.create(name= task_id,
# 				alive = False,
# 				folder = workdir,
# 				protein = filename,
# 				chain = chainData,
# 				residue_number = residue_number,
# 				type_docking = type_docking,
# 				size = size,
# 				fail = False,
# 				user_email = myemail,
# 				projectName = projectName,
# 				cluster_cutoff = cluster_cutoff,
# 				site=request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id}))

# 				# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
# 			process.publish()

# 			for eachLigandFile in ligandList:
# 				ligand = LigandFile.objects.create(name_of_Process = task_id,
# 					filename = eachLigandFile)
# 				ligand.publish()
			
# # 			send_mail(
# # 	'HTP SurflexDock Task Created',
# # 	'Your request to the server was accepted. You can follow the process on ' + "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id}) ,
# # 	'devlab230uenf@gmail.com',
# # 	[myemail],
# # 	fail_silently=False,
# # )
# 			url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail3",  kwargs={'name': task_id})
# 			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 			return HttpResponseRedirect(url)
# 	else:
# 		form = DockingForm()  # A empty, unbound form
# 		# form.fields["projectName"].initial = "oiasd@sfd.com"
# 		# form.fields["structureFile"].initial = "aew.pdb"

# 		return render(
# 			request,
# 			'dyna/load_files.html',
# 			{'form': form}
# 		)

# def task_list(request):	
# 	posts = Process.objects.all()
# 	for eachPost in posts:
# 		eachPost.b = False
# 		if result(eachPost.name) != None and not eachPost.alive:
# 			eachPost.alive = True
# 			eachPost.save()
# 		if os.path.exists(eachPost.folder):
# 			size = subprocess.check_output(['du','-sh', eachPost.folder]).split()[0].decode('utf-8')
# 			eachPost.b = True
# 			eachPost.size = size
# 		else:
# 			eachPost.delete()

# 	return render(
# 			request,
# 			'dyna/task_list.html',
# 			{'posts': posts}
# 		)



# def task_detail(request,name):
		
# 		post = get_object_or_404(Process, name=name)
# 		ligands = LigandFile.objects.all().filter(name_of_Process=name)
# 		nresult = result(name)
# 		if nresult != None and not post.alive:
# 			post.alive = True
# 			post.save()


# 		return render(
# 			request,
# 			'dyna/task.html',
# 			{'post': post, 'ligands' : ligands,'result' : nresult}
# 		)

# def download_detail_table(request, name):
# 		post = get_object_or_404(Process, name=name)
# 		list_of_compounds = []
# 		has_cluster = False
# 		ligands = LigandFile.objects.all().filter(name_of_Process=name)
# 		subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',os.path.dirname(post.protein)])
		
# 		# subprocess.call(["gs","-sDEVICE=jpeg","-dJPEGQ=100","-dNOPAUSE","-dBATCH","-dSAFER","-r300","-sOutputFile=" + os.path.dirname(post.protein) + os.sep + "output.jpg", os.path.dirname(post.protein) + os.sep + "cluster100ns.eps"])
# 		# if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
# 		# 	subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
# 		# 	has_cluster = True

		
# 		for eachFolder in os.listdir(post.folder):
# 			path_name = os.path.dirname(post.protein) + os.sep + eachFolder
# 			if os.path.isdir(path_name) and eachFolder != "Logs":
# 				for eachFile in os.listdir(path_name):
# 					int_path = path_name + os.sep + eachFile

# 					if os.path.isdir(int_path):
# 						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


# 									dlg_file = int_path + os.sep + "ligant_receptor.dlg"
# 									list_of_folder_name =  dlg_file.split(os.sep)
# 									ligand_name = list_of_folder_name[-2]
# 									time_name = list_of_folder_name[-3]
# 									parser = DlgParser(dlg_file)
# 									mylist = {}
# 									print dlg_file
# 									for eachStruct in parser.clist:
# 										if 'inhib_constant_units' in eachStruct:
# 											if eachStruct['inhib_constant_units'] == 'uM':
# 												mag = math.pow(10,-6)
# 									 		elif eachStruct['inhib_constant_units'] == 'mM':
# 												mag = math.pow(10,-3)
# 											elif eachStruct['inhib_constant_units'] == 'nM':
# 												mag = math.pow(10,-9)
# 											elif eachStruct['inhib_constant_units'] == 'pM':
# 												mag = math.pow(10,-12)
# 											elif eachStruct['inhib_constant_units'] == 'aM':
# 												mag = math.pow(10,-18)
# 											mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
# 									better_Ki = math.pow(10,0)
# 									docking_id_better_Ki = None
# 									better_binding_energy = 100
# 									for eachElem in mylist:

# 										ki = mylist[eachElem][0]
# 										binding_energy = mylist[eachElem][1]

# 										if binding_energy <  better_binding_energy:
# 											docking_id_better_Ki = eachElem
# 											better_Ki = ki
# 											better_binding_energy = binding_energy
# 									if binding_energy != None:
# 										# print os.path.basename(int_path)
# 										text = text + os.path.basename(int_path)[9:-6] + "   " + os.path.basename(int_path) + "   " + eachFolder + "   " + str(better_Ki) + "   " + "0" + "   " + str(better_binding_energy) + "   " + "   " + "0" + "   "  + "0" + "   " + str(better_binding_energy) + "\n"
# 										# z = {'ki': ki, "time": time_name[0:-7], "compound": os.path.basename(int_path),   'status':False, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'folder' : int_path, 'binding_energy': binding_energy}
# 										# list_of_compounds.append(z)


# 		response = HttpResponse((text), content_type='application/force-download')
# 		# response['Content-Disposition'] = 'attachment; filename=receptor.pdb'
# 		return response

# def download_table(request, name):
# 	post = get_object_or_404(Process, name=name)
# 	list_of_compounds = []
# 	has_cluster = False
# 	ligands = LigandFile.objects.all().filter(name_of_Process=name)

# 	text = ""
# 	dict_of_compounds = {}
# 	for eachFolder in os.listdir(post.folder):
# 		# print post.folder
# 		path_name = post.folder + os.sep + eachFolder

# 		if os.path.isdir(path_name) and eachFolder != "Logs":
# 			for eachFile in os.listdir(path_name):
# 				int_path = path_name + os.sep + eachFile

# 				if os.path.isdir(int_path):
# 					if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


# 								dlg_file = int_path + os.sep + "ligant_receptor.dlg"
# 								list_of_folder_name =  dlg_file.split(os.sep)
# 								ligand_name = list_of_folder_name[-2]
# 								time_name = list_of_folder_name[-3]
# 								parser = DlgParser(dlg_file)
# 								mylist = {}
# 								for eachStruct in parser.clist:
# 									if 'inhib_constant_units' in eachStruct:
# 										if eachStruct['inhib_constant_units'] == 'uM':
# 											mag = math.pow(10,-6)
# 								 		elif eachStruct['inhib_constant_units'] == 'mM':
# 											mag = math.pow(10,-3)
# 										elif eachStruct['inhib_constant_units'] == 'nM':
# 											mag = math.pow(10,-9)
# 										elif eachStruct['inhib_constant_units'] == 'pM':
# 											mag = math.pow(10,-12)
# 										elif eachStruct['inhib_constant_units'] == 'fM':
# 											mag = math.pow(10,-15)
# 										elif eachStruct['inhib_constant_units'] == 'aM':
# 											mag = math.pow(10,-18)
# 										mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
# 								better_Ki = math.pow(10,0)
# 								docking_id_better_Ki = None
# 								better_binding_energy = 100
# 								for eachElem in mylist:

# 									ki = mylist[eachElem][0]
# 									binding_energy = mylist[eachElem][1]

# 									if binding_energy <  better_binding_energy:
# 										docking_id_better_Ki = eachElem
# 										better_Ki = ki
# 										better_binding_energy = binding_energy
# 								compound = os.path.basename(int_path)
# 								if compound not in dict_of_compounds.keys():
# 									dict_of_compounds[compound] = {}
								
# 								dict_of_times = dict_of_compounds[compound] 
# 								# if time_name[0:-7] not in dict_of_times.keys():

# 								dict_of_times[time_name[0:-7]] = better_binding_energy

# 								dict_of_compounds[compound] = dict_of_times


# 								# if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"):
# 								# 	z = {'ki': better_Ki, "time": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'folder' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"}
# 								# else:
# 								# 	z = {'ki': better_Ki, "time": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'folder' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein)}
# 								# # list_of_compounds.append(z)

# 								# text = text + z["compound"]  + ";" + str(z["binding_energy"]) + "\n"
# 	for eachCompound in dict_of_compounds:
# 		text = text + eachCompound
# 		dict_of_times = dict_of_compounds[eachCompound]
# 		for time in dict_of_times:
# 			text = text + ";" + time
# 		text = text + "\n"



# 	response = HttpResponse((text), content_type='application/force-download')
# 	return response

# def download_protein_str(request, experiment_id,time, compound, pose):


# 		post = get_object_or_404(Process, name=experiment_id)
# 		best_model = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
# 		# if time != "Control_folder":
# 		# 	best_model = best_model + "_em.pdb"
# 		zip_file = open(best_model, 'r')
# 		x = zip_file.readlines()

# 		response = HttpResponse("".join(x), content_type='text/plain')
# 		# response['Content-Disposition'] = 'attachment; filename=receptor.pdb'
# 		return response
# def download_protein_ligand(request, experiment_id,time, compound, pose):
# 		post = get_object_or_404(Process, name=experiment_id)

# 		best_model_path = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
# 		best_model = open(best_model_path, 'r')
# 		list_receptor = best_model.readlines()

# 		# print post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg"
# 		parser = DlgParser(post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg")
# 		list_compound = parser.run_models[int(pose)]
# 		text_compoundPDB =  "\n"
# 		text_compound =  "\n"
# 		atom_num = 1

# 		for eachLine in list_receptor:
# 			atom_serial_number = str(atom_num).rjust(5)
# 			text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:21] + "R" + eachLine[22:54] + "\n"
# 			atom_num = atom_num + 1

# 		for eachLine in list_compound:
# 			text_compound = text_compound + eachLine + "\n"
# 			if eachLine.startswith("ATOM"):
# 				text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:21] + "L" + eachLine[22:54] + "\n"
# 		response = HttpResponse("".join(text_compoundPDB), content_type='text/plain')
# 		response['Content-Disposition'] = 'attachment; filename=complex_' + compound.split(".")[0] + "_" + time.split('.')[0] + "_" + pose + '.pdb'
# 		return response
# def download_ligand(request, experiment_id,time, compound, pose):
# 		post = get_object_or_404(Process, name=experiment_id)

# 		best_model_path = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
# 		# best_model = open(best_model_path, 'r')
# 		# list_receptor = best_model.readlines()

# 		# print post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg"
# 		parser = DlgParser(post.folder + os.sep + time + "_folder" + os.sep + compound + os.sep + "ligant_receptor.dlg")
# 		list_compound = parser.run_models[int(pose)]
# 		text_compoundPDB =  "\n"
# 		text_compound =  "\n"
# 		atom_num = 1

# 		# for eachLine in list_receptor:
# 		# 	atom_serial_number = str(atom_num).rjust(5)
# 		# 	text_compoundPDB =  text_compoundPDB  + eachLine[0:6] + atom_serial_number + eachLine[11:21] + "R" + eachLine[22:54] + "\n"
# 		# 	atom_num = atom_num + 1

# 		for eachLine in list_compound:
# 			text_compound = text_compound + eachLine + "\n"
# 			if eachLine.startswith("ATOM"):
# 				text_compoundPDB =  text_compoundPDB  + eachLine[0:54] + "\n"
# 		response = HttpResponse("".join(text_compoundPDB), content_type='text/plain')
# 		# response['Content-Disposition'] = 'attachment; filename=complex_' + compound.split(".")[0] + "_" + time.split('.')[0] + "_" + pose + '.pdb'
# 		return response
# def download(request, name):
		
# 		temp = "/tmp/" + name + ".zip"
# 		post = get_object_or_404(Process, name=name)
# 		subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',os.path.dirname(post.protein)])
# 		subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'cluster100ns.png'])
# 		z = zipfile.ZipFile(temp, "w", zipfile.ZIP_DEFLATED)

# 		for eachFolder in os.listdir(os.path.dirname(post.protein)):
# 			path_name = os.path.dirname(post.protein) + os.sep + eachFolder
# 			if eachFolder.endswith("cluster100ns.png"):
# 				z.write(path_name,  eachFolder)
# 			if eachFolder.endswith("analisys.pdf"):
# 				z.write(path_name,  eachFolder)
# 			if eachFolder.endswith("telemetry.txt"):
# 				z.write(path_name,  eachFolder)
# 			if os.path.isdir(path_name) and eachFolder != "Logs":
# 				for eachFile in os.listdir(path_name):
# 					int_path = path_name + os.sep + eachFile
# 					if eachFile.endswith("_em.pdb") or eachFile.endswith(os.path.basename(post.protein)) :
# 						if os.path.exists(int_path):
# 							z.write( int_path,  eachFolder + os.sep + eachFile)
# 					if eachFile.endswith("rmsf.xvg"):
# 							z.write( int_path ,  eachFolder + os.sep + eachFile)
# 					if os.path.isdir(int_path):
# 						if os.path.exists(int_path + os.sep + os.path.basename(int_path)):
# 							z.write(int_path + os.sep + os.path.basename(int_path), eachFolder + os.sep + os.path.basename(int_path)+ os.sep + os.path.basename(int_path))
# 						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):
# 							z.write(int_path + os.sep +  os.sep + "ligant_receptor.dlg",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "ligant_receptor.dlg")
# 						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dpf"):
# 							z.write(int_path + os.sep +  os.sep + "ligant_receptor.dpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "ligant_receptor.dpf")
# 						if os.path.exists(int_path + os.sep +  os.sep + "grid.gpf"):
# 							z.write(int_path + os.sep +  os.sep + "grid.gpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "grid.gpf")
# 						if os.path.exists(int_path + os.sep +  os.sep + "receptor.gpf"):
# 							z.write(int_path + os.sep +  os.sep + "receptor.gpf",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "receptor.gpf")
# 						if os.path.exists(int_path + os.sep +  os.sep + "receptor.pdbqt"):
# 							z.write(int_path + os.sep +  os.sep + "receptor.pdbqt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "receptor.pdbqt")			
# 						if os.path.exists(int_path + os.sep +  os.sep + "docking_log.txt"):
# 							z.write(int_path + os.sep +  os.sep + "docking_log.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "docking_log.txt")
# 						if os.path.exists(int_path + os.sep +  os.sep + "docking_output.txt"):
# 							z.write(int_path + os.sep +  os.sep + "docking_output.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "docking_output.txt")
# 						if os.path.exists(int_path + os.sep +  os.sep + "telemetry.txt"):
# 							z.write(int_path + os.sep +  os.sep + "telemetry.txt",  eachFolder + os.sep + os.path.basename(int_path)+ os.sep + "telemetry.txt")
# 			if eachFolder == "Logs":
# 				for eachFile in os.listdir(path_name):
# 					z.write(path_name + os.sep + eachFile, "Logs" + os.sep + eachFile)

# 		z.close()
# 		zip_file = open(temp, 'r')
# 		response = HttpResponse(zip_file, content_type='application/force-download')
# 		response['Content-Disposition'] = 'attachment; filename=' + name + ".zip"
# 		return response

		
# def show_experiment(request, name,docking_number):
# 	return HttpResponse('Not implemented yet!')

# def experiment_detail(request,name):
# 		post = get_object_or_404(Process, name=name)
# 		list_of_compounds = []

	
# 		for eachFolder in os.listdir(os.path.dirname(post.protein)):
# 			path_name = os.path.dirname(post.protein) + os.sep + eachFolder
# 			if os.path.isdir(path_name) and eachFolder != "Logs":
# 				for eachFile in os.listdir(path_name):
# 					int_path = path_name + os.sep + eachFile

# 					if os.path.isdir(int_path):
# 						if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


# 									dlg_file = int_path + os.sep + "ligant_receptor.dlg"
# 									list_of_folder_name =  dlg_file.split(os.sep)
# 									ligand_name = list_of_folder_name[-2]
# 									time_name = list_of_folder_name[-3][0:-7]
# 									parser = DlgParser(dlg_file)
# 									for eachStruct in parser.clist:
# 										if eachStruct['inhib_constant_units'] == 'uM':
# 											mag = math.pow(10,-6)
# 								 		else:
# 											mag = math.pow(10,-9)
# 										z = {'ki': eachStruct['inhib_constant']*mag, "time": 'os.path.basename(os.path.dirname(int_path))', "compound": os.path.basename(int_path),   'status':False, 'experiment_id': name, 'pose_number' : eachStruct['run'] }
# 										list_of_compounds.append(z)

# 		compounds = Compound(list_of_compounds)
# 		RequestConfig(request).configure(compounds)
# 		return render(
# 			request,
# 			'dyna/experiment_detail.html',
# 			{'post': post, 'compounds': compounds}
# 		)


# def show_pose(request, experiment_id,time, compound, pose):


# 		post = get_object_or_404(Process, name=experiment_id)
# 		best_model = post.folder + time + "_folder" + os.sep + compound + os.sep +  "receptor.pdbqt"
# 		# if time != "Control":
# 		# 	best_model = best_model + "_em.pdb"


# 		receptor_text = file(best_model,'r')
# 		text_ = "".join(receptor_text.readlines()[1:])
# 		receptor_text.close()



# 		context = {'experiment_id': experiment_id,'time': time, 'compound': compound, 'pose': pose}
# 		return render(request, 'dyna/show_pose.html', context)

# def log(request, name):
# 	alog = get_object_or_404(Process, name=name)
# 	x = os.path.dirname(alog.protein) + os.sep + "commands.txt"
# 	# print x
# 	zz = file(x,'r')
# 	k = "\n".join(zz.readlines())
# 	zz.close()
# 	context = {'experiment_id': k}
# 	return render(request, 'dyna/activeLog.html', context)



# def activeLog(request, name):
# 	alog = get_object_or_404(ActiveLog, name=name)
# 	x = alog.path
# 	print x
# 	zz = file(x,'r')
# 	k = "\n".join(zz.readlines())
# 	context = {'experiment_id': k}
# 	return render(request, 'dyna/activeLog.html', context)




# def experiment_detailn(request,name):
# 	if request.method == 'POST':

# 		mylist = request.session['mylist']


# 		list_of_input_ids=request.POST.getlist("bbb")

# 		new_list = []
# 		for eachTag in list_of_input_ids:
# 			for eachN in mylist:
# 				if eachN['compound'] == eachTag.split('*')[0] and eachN['conformation'] == eachTag.split('*')[1]:
# 					new_list.append(eachN)
# 		# print list_of_input_ids
# 		request.session['mylist2'] = new_list
# 		url = "http://" + request.META['HTTP_HOST'] + reverse("second_stage",  kwargs={'name': name})
# 			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 		return HttpResponseRedirect(url)
# 	else:
# 		free_space = subprocess.check_output(['df','-h', TEMPORARYFOLDER]).split('\n')[1].split()[3].decode('utf-8')
# 		post = get_object_or_404(Process, name=name)
# 		has_cluster = False
# 		if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
# 			subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
# 			has_cluster = True

# 		try:
# 			if request.session['name'] != name:
# 				raise KeyError() 
# 			mytime = int(datetime.datetime.now().strftime('%s'))
# 			if mytime - request.session['time'] > 600:
# 				raise KeyError()
# 			list_of_compounds = request.session['mylist']
# 		except KeyError as e:
# 			request.session['name'] = name
# 			request.session['time'] = int(datetime.datetime.now().strftime('%s'))
# 			list_of_compounds = []

			
# 			for eachFolder in os.listdir(post.folder):
# 				# print post.folder
# 				path_name = post.folder + os.sep + eachFolder
# 				if os.path.isdir(path_name) and eachFolder != "Logs":
# 					for eachFile in os.listdir(path_name):
# 						int_path = path_name + os.sep + eachFile

# 						if os.path.isdir(int_path):
# 							if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


# 										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
# 										list_of_folder_name =  dlg_file.split(os.sep)
# 										ligand_name = list_of_folder_name[-2]
# 										time_name = list_of_folder_name[-3]
# 										parser = DlgParser(dlg_file)
# 										mylist = {}
# 										for eachStruct in parser.clist:
# 											if 'inhib_constant_units' in eachStruct:
# 												if eachStruct['inhib_constant_units'] == 'uM':
# 													mag = math.pow(10,-6)
# 										 		elif eachStruct['inhib_constant_units'] == 'mM':
# 													mag = math.pow(10,-3)
# 												elif eachStruct['inhib_constant_units'] == 'nM':
# 													mag = math.pow(10,-9)
# 												elif eachStruct['inhib_constant_units'] == 'pM':
# 													mag = math.pow(10,-12)
# 												elif eachStruct['inhib_constant_units'] == 'fM':
# 													mag = math.pow(10,-15)
# 												elif eachStruct['inhib_constant_units'] == 'aM':
# 													mag = math.pow(10,-18)
# 												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
# 										better_Ki = math.pow(10,0)
# 										docking_id_better_Ki = None
# 										better_binding_energy = 100
# 										for eachElem in mylist:

# 											ki = mylist[eachElem][0]
# 											binding_energy = mylist[eachElem][1]

# 											if binding_energy <  better_binding_energy:
# 												docking_id_better_Ki = eachElem
# 												better_Ki = ki
# 												better_binding_energy = binding_energy
# 										if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"):
# 											z = {'ki': better_Ki, "time": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'folder' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"}
# 										else:
# 											z = {'ki': better_Ki, "time": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'folder' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein)}
# 										list_of_compounds.append(z)

# 				request.session['mylist'] = list_of_compounds


# 		compounds = Compound(list_of_compounds)
# 		# compounds.paginate = False
# 		RequestConfig(request, paginate=False).configure(compounds)
# 		# for eachElem in compounds:
# 		# 	print eachElem
# 		return render(
# 			request,
# 			'dyna/experiment_detail.html',
# 			{'free_space': free_space, 'post' : post, 'compounds': compounds, 'has_cluster' : has_cluster}
# 		)



# def experiment_detail3(request,name):
# 	if request.method == 'POST':

# 		mylist = request.session['mylist']


# 		list_of_input_ids=request.POST.getlist("bbb")

# 		new_list = []
# 		for eachTag in list_of_input_ids:
# 			for eachN in mylist:
# 				if eachN['compound'] == eachTag.split('*')[0] and eachN['conformation'] == eachTag.split('*')[1]:
# 					new_list.append(eachN)
# 		# print list_of_input_ids
# 		request.session['mylist2'] = new_list
# 		url = "http://" + request.META['HTTP_HOST'] + reverse("second_stage",  kwargs={'name': name})
# 			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 		return HttpResponseRedirect(url)
# 	else:
# 		free_space = subprocess.check_output(['df','-h', TEMPORARYFOLDER]).split('\n')[1].split()[3].decode('utf-8')
# 		post = get_object_or_404(Process, name=name)
# 		has_cluster = False
# 		if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
# 			subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
# 			has_cluster = True

# 		try:
# 			if request.session['name'] != name:
# 				raise KeyError() 
# 			mytime = int(datetime.datetime.now().strftime('%s'))
# 			if mytime - request.session['time'] > 600:
# 				raise KeyError()
# 			list_of_compounds = request.session['mylist']
# 		except KeyError as e:
# 			request.session['name'] = name
# 			request.session['time'] = int(datetime.datetime.now().strftime('%s'))
# 			list_of_compounds = []

			
# 			for eachFolder in os.listdir(post.folder):
# 				# print post.folder
# 				path_name = post.folder + os.sep + eachFolder
# 				if os.path.isdir(path_name) and eachFolder != "Logs":
# 					for eachFile in os.listdir(path_name):
# 						int_path = path_name + os.sep + eachFile

# 						if os.path.isdir(int_path):
# 							if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


# 										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
# 										list_of_folder_name =  dlg_file.split(os.sep)
# 										ligand_name = list_of_folder_name[-2]
# 										time_name = list_of_folder_name[-3]
# 										parser = DlgParser(dlg_file)
# 										mylist = {}
# 										for eachStruct in parser.clist:
# 											if 'inhib_constant_units' in eachStruct:
# 												if eachStruct['inhib_constant_units'] == 'uM':
# 													mag = math.pow(10,-6)
# 										 		elif eachStruct['inhib_constant_units'] == 'mM':
# 													mag = math.pow(10,-3)
# 												elif eachStruct['inhib_constant_units'] == 'nM':
# 													mag = math.pow(10,-9)
# 												elif eachStruct['inhib_constant_units'] == 'pM':
# 													mag = math.pow(10,-12)
# 												elif eachStruct['inhib_constant_units'] == 'fM':
# 													mag = math.pow(10,-15)
# 												elif eachStruct['inhib_constant_units'] == 'aM':
# 													mag = math.pow(10,-18)
# 												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
# 										better_Ki = math.pow(10,0)
# 										docking_id_better_Ki = None
# 										better_binding_energy = 100
# 										for eachElem in mylist:

# 											ki = mylist[eachElem][0]
# 											binding_energy = mylist[eachElem][1]

# 											if binding_energy <  better_binding_energy:
# 												docking_id_better_Ki = eachElem
# 												better_Ki = ki
# 												better_binding_energy = binding_energy
# 										if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"):
# 											z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein) + "_em.pdb"}
# 										else:
# 											z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(post.protein)}
# 										list_of_compounds.append(z)

# 				request.session['mylist'] = list_of_compounds


# 		compounds = Compound(list_of_compounds)
# 		# compounds.paginate = False
# 		RequestConfig(request, paginate=False).configure(compounds)
# 		# for eachElem in compounds:
# 		# 	print eachElem
# 		return render(
# 			request,
# 			'dyna/experiment_detail_n.html',
# 			{'free_space': free_space, 'post' : post, 'compounds': compounds, 'has_cluster' : has_cluster}
# 		)

# def delete_task(request, name):
# 	post = get_object_or_404(Process, name=name)
# 	workdir = post.folder
# 	shutil.rmtree(workdir)
# 	return render(request,'dyna/delete_task.html', {'name' : name} )

# def second_stage(request, name):
# 	if request.method == 'POST':

# 		form = DockingFormStage2(request.POST, request.FILES)
# 		if form.is_valid():
# 			type_docking = form.cleaned_data['type_docking']
# 			mylist = request.session['mylist2']
# 			workdir = tempfile.mkdtemp(dir=TEMPORARYFOLDER) + os.sep
# 			ligandList = []
# 			for eachLigand in mylist:
# 				time_folder = workdir + os.sep + os.path.basename(os.path.dirname(eachLigand['download_complex']))
# 				if not os.path.exists(time_folder):
# 					os.mkdir(time_folder)
# 					shutil.copyfile(eachLigand['receptor'], time_folder + os.sep + os.path.basename(eachLigand['receptor']))
# 				os.mkdir(time_folder + os.sep + eachLigand['compound'])
# 				ligand_path = time_folder + os.sep + eachLigand['compound'] + os.sep + eachLigand['compound']
# 				shutil.copyfile(eachLigand['download_complex'] + os.sep + eachLigand['compound'], time_folder + os.sep + eachLigand['compound'] + os.sep + eachLigand['compound'])
# 				ligandList.append({ 'ligand' : ligand_path, 'receptor' : time_folder + os.sep + os.path.basename(eachLigand['receptor'] )})

# 			post = get_object_or_404(Process, name=name)
# 			task_id = async(sleep_task3, workdir,ligandList,post.chain,post.residue_number, post.size, type_docking)
# 			process = Process.objects.create(name= task_id,
# 				alive = False,
# 				folder = workdir,
# 				protein = os.path.basename(post.protein),
# 				chain = post.chain,
# 				residue_number = post.residue_number,
# 				type_docking = type_docking,
# 				size = post.size,
# 				fail = False,
# 				user_email = post.user_email,
# 				projectName = post.projectName,
# 				site=request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id}))
# 				# site=request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id}))
# 			process.publish()
# 			url = "http://" + request.META['HTTP_HOST'] + reverse("experiment_detail2",  kwargs={'name': task_id})
# 			# url = "http://" + request.META['HTTP_HOST'] + reverse("task",  kwargs={'name': task_id})

# 			return HttpResponseRedirect(url)
# 	else:
# 		form = DockingFormStage2()  # A empty, unbound form
# 		new_list = request.session['mylist2']
# 		# print new_list
# 		compounds = CompoundLite(new_list)
# 		RequestConfig(request, paginate=False).configure(compounds)
# 		return render(
# 			request,
# 			'dyna/experiment_detail3B.html',
# 			{'compounds' : compounds, 'form' : form, 'name' : name}
# 		)
# def queue_monitor(request):
# 		number_of_cluster = len(Stat.get_all())
# 		return render(
# 			request,
# 			'dyna/queue_monitor.html',
# 			{'number_of_cluster' : number_of_cluster, 'clusters' : Stat.get_all()}
# 		)		
		
# def experiment_detail2(request,name):
# 		post = get_object_or_404(Process, name=name)
# 		list_of_compounds = []
# 		has_cluster = False
# 		ligands = LigandFile.objects.all().filter(name_of_Process=name)
# 		print post.folder
# 		subprocess.call(['python', settings.BASE_DIR + os.sep + 'joao.py',post.folder])
		
# 		# subprocess.call(["gs","-sDEVICE=jpeg","-dJPEGQ=100","-dNOPAUSE","-dBATCH","-dSAFER","-r300","-sOutputFile=" + os.path.dirname(post.protein) + os.sep + "output.jpg", os.path.dirname(post.protein) + os.sep + "cluster100ns.eps"])
# 		if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
# 			subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
# 			has_cluster = True
# 		try:
# 			if request.session['name'] != name:
# 				raise KeyError() 
# 			mytime = int(datetime.datetime.now().strftime('%s'))
# 			if mytime - request.session['time'] > 600:
# 				raise KeyError()
# 			list_of_compounds = request.session['mylist']
# 		except KeyError as e:

# 			for eachFolder in os.listdir(post.folder):
# 				path_name = post.folder + os.sep + eachFolder
# 				if os.path.isdir(path_name) and eachFolder != "Logs":
# 					for eachFile in os.listdir(path_name):
# 						int_path = path_name + os.sep + eachFile

# 						if os.path.isdir(int_path):
# 							if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


# 										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
# 										list_of_folder_name =  dlg_file.split(os.sep)
# 										ligand_name = list_of_folder_name[-2]
# 										time_name = list_of_folder_name[-3]
# 										parser = DlgParser(dlg_file)
# 										mylist = {}
# 										# print dlg_file
# 										for eachStruct in parser.clist:
# 											if 'inhib_constant_units' in eachStruct:
# 												if eachStruct['inhib_constant_units'] == 'uM':
# 													mag = math.pow(10,-6)
# 										 		elif eachStruct['inhib_constant_units'] == 'mM':
# 													mag = math.pow(10,-3)
# 												elif eachStruct['inhib_constant_units'] == 'nM':
# 													mag = math.pow(10,-9)
# 												elif eachStruct['inhib_constant_units'] == 'pM':
# 													mag = math.pow(10,-12)
# 												elif eachStruct['inhib_constant_units'] == 'fM':
# 													mag = math.pow(10,-15)
# 												elif eachStruct['inhib_constant_units'] == 'aM':
# 													mag = math.pow(10,-18)
# 												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
# 										better_Ki = math.pow(10,0)
# 										docking_id_better_Ki = None
										
# 										for eachElem in mylist:
# 											ki = mylist[eachElem][0]
# 											binding_energy = mylist[eachElem][1]

# 											# if ki <  better_Ki:
# 											docking_id_better_Ki = eachElem
# 											# 	better_Ki = ki
# 											z = {'ki': ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':False, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': binding_energy}
# 											list_of_compounds.append(z)
# 					request.session['mylist'] = list_of_compounds

# 		compounds = CompoundLite(list_of_compounds)
# 		compounds.exclude = ('select')
# 		ligand_list = get_pdbqts(name)
# 		RequestConfig(request, paginate={'per_page': 50}).configure(compounds)
# 		return render(
# 			request,
# 			'dyna/experiment_detail3.html',
# 			{'post' : post, 'compounds': compounds, 'ligands': ligand_list, 'has_cluster' : has_cluster }
# 		)
# def clusterimg(request, name):
# 	post = get_object_or_404(Process, name=name)
# 	workdir = post.folder
# 	mycluster_img_eps_format = workdir + os.sep + "cluster100ns.eps"
# 	mycluster_img = workdir + os.sep + "output.png"
# 	if os.path.exists(os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps'):
# 		subprocess.call(['convert', '-density','100', os.path.dirname(post.protein) + os.sep + 'cluster100ns.eps', '-flatten', os.path.dirname(post.protein) + os.sep +'output.png'])
# 		# has_cluster = True
# 	response = HttpResponse(content_type="image/png")
# 	# request.session['dopeimg'] = workdir + evaluate_energy_manager.__output_files__[0]
# 	im = Image.open(mycluster_img)
# 	im.save(response, "PNG")
# 	return response

# def boxplot(request, name, ligand):
# 	post = get_object_or_404(Process, name=name)
# 	workdir = post.folder
# 	# print workdir + os.sep + ligand + "_" + post.type_docking + "_.csv boxplot.png"
# 	if (os.path.exists(workdir + os.sep + ligand + "_" + "10" + "_.csv boxplot.png")):
# 		boxplot_file = workdir + os.sep + ligand + "_" + "10" + "_.csv boxplot.png"
# 	else:
# 		boxplot_file = settings.BASE_DIR + os.sep + "withoutInf.png"
# 		print boxplot_file
# 	response = HttpResponse(content_type="image/png")
# 	im = Image.open(boxplot_file)
# 	im.save(response, "PNG")
# 	return response

# def get_pdbqts(name):
# 	mylist = []
# 	post = get_object_or_404(Process, name=name)
# 	workdir = post.folder
# 	for eachFile in os.walk(workdir):
# 			if eachFile[0].endswith(".pdbqt"):
# 				mylist.append(os.path.basename(eachFile[0]))

# 	return mylist



# def debug(request, name):
# 	instance = get_object_or_404(Process, name=name)
# 	last_command_list = os.path.dirname(instance.protein) + os.sep + "commands.txt"
# 	last_command = None
# 	number_of_runned_steps = 0
# 	if os.path.exists(last_command_list):
# 		last_command_stub = file(last_command_list,'r')
# 		lines_of_command_file = last_command_stub.readlines()
# 		last_command_stub.close()
# 		last_command = lines_of_command_file[-1]
# 		number_of_runned_steps = len(lines_of_command_file)
	
# 	count_ligands = 0
# 	count_processed_ligands = 0
# 	clusters = []
# 	cluster_todos = []
# 	count_ligands_in_each_cluster = []
# 	error_ligands_in_each_cluster = []
# 	for eachFile in os.listdir(instance.folder):
# 		if os.path.isdir(instance.folder + os.sep + eachFile) and (not eachFile ==  "Control_folder"):
# 			clusters.append(eachFile)
# 		if eachFile.endswith(".pdbqt"):
# 			count_ligands += 1
# 		if os.path.isdir(instance.folder + os.sep + eachFile):
# 			cluster_count = 0
# 			error_ligands = 0
# 			cluster_todos.append(eachFile)
# 			for eachF in os.listdir(instance.folder + os.sep + eachFile):
# 				if os.path.exists(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'ligant_receptor.dlg'):
# 					count_processed_ligands += 1
# 					cluster_count += 1
# 				if os.path.exists(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'last_output.txt'):
# 					myfile = file(instance.folder + os.sep + eachFile + os.sep + eachF + os.sep + 'last_output.txt', 'r')
# 					try:
# 						erro_list = myfile.readlines()[0]
						
# 						# print erro_list
# 						if erro_list != '/usr/local/bin//autodock4: WARNING: Using autodock4.0 unbound extended model in autodock4.2!\n':
# 							error_ligands +=1
# 						pass
# 					except Exception as e:
# 						pass
# 					myfile.close()
# 			count_ligands_in_each_cluster.append(cluster_count)
# 			error_ligands_in_each_cluster.append(error_ligands)

# 	missing_simulation_files = check_simulation_files(instance.folder, os.path.basename(instance.protein), clusters)
	
# 	twi = zip(cluster_todos, count_ligands_in_each_cluster, error_ligands_in_each_cluster)

# 	return render(request, 'dyna/debug.html', {'twi' : twi, 'clusterB' : cluster_todos, 'count_ligands_in_each_cluster' : count_ligands_in_each_cluster, 'count_processed_ligands' : count_processed_ligands, 'count_ligands': count_ligands, 'clusters': clusters, 'number_of_runned_steps' : number_of_runned_steps , 'missing_files' : missing_simulation_files,'last_command' : last_command, 'post' : instance})

# def dockingLogFiles(request, name, time, compound):
# 	instance = get_object_or_404(Process, name=name)
# 	real_path_commands = instance.folder + os.sep + time	 + os.sep + compound + os.sep + 'commands.txt'
# 	real_path = instance.folder + os.sep + time	 + os.sep + compound + os.sep + 'last_output.txt'
# 	k = ""
# 	if os.path.exists(real_path_commands):
# 		commands_file = file(real_path_commands, 'r')
# 		k = ">>>> Commands executed: \n\n" + "\n".join(commands_file.readlines())

# 	logfile = file(real_path, 'r')
# 	k = k + "\n\n\n>>>> The last command caused the following error:\n\n" + "\n".join(logfile.readlines())
# 	logfile.close()
# 	context = {'experiment_id': k}
# 	return render(request, 'dyna/activeLog.html', context)


# def dockingLog(request, name, time):
# 	instance = get_object_or_404(Process, name=name)
# 	real_path = instance.folder + os.sep + time
# 	docking_with_erros = []
# 	for eachFile in os.listdir(real_path):

# 		if os.path.isdir(real_path + os.sep + eachFile):
# 			full_path = real_path + os.sep + eachFile
# 			if os.path.exists(full_path + os.sep + 'last_output.txt'):
# 				myfile = file(full_path + os.sep +  'last_output.txt', 'r')

# 				erro_list = myfile.readlines()
# 				first_line = erro_list[0]
# 				myfile.close()
				
# 				if first_line != '/usr/local/bin//autodock4: WARNING: Using autodock4.0 unbound extended model in autodock4.2!\n':
# 					docking_with_erros.append(eachFile)

# 	return render(request, 'dyna/dockingLog.html', {'name': name, 'time': time, 'docking_with_erros' : docking_with_erros})
# def simulationLog(request, name):
# 	instance = get_object_or_404(Process, name=name)
# 	list_of_logs = ['_em.log', '_md.log', '_npt.log']
# 	list_of_finded_logs = []
# 	for eachLogFile in list_of_logs:
# 		filename = instance.protein + eachLogFile
# 		if os.path.exists(filename):
# 			list_of_finded_logs.append(os.path.basename(filename))

# 	list_of_finded_logs.append('cluster.log')
# 	return render(request, 'dyna/show_logs.html', { 'list_of_files' : list_of_finded_logs, 'post' : instance})

# def readLog(request, name, logname):
# 	if logname.endswith(".log"):
# 		instance = get_object_or_404(Process, name=name)
# 		folder = instance.folder 
# 		logfilename = folder + os.sep + logname
# 		logfile = file(logfilename, 'r')
# 		k = "\n".join(logfile.readlines())
# 		logfile.close()
# 		context = {'experiment_id': k}
# 		return render(request, 'dyna/activeLog.html', context)
# 	else:
# 		return None

# def check_simulation_files(workdir, protein, cluster):
# 	missing_files = []
# 	sufix_of_necessary_files = ['.gro', '_s.gro',
# 	'_ion.tpr', '_ion.gro', '_em.tpr', '_em.gro', '_nvt.tpr', '_nvt.gro',
# 	'_md.tpr', '_md.trr', '_md.xtc', '_em.trr', '_nvt.trr', '_md.trr', '_cluster.pdb']
# 	other_necessay_files = ['Integrin.ndx', 'output.ndx', 'ions.mdp', 'topol.top',
# 	'trajout.xtc', 'rmsd-clust.xpm', 'cluster100ns.eps',
# 	]
# 	# print cluster
# 	for eachFolder in cluster:
# 		folder = workdir + os.sep + eachFolder
# 		sufix_of_necessary_filesB = ['_md.tpr', '_md.pdb', '_em.gro', '_em.top',
# 		 '_em.tpr']
# 		other_files = ['l-bfgs_after_md.mdp', 'Integrin.ndx']
# 		for eachFile in sufix_of_necessary_filesB:
# 			path_name = folder + os.sep + protein + eachFile
# 			if not os.path.exists(path_name):
# 				missing_files.append(path_name)
# 		for eachFile in other_necessay_files:
# 			path_name = workdir + os.sep + eachFile
# 			if not os.path.exists(path_name):
# 				missing_files.append(path_name)

# 	for eachFile in sufix_of_necessary_files:
# 		path_name = workdir + os.sep + protein + eachFile
# 		if not os.path.exists(path_name):
# 			missing_files.append(path_name)

# 	for eachFile in other_necessay_files:
# 		path_name = workdir + os.sep + eachFile
# 		if not os.path.exists(path_name):
# 			missing_files.append(path_name)
# 	return missing_files

# def log(request, name):
# 	alog = get_object_or_404(Process, name=name)
# 	x = os.path.dirname(alog.protein) + os.sep + "commands.txt"
# 	# print x
# 	zz = file(x,'r')
# 	k = "\n".join(zz.readlines())
# 	zz.close()
# 	context = {'experiment_id': k}
# 	return render(request, 'dyna/activeLog.html', context)



# class Compound(tables.Table):


# 		experiment_id = tables.Column(visible=False)
# 		pose_number = tables.LinkColumn(viewname='show_pose', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
# 		compound = tables.Column()
# 		ki = tables.Column()
# 		conformation = tables.Column()
# 		binding_energy = tables.Column()

# 		template = '<input type="checkbox" name="bbb" value="{{ record.compound }}' + '*' + '{{ record.conformation }}" />'


# 		download_complex = tables.LinkColumn(text = "Download!", viewname='download_protein_ligand', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
# 		refine = tables.TemplateColumn(template)
# 		class Meta:
# 			template_name = 'django_tables2/bootstrap.html'
# 			table_pagination = False
# 			# # model = vars
# 			# fields = ('idvars')
# 			attrs = {
# 			'th' : {
# 					'_ordering': {
# 						'orderable': 'sortable', # Instead of `orderable`
# 						'ascending': 'ascend',   # Instead of `asc`
# 						'descending': 'descend'  # Instead of `desc`
# 					}
# 				}
# 			}


		
# class CompoundLite(tables.Table):


# 		experiment_id = tables.Column(visible=False)
# 		pose_number = tables.LinkColumn(viewname='show_pose', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
# 		compound = tables.Column()
# 		ki = tables.Column()
# 		conformation = tables.Column()
# 		binding_energy = tables.Column()

# 		# template = '<input type="checkbox" name="bbb" value="{{ record.compound }}' + '*' + '{{ record.conformation }}" />'

# 		# refine = tables.TemplateColumn(template)

# 		download_complex = tables.LinkColumn(text = "Download!", viewname='download_protein_ligand', args=[A('experiment_id'), A('conformation'),A('compound'), A('pose_number')], attrs={'a': {'target': '_blank'}})
# 		class Meta:
# 			template_name = 'django_tables2/bootstrap.html'
# 			table_pagination = False
# 			# # model = vars
# 			# fields = ('idvars')
# 			attrs = {
# 			'th' : {
# 					'_ordering': {
# 						'orderable': 'sortable', # Instead of `orderable`
# 						'ascending': 'ascend',   # Instead of `asc`
# 						'descending': 'descend'  # Instead of `desc`
# 					}
# 				}
# 			}
