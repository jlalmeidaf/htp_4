import os, sys, math
from reportlab.pdfgen import canvas
from reportlab.lib import colors
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
sys.path.append(os.environ['MGLTOOLS'] + '/MGLToolsPckgs/')
from AutoDockTools.DlgParser import DlgParser
import subprocess, csv


table = {}

def main(dirname):
	# print dirname
	cnv = canvas.Canvas(dirname + os.sep + "analisys.pdf")
	for x in os.walk(dirname):
	    if x[0].endswith(".pdbqt"):
	    	# print x[0]
	        myfunc(x[0])

	image_path = mydict2(dirname, 10)
	# image_path2 = mydict2(dirname, 30)
	time_list = geraTables(dirname)
	for x in range(0,len(image_path)):
		first_image = image_path[x]
		# second_image = image_path2[x]
		cnv.drawString( 20, 820, "Analysis of the 10 best dockings of " + os.path.basename(first_image)[:-8])
		cnv.drawImage(first_image + " boxplot.png", 100,450, width=350, height=350)
		# cnv.drawString( 20, 450, "Analysis of all dockings of " + os.path.basename(first_image)[:-8])
		# cnv.drawImage(second_image + " boxplot.png", 100,90, width=350, height=350)
		posX =  100
		for eachTime in time_list:
			cnv.drawString( posX, 400, eachTime)
			if os.path.exists(dirname + os.sep + os.path.basename(first_image)[:-8] + eachTime + ".csv"):
				with open(dirname + os.sep + os.path.basename(first_image)[:-8] + eachTime + ".csv") as myfile:
					reader = csv.reader(myfile, delimiter=',')
					data = list(reader)
				t=Table(data)
				# t.setStyle(TableStyle([('BACKGROUND',(1,1),(-2,-2),colors.green),
				#                        ('TEXTCOLOR',(0,0),(1,-1),colors.red)]))
				t.wrapOn(cnv, 0, 0)
				t.drawOn(cnv,posX,200)
				posX = posX + 150
		cnv.showPage()
	cnv.save()
	
	# doc = SimpleDocTemplate("simple_table.pdf", pagesize=letter)
	# container for the 'Flowable' objects
	 




def myfunc(path):
    dlg_file = path + os.sep + "ligant_receptor.dlg"
    list_of_folder_name =  dlg_file.split(os.sep)
    ligand_name = list_of_folder_name[-2]
    time_name = list_of_folder_name[-3]
    if os.path.exists(dlg_file):
	    parser = DlgParser(dlg_file)
	    mylist = []
	    for eachStruct in parser.clist:
	    	if('inhib_constant' in eachStruct):
		    	if 'inhib_constant_units' in eachStruct:
					if eachStruct['inhib_constant_units'] == 'uM':
						mag = math.pow(10,-6)
			 		elif eachStruct['inhib_constant_units'] == 'mM':
						mag = math.pow(10,-3)
					elif eachStruct['inhib_constant_units'] == 'nM':
						mag = math.pow(10,-9)
					elif eachStruct['inhib_constant_units'] == 'pM':
						mag = math.pow(10,-12)
					elif eachStruct['inhib_constant_units'] == 'fM':
						mag = math.pow(10,-15)
					elif eachStruct['inhib_constant_units'] == 'aM':
						mag = math.pow(10,-18)
		    	mylist.append([eachStruct['inhib_constant']*mag, parser.clist.index(eachStruct)])
	    mylist.sort()
	    table_col = {} 
	    table_col[time_name] = mylist
	    if table.has_key(ligand_name):
	        ligand_table = table[ligand_name]
	        ligand_table[time_name] = mylist
	        table[ligand_name] = ligand_table
	    else:
	        table[ligand_name] = table_col

def mydict2(path, size):
        list_of_files = []
        for eachLigand in table:
                #print eachLigand
                title = ""
                for eachTime in table[eachLigand]:
                                title = title + eachTime[:-7] + ","
                ligandFile = file(path + os.sep + eachLigand + "_" + str(size) + "_" +".csv","w")
                ligandFile.write(title[:-1] + "\n")
                for x in xrange(0,size):
                            line = ""
                            for eachTime in table[eachLigand]:
                                    columm = table[eachLigand]
                                    myc = columm[eachTime]
                                    #myc.sort()
                                    try:
                                    	line = line + str(myc[x][0]) + ","
                                    except Exception as e:
                                    	pass
                                    
                            ligandFile.write(line[:-1] + "\n")
                ligandFile.close()
                subprocess.call(['Rscript', os.path.dirname(os.path.abspath(__file__)) + os.sep + 'geraGraph.r', path + os.sep + eachLigand + "_" + str(size) + "_" +".csv"])
                list_of_files.append(path + os.sep + eachLigand + "_" + str(size) + "_" +".csv")
        return list_of_files

def geraTables(path):
	list_of_times = []
	for eachLigand in table:
		for eachTime in table[eachLigand]:
			name = path + os.sep + eachLigand + eachTime + ".csv"
			myfile = file(name, "w")
			text = "pos" + "," + "Ki\n"
			for x in range(0,10):
				columm = table[eachLigand]
				myc = columm[eachTime]
				text = text + str(myc[x][1]) + "," + str(myc[x][0]) + "\n"
			myfile.write(text)
			myfile.close()
			if eachTime  not in list_of_times:
				list_of_times.append(eachTime)
	return list_of_times



if __name__ == '__main__':
	# print sys.argv[1]
	main(sys.argv[1])
