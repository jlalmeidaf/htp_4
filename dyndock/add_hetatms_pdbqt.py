import sys
import os
def main():
	if os.path.exists(sys.argv[2]):
		pdbqt_file = file(sys.argv[1],'a')
		hetatm_pdbqt_file = file(sys.argv[2],'r')
		hetatm_lines = hetatm_pdbqt_file.readlines()
		for eachLine in hetatm_lines:
			pdbqt_file.write(eachLine)
		pdbqt_file.close()
		hetatm_pdbqt_file.close()


if __name__ == '__main__':
	main()