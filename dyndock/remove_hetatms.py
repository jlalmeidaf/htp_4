import sys
def main():
	input_pdb = sys.argv[1]
	output_pdb = sys.argv[2]
	output_het_pdb = sys.argv[3]
	input_file = file(input_pdb, "r")
	file_in_list = input_file.readlines()
	output_file = file(output_pdb, 'w')
	# output_het_file = file(output_het_pdb,'w')
	list_of_lines_with_het = []
	for eachLine in file_in_list:
		if (eachLine.startswith("ATOM") or eachLine.startswith("HETATM")) and not had_a_residue(eachLine):
			if get_resName(eachLine) != "CL" and get_resName(eachLine) != "NA":
				list_of_lines_with_het.append(line_in_pdbqt_format(eachLine))
		else:
			output_file.write(eachLine)

	if len(list_of_lines_with_het)>0:
		output_het_file = file(output_het_pdb,'w')
		output_het_file.writelines(list_of_lines_with_het)
		output_het_file.close()



	input_file.close()
	output_file.close()
def line_in_pdbqt_format(line):
	if 'Mg' in line:
		return line[0:66] + '{0:{fill}{align}10}'.format('1.200', fill=' ',align='>') + '{0:{fill}{align}3}'.format('Mg', fill=' ',align='>') + "\n"
	if 'Zn' in line:
		return line[0:66] + '{0:{fill}{align}10}'.format('0.8', fill=' ',align='>') + '{0:{fill}{align}3}'.format('Zn', fill=' ',align='>') + "\n"
	if 'Ca' in line:
		return line[0:66] + '{0:{fill}{align}10}'.format('0.8', fill=' ',align='>') + '{0:{fill}{align}3}'.format('Ca', fill=' ',align='>') + "\n"
	if 'Cl' in line:
		return line[0:66] + '{0:{fill}{align}10}'.format('-0.8', fill=' ',align='>') + '{0:{fill}{align}3}'.format('Cl', fill=' ',align='>') + "\n"

def had_a_residue(line):
	resList = ['ALA',
 'ARG',
 'ASN',
 'ASP',
 'CYS',
 'GLN',
 'GLU',
 'GLY',
 'HIS',
 'ILE',
 'LEU',
 'LYS',
 'MET',
 'PHE',
 'PRO',
 'SER',
 'THR',
 'TRP',
 'TYR',
 'VAL',
 'ASX',
 'GLX']
	if get_resName(line) in resList:
		return True
	else:
		return False

def get_resName(line):
	return line[17:20].strip()
if __name__ == '__main__':
	main()
