#!/bin/bash -e
#PATH=$PATH:/usr/local/gromacs/bin/
source /home/jorgehf/joao/gromacs_5.1.5_GPU/bin/GMXRC
source /home/jorgehf/joao/amber20/amber.sh
if [ -f /home/jorgehf/joao_htp_test/myenv/bin/activate ]; then
    echo   "Load Python virtualenv from '.venv/bin/activate'"
    source /home/jorgehf/joao_htp_test/myenv/bin/activate
fi
exec "$@"
