# make_ndx_protein_lig esta usando numeros para selecionar grupos
import subprocess, os, shutil, time
import psutil
import threading
from LogException import LogException
from dyndock import settings

ACPYPEFOLDER = settings.ACPYPEFOLDER
NT_CPU = settings.NT_CPU
NB = settings.NB


class MMBPSA():
	"""docstring for MMBPSA"""
	def __init__(self, complex_name):
		self.complex = complex_name
		self.workdir = os.path.dirname(self.complex)
		self.ligand = self.workdir + os.sep + "ligand.pdb"
		self.receptor = self.workdir + os.sep + "receptor.pdb"
		self.receptor_gro = None
		self.ligand_gro_path = self.workdir  + os.sep + "Ligand_GMX.gro"
		self.complex_gro_path = self.workdir + os.sep + "complex.gro"
		self.topology = self.workdir + os.sep + "topol.top"
		self.newbox = self.workdir + os.sep + "newbox.gro"
		self.ions_tpr = self.workdir + os.sep + "ions.tpr"
		self.solv_gro = self.workdir + os.sep + "solv.gro"
		self.solv_ions = self.workdir + os.sep + "solv_ions.gro"
		self.em_tpr = self.workdir + os.sep + "em.tpr"
		self.ligand_resnr_ndx = self.workdir + os.sep + "index_ligand.ndx"
		self.posre_ligand_itp = self.workdir + os.sep + "posre_ligand.itp"
		self.nvt_tpr = self.workdir + os.sep + "nvt.tpr" 
		self.em_gro = self.workdir + os.sep + "em.gro"
		self.protein_lig_ndx = self.workdir + os.sep + "index.ndx"
		self.npt_mdp = self.workdir + os.sep + "npt.mdp"
		self.npt_gro = self.workdir + os.sep + "npt.gro"
		self.npt_cpt = self.workdir + os.sep + "npt.cpt"
		self.nvt_gro = self.workdir + os.sep + "nvt.gro"
		self.nvt_cpt = self.workdir + os.sep + "nvt.cpt"
		self.npt_tpr = self.workdir + os.sep + "npt.tpr"
		self.md_xtc = self.workdir + os.sep + "md_0_10.xtc"
		self.md_tpr = self.workdir + os.sep + "md_0_10.tpr"
		self.full_energy_dat = self.workdir + os.sep + "full_energy.dat"
		self.pbsa_ndx_file = None
	
	def sep_ligand_from_receptor(self):
		complex_file = file(self.complex, 'r')
		lines = complex_file.readlines()
		ligand_file = file( self.ligand, 'w')
		receptor_file = file(self.receptor, 'w')

		for eachLine in lines:
			if len(eachLine) >= 21:
				if "L" in eachLine[21]:
					new_line = eachLine[:17] + "<0>" + eachLine[20:]
					ligand_file.write(new_line)
				else:
					receptor_file.write(eachLine)
		ligand_file.close()
		receptor_file.close()

	def add_hidrogen_in_ligand(self):
		commands = ['babel', '-h', self.ligand, self.ligand]
		self._exec_command(commands)

	def get_ligand_topology(self):
		mol2_path = self.workdir + os.sep + "Ligand.mol2"
		commands = ["antechamber", "-i", self.ligand, "-o", mol2_path, "-fi", "pdb", "-fo", "mol2", "-c", "gas"]
		self._exec_command(commands)
		commands = [ACPYPEFOLDER + os.sep + "acpype_lib/acpype.py", "-i", mol2_path, "-f"]
		self._exec_command(commands)
		self.ligand_gro_path = self.workdir  + os.sep + "Ligand_GMX.gro"
		shutil.copyfile(self.workdir + os.sep + "Ligand.acpype" + os.sep + "Ligand_GMX.gro", self.ligand_gro_path)
		shutil.copyfile(self.workdir + os.sep + "Ligand.acpype" + os.sep + "Ligand_GMX.itp", self.workdir  + os.sep + "Ligand_GMX.itp")
		shutil.copyfile(self.workdir + os.sep + "Ligand.acpype" + os.sep + "Ligand_GMX.top", self.workdir  + os.sep + "Ligand_GMX.top")


	def get_receptor_topology(self):
		self.receptor_gro = self.workdir + os.sep + "receptor.gro"
		commands = ["pdb2gmx", "-f", self.receptor, "-o", self.receptor_gro, "-ignh", '-ff', 'amber99sb-ildn', '-water', 'spce' ]
		self._exec_gromacs_command(commands) 

	def generate_complex_gro(self):
		self.complex_gro_path = self.workdir + os.sep + "complex.gro"

		complex_file = file(self.complex_gro_path, 'w')
		receptor_gro_file = file(self.receptor_gro, 'r')
		receptor_gro_lines = receptor_gro_file.readlines()

		ligand_gro_file = file(self.ligand_gro_path, 'r')
		ligand_gro_lines = ligand_gro_file.readlines()

		complex_size = int(receptor_gro_lines[1]) + int(ligand_gro_lines[1])	
		complex_file.write("HTP SurflecDock\n")
		complex_file.write(str(complex_size) + "\n")
		complex_file.writelines(receptor_gro_lines[2:-1])
		complex_file.writelines(ligand_gro_lines[2:-1])
		complex_file.write(receptor_gro_lines[-1])
		complex_file.close()
		receptor_gro_file.close()
		ligand_gro_file.close()

	def include_lig_topology_in_topol_top(self):
		self.topology = self.workdir + os.sep + "topol.top"
		topology_file = file(self.topology,'r')
		lines = topology_file.readlines()
		topology_file.close()
		self.new_topology_file = file(self.topology,'w')
		self.new_topology_file.writelines(lines[0:22])
		self.new_topology_file.write('#include "Ligand_GMX.itp"\n')
		self.new_topology_file.writelines(lines[22:])
		self.new_topology_file.write("Ligand     1\n")
		self.new_topology_file.close()

	def prepare_solvatation_box(self):
		self.newbox = self.workdir + os.sep + "newbox.gro"
		commands = ["editconf","-f",self.complex_gro_path,"-o",self.newbox,"-bt","triclinic","-d","1.0","-c"]
		self._exec_gromacs_command(commands)

	def generate_box(self):
		self.solv_gro = self.workdir + os.sep + "solv.gro"
		commands = ["solvate","-cp",self.newbox,"-cs","spc216.gro","-p",self.topology,"-o",self.solv_gro]
		self._exec_gromacs_command(commands)

	def write_ions_mdp(self):
		mdp_path = self.workdir + os.sep + "ions.mdp"
		content = '''
; LINES STARTING WITH ';' ARE COMMENTS
title		    = Minimization	; Title of run

; Parameters describing what to do, when to stop and what to save
integrator	    = steep		; Algorithm (steep = steepest descent minimization)
emtol		    = 1000.0  	; Stop minimization when the maximum force < 10.0 kJ/mol
emstep          = 0.01      ; Energy step size
nsteps		    = 50000	  	; Maximum number of (minimization) steps to perform
;nsteps		    = 5000	  	; Maximum number of (minimization) steps to perform

; Parameters describing how to find the neighbors of each atom and how to calculate the interactions
nstlist		    = 1		    ; Frequency to update the neighbor list and long range forces
cutoff-scheme   = Verlet
ns_type		    = grid		; Method to determine neighbor list (simple, grid)
rlist		    = 1.0		; Cut-off for making neighbor list (short range forces)
coulombtype	    = cutoff	; Treatment of long range electrostatic interactions
rcoulomb	    = 1.0		; long range electrostatic cut-off
rvdw		    = 1.0		; long range Van der Waals cut-off
pbc             = xyz 		; Periodic Boundary Conditions
'''		
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()

	def prepare_for_generate_ions(self):
		self.ions_tpr = self.workdir + os.sep + "ions.tpr"
		self.write_ions_mdp()
		commands = ["grompp","-f","ions.mdp","-c",self.solv_gro,"-p",self.topology,"-o",self.ions_tpr]
		self._exec_gromacs_command(commands)

	def generate_ions(self):
		self.solv_ions = self.workdir + os.sep + "solv_ions.gro"
		commands = ["genion","-s",self.ions_tpr,"-o",self.solv_ions,"-p",self.topology,"-pname","NA","-nname","CL","-neutral"]
		self._exec_gromacs_command(commands, "SOL")

	def write_em_mdp(self):
		mdp_path = self.workdir + os.sep + "em.mdp"
		content = '''
; LINES STARTING WITH ';' ARE COMMENTS
title		    = Minimization	; Title of run

; Parameters describing what to do, when to stop and what to save
integrator	    = steep		; Algorithm (steep = steepest descent minimization)
emtol		    = 1000.0  	; Stop minimization when the maximum force < 10.0 kJ/mol
emstep          = 0.01      ; Energy step size
nsteps		    = 50000	  	; Maximum number of (minimization) steps to perform
;nsteps		    = 5000	  	; Maximum number of (minimization) steps to perform

; Parameters describing how to find the neighbors of each atom and how to calculate the interactions
nstlist		    = 1		        ; Frequency to update the neighbor list and long range forces
cutoff-scheme   = Verlet
ns_type		    = grid		    ; Method to determine neighbor list (simple, grid)
rlist		    = 1.2		    ; Cut-off for making neighbor list (short range forces)
coulombtype	    = PME		    ; Treatment of long range electrostatic interactions
rcoulomb	    = 1.2		    ; long range electrostatic cut-off
vdwtype         = cutoff
vdw-modifier    = force-switch
rvdw-switch     = 1.0
rvdw		    = 1.2		    ; long range Van der Waals cut-off
pbc             = xyz 		    ; Periodic Boundary Conditions
DispCorr        = no
'''
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()

	def prepare_for_em(self):
		self.em_tpr = self.workdir + os.sep + "em.tpr"
		self.write_em_mdp()
		commands = ["grompp","-f","em.mdp","-c",self.solv_ions,"-p",self.topology,"-o",self.em_tpr]
		self._exec_gromacs_command(commands)

	def run_em(self):
		commands = ["mdrun","-v","-deffnm","em", "-nb", NB, "-nt", NT_CPU]
		self._exec_gromacs_command(commands)
		self.em_gro = self.workdir + os.sep + "em.gro"

	def make_ndx_for_restriction(self):
		parameters = '0 & ! a H*\nq\n'
		self.ligand_resnr_ndx = self.workdir + os.sep + "index_ligand.ndx"
		commands = ["make_ndx","-f",self.ligand_gro_path,"-o",self.ligand_resnr_ndx]
		self._exec_gromacs_command(commands, parameters)

	def run_restrictions_on_ligand(self):
		parameters = '3\nq\n'
		self.posre_ligand_itp = self.workdir + os.sep + "posre_ligand.itp" 
		commands = ["genrestr","-f",self.ligand_gro_path,"-n",self.ligand_resnr_ndx,"-o",self.posre_ligand_itp,"-fc","1000","1000","1000"]
		self._exec_gromacs_command(commands,parameters)

	def add_restriction_to_topol(self):
		self.topology = self.workdir + os.sep + "topol.top"
		topology_file = file(self.topology,'r')
		lines = topology_file.readlines()
		topology_file.close()
		new_topology_file = file(self.topology,'w')
		for eachLine in lines:
			new_topology_file.write(eachLine)
			if eachLine.startswith('#include "posre.itp"'):
				new_topology_file.write('#include "posre_ligand.itp"\n')
		new_topology_file.close()

	def _make_index_ndx(self):
		parameters = "q\n"
		self.protein_lig_ndx = self.workdir + os.sep + "index.ndx"
		commands = ["make_ndx","-f",self.em_gro,"-o",self.protein_lig_ndx ]
		self._exec_gromacs_command(commands, parameters)
		ndx_file = file(self.protein_lig_ndx, 'r')
		readlines = ndx_file.readlines()
		ndx_file.close()
		new_ndx_file = file(self.protein_lig_ndx, 'w')
		count_LIG = 0
		for eachLine in readlines:
			if eachLine.startswith("[ <0> ]"):
				count_LIG = count_LIG + 1
			if eachLine.startswith("[ <0> ]") and count_LIG > 1:
				new_ndx_file.write("[ ZIN_old ]\n")
			else:
				new_ndx_file.write(eachLine)
		new_ndx_file.close()

	def make_ndx_protein_lig(self):
		self._make_index_ndx()
		parameters = "\"Protein\" | \"<0>\"\nq\n"
		self.protein_lig_ndx = self.workdir + os.sep + "index.ndx"
		commands = ["make_ndx","-n",self.protein_lig_ndx,"-o",self.protein_lig_ndx ]
		self._exec_gromacs_command(commands, parameters)

	def write_nvt_mdp(self):
		mdp_path = self.workdir + os.sep + "nvt.mdp"
		self.nvt_mdp = mdp_path
		content = '''
title                   = Protein-ligand complex NVT equilibration 
define                  = -DPOSRES  ; position restrain the protein and ligand
; Run parameters
integrator              = md        ; leap-frog integrator
nsteps                  = 500000     ; 2 * 50000 = 1000 ps
;nsteps                  = 25000     ; 2 * 50000 = 100 ps
dt                      = 0.002     ; 2 fs
; Output control
nstenergy               = 5000   ; save energies every 1.0 ps
nstlog                  = 5000   ; update log file every 1.0 ps
nstxout-compressed      = 5000   ; save coordinates every 1.0 ps
; Bond parameters
continuation            = no        ; first dynamics run
constraint_algorithm    = lincs     ; holonomic constraints 
constraints             = h-bonds   ; bonds to H are constrained 
lincs_iter              = 1         ; accuracy of LINCS
lincs_order             = 4         ; also related to accuracy
; Neighbor searching and vdW
cutoff-scheme           = Verlet
ns_type                 = grid      ; search neighboring grid cells
nstlist                 = 20        ; largely irrelevant with Verlet
rlist                   = 1.2
vdwtype                 = cutoff
vdw-modifier            = force-switch
rvdw-switch             = 1.0
rvdw                    = 1.2       ; short-range van der Waals cutoff (in nm)
; Electrostatics
coulombtype             = PME       ; Particle Mesh Ewald for long-range electrostatics
rcoulomb                = 1.2       ; short-range electrostatic cutoff (in nm)
pme_order               = 4         ; cubic interpolation
fourierspacing          = 0.16      ; grid spacing for FFT
; Temperature coupling
tcoupl                  = V-rescale                     ; modified Berendsen thermostat
tc-grps                 = Protein_<0> Water_and_ions    ; two coupling groups - more accurate
tau_t                   = 0.1   0.1                     ; time constant, in ps
ref_t                   = 300   300                     ; reference temperature, one for each group, in K
; Pressure coupling
pcoupl                  = no        ; no pressure coupling in NVT
; Periodic boundary conditions
pbc                     = xyz       ; 3-D PBC
; Dispersion correction is not used for proteins with the C36 additive FF
DispCorr                = no 
; Velocity generation
gen_vel                 = yes       ; assign velocities from Maxwell distribution
gen_temp                = 300       ; temperature for Maxwell distribution
gen_seed                = -1        ; generate a random seed
'''
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()

	def prepare_for_nvt(self):
		self.write_nvt_mdp()		
		self.nvt_tpr = self.workdir + os.sep + "nvt.tpr"
		commands = ["grompp","-f",self.nvt_mdp,"-c",self.em_gro,"-r",self.em_gro,"-p",self.topology,"-n",self.protein_lig_ndx,"-o",self.nvt_tpr]
		self._exec_gromacs_command(commands)

	def run_nvt(self):
		commands = ["mdrun","-deffnm","nvt", "-nb", NB, "-nt", NT_CPU]
		self._exec_gromacs_command(commands)
		self.nvt_gro = self.workdir + os.sep + "nvt.gro"
		self.nvt_cpt = self.workdir + os.sep + "nvt.cpt"

	def write_npt_mdp(self):
		mdp_path = self.workdir + os.sep + "npt.mdp"
		self.npt_mdp = mdp_path
		content = '''
title                   = Protein-ligand complex NPT equilibration 
define                  = -DPOSRES  ; position restrain the protein and ligand
; Run parameters
integrator              = md        ; leap-frog integrator
nsteps                  = 500000     ; 2 * 500000 = 1000 ps
;nsteps                  = 25000     ; 2 * 50000 = 100 ps
dt                      = 0.002     ; 2 fs
; Output control
nstenergy               = 500       ; save energies every 1.0 ps
nstlog                  = 500       ; update log file every 1.0 ps
nstxout-compressed      = 500       ; save coordinates every 1.0 ps
; Bond parameters
continuation            = yes       ; continuing from NVT 
constraint_algorithm    = lincs     ; holonomic constraints 
constraints             = h-bonds   ; bonds to H are constrained 
lincs_iter              = 1         ; accuracy of LINCS
lincs_order             = 4         ; also related to accuracy
; Neighbor searching and vdW
cutoff-scheme           = Verlet
ns_type                 = grid      ; search neighboring grid cells
nstlist                 = 20        ; largely irrelevant with Verlet
rlist                   = 1.2
vdwtype                 = cutoff
vdw-modifier            = force-switch
rvdw-switch             = 1.0
rvdw                    = 1.2       ; short-range van der Waals cutoff (in nm)
; Electrostatics
coulombtype             = PME       ; Particle Mesh Ewald for long-range electrostatics
rcoulomb                = 1.2
pme_order               = 4         ; cubic interpolation
fourierspacing          = 0.16      ; grid spacing for FFT
; Temperature coupling
tcoupl                  = V-rescale                     ; modified Berendsen thermostat
tc-grps                 = Protein_<0> Water_and_ions    ; two coupling groups - more accurate
tau_t                   = 0.1   0.1                     ; time constant, in ps
ref_t                   = 300   300                     ; reference temperature, one for each group, in K
; Pressure coupling
pcoupl                  = Berendsen                     ; pressure coupling is on for NPT
pcoupltype              = isotropic                     ; uniform scaling of box vectors
tau_p                   = 2.0                           ; time constant, in ps
ref_p                   = 1.0                           ; reference pressure, in bar
compressibility         = 4.5e-5                        ; isothermal compressibility of water, bar^-1
refcoord_scaling        = com
; Periodic boundary conditions
pbc                     = xyz       ; 3-D PBC
; Dispersion correction is not used for proteins with the C36 additive FF
DispCorr                = no 
; Velocity generation
gen_vel                 = no        ; velocity generation off after NVT 
'''
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()	

	def prepare_for_npt(self):
		self.write_npt_mdp()		
		self.npt_tpr = self.workdir + os.sep + "npt.tpr"
		commands = ["grompp","-f",self.npt_mdp,"-c",self.nvt_gro,"-r",self.nvt_gro,"-t",self.nvt_cpt,"-p",self.topology,"-n",self.protein_lig_ndx,"-o",self.npt_tpr]
		self._exec_gromacs_command(commands)


	def run_npt(self):
		commands = ["mdrun","-deffnm","npt", "-nb", NB, "-nt", NT_CPU]
		self._exec_gromacs_command(commands)
		self.npt_gro = self.workdir + os.sep + "npt.gro"
		self.npt_cpt = self.workdir + os.sep + "npt.cpt"

	def write_md_mdp(self):
		mdp_path = self.workdir + os.sep + "md.mdp"
		self.md_mdp = mdp_path
		content = '''
title                   = Protein-ligand complex MD simulation 
; Run parameters
integrator              = md        ; leap-frog integrator
nsteps                  = 2500000   ; 2 * 5000000 = 10000 ps (10 ns)
;nsteps                  = 25000     ; 2 * 50000 = 100 ps
dt                      = 0.002     ; 2 fs
; Output control
nstenergy               = 10000      ; save energies every 20.0 ps
nstlog                  = 10000      ; update log file every 20.0 ps
nstxout-compressed      = 10000      ; save coordinates every 20.0 ps
; Bond parameters
continuation            = yes       ; continuing from NPT 
constraint_algorithm    = lincs     ; holonomic constraints 
constraints             = h-bonds   ; bonds to H are constrained
lincs_iter              = 1         ; accuracy of LINCS
lincs_order             = 4         ; also related to accuracy
; Neighbor searching and vdW
cutoff-scheme           = Verlet
ns_type                 = grid      ; search neighboring grid cells
nstlist                 = 20        ; largely irrelevant with Verlet
rlist                   = 1.2
vdwtype                 = cutoff
vdw-modifier            = force-switch
rvdw-switch             = 1.0
rvdw                    = 1.2       ; short-range van der Waals cutoff (in nm)
; Electrostatics
coulombtype             = PME       ; Particle Mesh Ewald for long-range electrostatics
rcoulomb                = 1.2
pme_order               = 4         ; cubic interpolation
fourierspacing          = 0.16      ; grid spacing for FFT
; Temperature coupling
tcoupl                  = V-rescale                     ; modified Berendsen thermostat
tc-grps                 = Protein_<0> Water_and_ions    ; two coupling groups - more accurate
tau_t                   = 0.1   0.1                     ; time constant, in ps
ref_t                   = 300   300                     ; reference temperature, one for each group, in K
; Pressure coupling 
pcoupl                  = Parrinello-Rahman             ; pressure coupling is on for NPT
pcoupltype              = isotropic                     ; uniform scaling of box vectors
tau_p                   = 2.0                           ; time constant, in ps
ref_p                   = 1.0                           ; reference pressure, in bar
compressibility         = 4.5e-5                        ; isothermal compressibility of water, bar^-1
; Periodic boundary conditions
pbc                     = xyz       ; 3-D PBC
; Dispersion correction is not used for proteins with the C36 additive FF
DispCorr                = no 
; Velocity generation
gen_vel                 = no        ; continuing from NPT equilibration 

'''
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()	

	def prepare_for_md(self):
		self.write_md_mdp()		
		self.md_tpr = self.workdir + os.sep + "md_0_10.tpr"
		commands = ["grompp","-f",self.md_mdp,"-c",self.npt_gro,"-t",self.npt_cpt,"-p",self.topology,"-n",self.protein_lig_ndx,"-o",self.md_tpr]
		self._exec_gromacs_command(commands)

	def run_md(self):
		commands = ["mdrun","-deffnm","md_0_10", "-nb", NB, "-nt", NT_CPU]
		self._exec_gromacs_command(commands)
		self.md_gro = self.workdir + os.sep + "md_0_10.gro"
		self.md_cpt = self.workdir + os.sep + "md_0_10.cpt"
		self.md_xtc = self.workdir + os.sep + "md_0_10.xtc"

	def write_pdsa_mdp(self):
		mdp_path = self.workdir + os.sep + "pbsa.mdp"
		self.pdsa_mdp = mdp_path
		content = '''
;Polar calculation: "yes" or "no"
polar		= yes

;=============
;PSIZE options
;=============
;Factor by which to expand molecular dimensions to get coarsegrid dimensions.
cfac 		= 1.5

;The desired fine mesh spacing (in A)
gridspace 	= 0.5

:Amount (in A) to add to molecular dimensions to get fine grid dimensions.
fadd 		= 5

;Maximum memory (in MB) available per-processor for a calculation.
gmemceil 	= 4000

;=============================================
;APBS kwywords for polar solvation calculation
;=============================================
;Charge of positive ions
pcharge 	= 1

;Radius of positive charged ions
prad		= 0.95

;Concentration of positive charged ions
pconc           = 0.150 

;Charge of negative ions
ncharge 	= -1

;Radius of negative charged ions
nrad		= 1.81

;Concentration of negative charged ions
nconc 		= 0.150

;Solute dielectric constant
pdie 		= 2

;Solvent dielectric constant
sdie 		= 80

;Reference or vacuum dielectric constant
vdie 		= 1

;Solvent probe radius
srad 		= 1.4

;Method used to map biomolecular charges on grid. chgm = spl0 or spl2 or spl4
chgm            = spl4

;Model used to construct dielectric and ionic boundary. srfm = smol or spl2 or spl4
srfm            = smol

;Value for cubic spline window. Only used in case of srfm = spl2 or spl4.
swin 		= 0.30

;Numebr of grid point per A^2. Not used when (srad = 0.0) or (srfm = spl2 or spl4)
sdens 		= 10

;Temperature in K
temp 		= 300

;Type of boundary condition to solve PB equation. bcfl = zero or sdh or mdh or focus or map
bcfl 		= mdh

;Non-linear (npbe) or linear (lpbe) PB equation to solve
PBsolver 	= lpbe


;========================================================
;APBS kwywords for Apolar/Non-polar solvation calculation
;========================================================
;Non-polar solvation calculation: "yes" or "no"
apolar		= yes

;Repulsive contribution to Non-polar 
;===SASA model ====

;Gamma (Surface Tension) kJ/(mol A^2)
gamma           = 0.0226778

;Probe radius for SASA (A)
sasrad          = 1.4

;Offset (c) kJ/mol
sasaconst       = 3.84982

;===SAV model===
;Pressure kJ/(mol A^3)
press           = 0

;Probe radius for SAV (A)
savrad          = 0

;Offset (c) kJ/mol
savconst        = 0

;Attractive contribution to Non-polar
;===WCA model ====
;using WCA method: "yes" or "no"
WCA             = no

;Probe radius for WCA
wcarad          = 1.20

;bulk solvent density in A^3
bconc		= 0.033428

;displacment in A for surface area derivative calculation
dpos		= 0.05

;Quadrature grid points per A for molecular surface or solvent accessible surface
APsdens		= 20

;Quadrature grid spacing in A for volume integral calculations
grid            = 0.45 0.45 0.45

;Parameter to construct solvent related surface or volume
APsrfm          = sacc

;Cubic spline window in A for spline based surface definitions
APswin          = 0.3

;Temperature in K
APtemp          = 300
'''
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()

	def calc_MmPbSaStat(self):
		script_path = os.path.dirname(os.path.realpath(__file__)) + os.sep + "MmPbSaStat.py"
		shutil.copyfile(script_path, self.workdir + os.sep + "MmPbSaStat.py")
		commands = ["python","MmPbSaStat.py","-m","energy_MM.xvg","-p","polar.xvg","-a","apolar.xvg"]
		self._exec_command(commands)

	def _generate_ndx_for_mmpbsa(self):
		self.pbsa_ndx_file = self.workdir + os.sep + "receptor_ligand.ndx"
		commands = ["make_ndx", "-n", "-o", self.pbsa_ndx_file]
		self._exec_gromacs_command(commands, 'q\n')
        

	def prepare_ndx_for_mmpbsa(self):
		self._generate_ndx_for_mmpbsa()
		ndx_file = file(self.pbsa_ndx_file,'r')
		readlines = ndx_file.readlines()
		ndx_file.close()
		lines = "".join(readlines)
		list_of_ndx = lines.split("[")
		self.receptor_ligand_ndx = self.workdir + "pbsa.ndx"
		pbsa_ndx_file = file(self.receptor_ligand_ndx, 'w')
		for eachLine in list_of_ndx:
			if eachLine.startswith(" Protein ]") or eachLine.startswith(" <0> ]"):
				pbsa_ndx_file.write("[" +  eachLine)
		pbsa_ndx_file.close()
        


	def run_mmbpsa_analisys(self):
		self.write_pdsa_mdp()	
		self.prepare_ndx_for_mmpbsa()

		parameters = "1\n2\nq\n"
		commands = ["g_mmpbsa","-f",self.md_xtc,"-s",self.md_tpr,"-n",self.protein_lig_ndx,"-i",self.pdsa_mdp,"-pdie","2","-pbsa","-decomp","-dt","20", "-b", "3000"]
		self._exec_command(commands, parameters)
		self.calc_MmPbSaStat()
		self.full_energy_dat = self.workdir + os.sep + "full_energy.dat"

	def generate_graphs(self):
		batch_path = self.workdir + os.sep + "grace.bat"
		batch = file(batch_path, 'w')
		batch.write('xaxis label "Time (ps)"\nyaxis label "\\f{Symbol}D\\f{}G\\sbinding\\N(Kcal/mol)"')
		batch.close()
		commands = ["grace","-block",self.full_energy_dat,"-bxy","1:17","-batch",batch_path, '-nosafe',  "-hdevice","PNG","-hardcopy","-printfile","binding_energy.png"]
		self._exec_command(commands)



	def _exec_gromacs_command(self, command_line, stdin_data=""):
		commands = ['gmx'] + command_line
		self._exec_command(commands, stdin_data)

	def _exec_command(self, command_line, stdin_data=""):
		file_commands = file(self.workdir + os.sep + "commands.txt","a")
		file_commands.write(" ".join(command_line) + "\n")
		file_commands.close()
		realpath = os.getcwd()
		os.chdir(self.workdir)
		output_data = file(self.workdir + os.sep + "last_output.txt", "w")
		process = subprocess.Popen(command_line, stdout=output_data, stderr=output_data, stdin=subprocess.PIPE, universal_newlines=True)
		watchdog = threading.Thread(target=self.watchdog_monitor, args=(process.pid,))
		watchdog.daemon=True
		watchdog.start()
		process.stdin.write(stdin_data)
		process.stdin.close()		
		output_data.close()
		os.chdir(realpath)
		if process.wait() != 0:
			raise LogException(" ".join(command_line),self.workdir + os.sep + "last_output.txt")

	def watchdog_monitor(self, pid):
		workdir = self.workdir
		while True:
			# print "laco"
			if os.path.exists(workdir + os.sep + 'venon.txt'):
				# print "killing " + str(pid)
				try:
					os.kill(pid, 9)
					break
				except OSError:
					break
			if pid not in psutil.pids():
				break
			time.sleep(5)

