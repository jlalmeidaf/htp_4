import unittest
from should_dsl import should, should_not
import shutil,os


import Docking


DYNDOCK_ENG_PATH = os.path.dirname(os.path.realpath(__file__)) + "/control_env"
DYNDOCK_TEST_PATH = os.path.dirname(os.path.realpath(__file__)) + "/test_content/docking"
os.environ['MGLTOOLS'] = "/home/jorgehf/Downloads/mgltools_x86_64Linux2_1.5.6"
os.environ['AUTODOCKFOLDER'] = "/usr/local/bin"


class CommandsDockingExamples(unittest.TestCase):
		# def test_if_prepare_ligand(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.wMG.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"

		# 	shutil.copyfile(receptor_test_path, receptor_path)
		# 	shutil.copyfile(ligand_test_path, ligand_path)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.prepare_ligand()

		# def test_if_prepare_receptor(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.pdb"

		# 	shutil.copyfile(receptor_test_path, receptor_path)
		# 	shutil.copyfile(ligand_test_path, ligand_path)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.prepare_receptor()
		# def test_if_prepare_GPF(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.pdb"

		# 	list_of_files = ["alphaVbeta3.pdb", "receptor.pdbqt", "A9e.pdbqt"]
		# 	copy_files(list_of_files)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.prepare_GPF("20", "62.541,45.808,75.184")

		# def test_if_prepare_GPF2(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.pdb"

		# 	list_of_files = ["alphaVbeta3.pdb", "receptor.pdbqt", "A9e.pdbqt", "grid.gpf"]
		# 	copy_files(list_of_files)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.prepare_GPF2()

		# def test_if_run_autogrid(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.pdb"

		# 	list_of_files = ["alphaVbeta3.pdb", "receptor.pdbqt", "A9e.pdbqt", "grid.gpf", "receptor.gpf"]
		# 	copy_files(list_of_files)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.run_autogrid()

		def test_if_compact_maps(self):
			clear_control_env()
			receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.pdb"
			ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
			ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
			receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.pdb"

			list_of_files = ["receptor.A.map", "receptor.C.map", "receptor.d.map", "receptor.e.map", "receptor.gpf"]
			copy_files(list_of_files)
			foo = Docking.Docking(receptor_path, ligand_path)
			foo.compact_maps()			

		# def test_if_get_pos_from_residue(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.pdb"

		# 	list_of_files = ["alphaVbeta3.pdb", "receptor.pdbqt", "A9e.pdbqt"]
		# 	copy_files(list_of_files)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo._get_Pos_from_residue(992, "A") |should| equal_to([62.541, 45.807999, 75.183998])

		# def test_if_remove_hets(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.wMG.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"

		# 	list_of_files = ["alphaVbeta3.wMG.pdb", "receptor.pdbqt", "A9e.pdbqt"]
		# 	copy_files(list_of_files)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.remove_heteroatoms()

		# def test_if_add_hets(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.wMG.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"

		# 	list_of_files = ["alphaVbeta3.wMG.pdb", "receptor.pdbqt", "alphaVbeta3.wMG.pdb.het"]
		# 	copy_files(list_of_files)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.add_hetatms_pdbqt(DYNDOCK_ENG_PATH + os.sep + "receptor.pdbqt")


		# def test_if_prepare_DPF(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.pdb"

		# 	list_of_files = ["alphaVbeta3.pdb", "receptor.pdbqt", "A9e.pdbqt", "grid.gpf", "receptor.gpf"]
		# 	copy_files(list_of_files)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.prepare_DPF("10")


		# def test_if_run_autodock(self):
		# 	clear_control_env()
		# 	receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.pdb"
		# 	ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "A9e.pdbqt"
		# 	ligand_path = DYNDOCK_ENG_PATH + os.sep + "A9e.pdbqt"
		# 	receptor_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.pdb"

		# 	list_of_files = ["alphaVbeta3.pdb", "receptor.pdbqt", "A9e.pdbqt", "grid.gpf", "receptor.gpf", "ligant_receptor.dpf",
		# 	 "receptor.maps.fld", "receptor.A.map", "receptor.C.map", "receptor.N.map", "receptor.NA.map", "receptor.OA.map", 
		# 	 "receptor.SA.map", "receptor.e.map", "receptor.d.map"]
		# 	copy_files(list_of_files)
		# 	foo = Docking.Docking(receptor_path, ligand_path)
		# 	foo.run_autodock()

def clear_control_env():
	if os.path.exists(DYNDOCK_ENG_PATH):
		shutil.rmtree(DYNDOCK_ENG_PATH)
	os.mkdir(DYNDOCK_ENG_PATH)

def copy_files(list_of_files, from_folder=""):
	for eachFile in list_of_files:
		shutil.copyfile(DYNDOCK_TEST_PATH + os.sep + from_folder + os.sep + eachFile, DYNDOCK_ENG_PATH + os.sep + eachFile)



if __name__ == '__main__':
    unittest.main()