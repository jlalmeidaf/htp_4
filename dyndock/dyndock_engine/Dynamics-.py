import subprocess, os, shutil
from LogException import LogException
class Dynamic(object):
	"""docstring for Dynamic"""
	def __init__(self, protein_name):
		super(Dynamic, self).__init__()
		self.stage = 0

		self.protein = protein_name
		self.ion_gro = None
		self.tpr_file = None
		self.protein_s_gro = None
		self.protein_gro = None
		self.topology_file = None
		self.workdir = os.path.dirname(self.protein)

	
	def generate_topology(self):		
		self.protein_gro = self.protein + ".gro"
		self.topology_file = self.workdir + os.sep + "topol.top"
		commands = ["pdb2gmx", "-f", self.protein, "-o", self.protein_gro, "-ignh", "-ff", "gromos53a6", "-water", "spc", "-missing" ]
		self._exec_command(commands)

	def generate_ndx_of_active_site_from_gro(self, pos, size):
		self.output_ndx = "output.ndx"
		commands = [ "g_select","-s",self.protein_gro,"-on",self.output_ndx ,"-select",'''"BoxT" group Protein and within ''' + size + ''' of resnr ''' + pos]
		self._exec_command(commands)

	def rename_ndx_of_active_site_from_gro(self):
		parameters = 'name 0 box\nq\n'
		commands = ["make_ndx","-n",self.output_ndx,"-o",self.output_ndx]
		self._exec_command(commands, parameters)

	def prepare_solvatation_box(self):
		commands = ["editconf","-f",self.protein_gro,"-o",self.protein_gro,"-c","-bt","triclinic","-d","1"]
		self._exec_command(commands)





	def write_ions_mdp(self):
		content = '''
; ions.mdp - used as input into grompp to generate ions.tpr
; Parameters describing what to do, when to stop and what to save
integrator	= steep		; Algorithm (steep = steepest descent minimization)
emtol		= 1000.0  	; Stop minimization when the maximum force < 1000.0 kJ/mol/nm
emstep      = 0.01      ; Energy step size
nsteps		= 50000	  	; Maximum number of (minimization) steps to perform

; Parameters describing how to find the neighbors of each atom and how to calculate the interactions
nstlist		    = 1		    ; Frequency to update the neighbor list and long range forces
;cutoff-scheme   = Verlet
ns_type		    = grid		; Method to determine neighbor list (simple, grid)
coulombtype	    = PME		; Treatment of long range electrostatic interactions
rcoulomb	    = 1.0		; Short-range electrostatic cut-off
rvdw		    = 1.0		; Short-range Van der Waals cut-off
pbc		        = xyz 		; Periodic Boundary Conditions (yes/no)
'''
		mdp_path = self.workdir + os.sep + "ions.mdp"
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()

	def write_em_mdp(self):
		content = '''
; ions.mdp - used as input into grompp to generate ions.tpr
; Parameters describing what to do, when to stop and what to save
integrator	= steep		; Algorithm (steep = steepest descent minimization)
emtol		= 1000.0  	; Stop minimization when the maximum force < 1000.0 kJ/mol/nm
emstep          = 0.01          ; Energy step size
nsteps		= 50000	  	; Maximum number of (minimization) wsteps to perform

; Parameters describing how to find the neighbors of each atom and how to calculate the interactions
nstlist		= 1		; Frequency to update the neighbor list and long range forces
ns_type		= grid		; Method to determine neighbor list (simple, grid)
rlist		= 1.0		; Cut-off for making neighbor list (short range forces)
coulombtype	= PME		; Treatment of long range electrostatic interactions
rcoulomb	= 1.0		; Short-range electrostatic cut-off
rvdw		= 1.0		; Short-range Van der Waals cut-off
pbc		= xyz 		; Periodic Boundary Conditions (yes/no)
'''
		mdp_path = self.workdir + os.sep + "em.mdp"
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()

	def generate_term_mdp(self, temp): #sem teste
		content = '''
title		= OPLS Lysozyme NVT equilibration 
define		= -DPOSRES	; position restrain the protein
; Run parameters
integrator	= md		; leap-frog integrator
nsteps		= 15000		; 2 * 10000 = 20 ps
dt		= 0.002		; 2 fs
; Output control
nstxout		= 1000		; save coordinates every 2 ps
nstvout		= 1000		; save velocities every 2 ps
nstenergy	= 1000		; save energies every 2 ps
nstlog		= 1000		; update log file every 2 ps
; Bond parameters
continuation	= no		; first dynamics run
constraint_algorithm = lincs	; holonomic constraints 
constraints	= all-bonds	; all bonds (even heavy atom-H bonds) constrained
lincs_iter	= 1		; accuracy of LINCS
lincs_order	= 4		; also related to accuracy
; Neighborsearching
ns_type		= grid		; search neighboring grid cells
nstlist		= 5		; 10 fs
rlist		= 1.0		; short-range neighborlist cutoff (in nm)
rcoulomb	= 1.0		; short-range electrostatic cutoff (in nm)
rvdw		= 1.0		; short-range van der Waals cutoff (in nm)
; Electrostatics
coulombtype	= PME		; Particle Mesh Ewald for long-range electrostatics
pme_order	= 4		; cubic interpolation
fourierspacing	= 0.16		; grid spacing for FFT
; Temperature coupling is on
tcoupl		= V-rescale	; modified Berendsen thermostat
tc-grps		= Protein Non-Protein	; two coupling groups - more accurate
tau_t		= 0.1	0.1	; time constant, in ps
ref_t		=''' + temp + '''	''' +temp+'''	; reference temperature, one for each group, in K
; Pressure coupling is off
pcoupl		= no 		; no pressure coupling in NVT
; Periodic boundary conditions
pbc		= xyz		; 3-D PBC
; Dispersion correction
DispCorr	= EnerPres	; account for cut-off vdW scheme
; Velocity generation
gen_vel		= yes		; assign velocities from Maxwell distribution
gen_temp	= '''+ temp+ '''		; temperature for Maxwell distribution
gen_seed	= -1		; generate a random seed
'''
		mdp_path = self.workdir + os.sep + "nvt"+temp+"k_20ps.mdp"
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()

	def generate_npt_mdp(self):
		content = '''
title		= OPLS Lysozyme NPT equilibration 
define		= -DPOSRES	; position restrain the protein
; Run parameters
integrator	= md		; leap-frog integrator
nsteps		= 25000		; 2 * 25000 = 50 ps
dt		= 0.002		; 2 fs
; Output control
nstxout		= 1000		; save coordinates every 2 ps
nstvout		= 1000		; save velocities every 2 ps
nstenergy	= 1000		; save energies every 2 ps
nstlog		= 1000		; update log file every 2 ps
; Bond parameters
continuation	= yes		; Restarting after NVT 
constraint_algorithm = lincs	; holonomic constraints 
constraints	= all-bonds	; all bonds (even heavy atom-H bonds) constrained
lincs_iter	= 1		; accuracy of LINCS
lincs_order	= 4		; also related to accuracy
; Neighborsearching
ns_type		= grid		; search neighboring grid cells
nstlist		= 5		; 10 fs
rlist		= 1.0		; short-range neighborlist cutoff (in nm)
rcoulomb	= 1.0		; short-range electrostatic cutoff (in nm)
rvdw		= 1.0		; short-range van der Waals cutoff (in nm)
; Electrostatics
coulombtype	= PME		; Particle Mesh Ewald for long-range electrostatics
pme_order	= 4		; cubic interpolation
fourierspacing	= 0.16		; grid spacing for FFT
; Temperature coupling is on
tcoupl		= V-rescale	; modified Berendsen thermostat
tc-grps		= Protein Non-Protein	; two coupling groups - more accurate
tau_t		= 0.1	0.1	; time constant, in ps
ref_t		= 300 	300	; reference temperature, one for each group, in K
; Pressure coupling is on
pcoupl		= Parrinello-Rahman	; Pressure coupling on in NPT
pcoupltype	= isotropic	; uniform scaling of box vectors
tau_p		= 2.0		; time constant, in ps
ref_p		= 1.0		; reference pressure, in bar
compressibility = 4.5e-5	; isothermal compressibility of water, bar^-1
refcoord_scaling = com
; Periodic boundary conditions
pbc		= xyz		; 3-D PBC
; Dispersion correction
DispCorr	= EnerPres	; account for cut-off vdW scheme
; Velocity generation
gen_vel		= no		; Velocity generation is off 
'''
		mdp_path = self.workdir + os.sep + "npt.mdp"
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()

	def gerenate_md_mdp(self):
		content = '''
title		= OPLS Lysozyme MD 
; Run parameters
integrator	= md		; leap-frog integrator
nsteps		= 1000000	; 2 * 150000 = 300 ps, 1 ns
dt		= 0.002		; 2 fs
; Output control
nstxout		= 1000		; save coordinates every 2 ps
nstvout		= 1000		; save velocities every 2 ps
nstxtcout	= 1000		; xtc compressed trajectory output every 2 ps
nstenergy	= 1000		; save energies every 2 ps
nstlog		= 1000		; update log file every 2 ps
; Bond parameters
continuation	= no		; Restarting after NPT 
constraint_algorithm = lincs	; holonomic constraints 
constraints	= all-bonds	; all bonds (even heavy atom-H bonds) constrained
lincs_iter	= 1		; accuracy of LINCS
lincs_order	= 4		; also related to accuracy
; Neighborsearching
ns_type		= grid		; search neighboring grid cells
nstlist		= 5		; 10 fs
rlist		= 1.0		; short-range neighborlist cutoff (in nm)
rcoulomb	= 1.0		; short-range electrostatic cutoff (in nm)
rvdw		= 1.0		; short-range van der Waals cutoff (in nm)
; Electrostatics
coulombtype	= PME		; Particle Mesh Ewald for long-range electrostatics
pme_order	= 4		; cubic interpolation
fourierspacing	= 0.16		; grid spacing for FFT
; Temperature coupling is on
tcoupl		= V-rescale	; modified Berendsen thermostat
tc-grps		= Protein Non-Protein	; two coupling groups - more accurate
tau_t		= 0.1	0.1	; time constant, in ps
ref_t		= 300 	300	; reference temperature, one for each group, in K
; Pressure coupling is on
pcoupl		= Parrinello-Rahman	; Pressure coupling on in NPT
pcoupltype	= isotropic	; uniform scaling of box vectors
tau_p		= 2.0		; time constant, in ps
ref_p		= 1.0		; reference pressure, in bar
compressibility = 4.5e-5	; isothermal compressibility of water, bar^-1
; Periodic boundary conditions
pbc		= xyz		; 3-D PBC
; Dispersion correction
DispCorr	= EnerPres	; account for cut-off vdW scheme
; Velocity generation
gen_vel		= no		; Velocity generation is off  
'''
		mdp_path = self.workdir + os.sep + "md.mdp"
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()
	def generate_em_after_md_mdp(self, path):
		content = '''
; ions.mdp - used as input into grompp to generate ions.tpr
; Parameters describing what to do, when to stop and what to save
integrator	= steep		; Algorithm (steep = steepest descent minimization)
emtol		= 2000.0  	; Stop minimization when the maximum force < 1000.0 kJ/mol/nm
emstep          = 0.01          ; Energy step size
nsteps		= 50000	  	; Maximum number of (minimization) steps to perform

; Parameters describing how to find the neighbors of each atom and how to calculate the interactions
nstlist		= 1		; Frequency to update the neighbor list and long range forces
ns_type		= grid		; Method to determine neighbor list (simple, grid)
rlist		= 1.0		; Cut-off for making neighbor list (short range forces)
coulombtype	= PME		; Treatment of long range electrostatic interactions
rcoulomb	= 1.0		; Short-range electrostatic cut-off
rvdw		= 1.0		; Short-range Van der Waals cut-off
pbc		= xyz 		; Periodic Boundary Conditions (yes/no)
'''
		mdp_path = path + os.sep + "em_after_md.mdp"
		mdp_file = file(mdp_path,'w')
		mdp_file.write(content)
		mdp_file.close()
#------------------
	def prepare_for_generate_ions(self):
		self.write_ions_mdp()
		self.tpr_file = self.protein + "_ion.tpr"
		self.execute_grompp("ions.mdp", self.protein_s_gro, self.tpr_file)

	def generate_ions(self):
		self.ion_gro = self.protein + "_ion.gro"
		commands = ["genion","-s",self.tpr_file,"-p",self.topology_file,"-o",self.ion_gro,"-neutral","-conc","0.01"]
		self._exec_command(commands,"SOL")

	def prepare_termalization_at(self, temp): #consertar para pegar do nvt gro file
		self.generate_term_mdp(temp)
		self.nvt_file = self.protein + "_nvt.tpr"
		self.execute_grompp("nvt"+temp+"k_20ps.mdp", self.em_gro_file, self.nvt_file)

	def prepare_continuation_of_termalization_at(self, temp):
		self.generate_term_mdp(temp)
		self.nvt_file = self.protein + "_nvt.tpr"
		self.execute_grompp("nvt"+temp+"k_20ps.mdp", self.nvt_gro_file, self.nvt_file)

	def prepare_for_em(self):
		self.write_em_mdp()
		self.em_tpr = self.protein + "_em.tpr"
		self.execute_grompp("em.mdp", self.ion_gro,self.em_tpr)

	def prepare_for_npt(self):
		self.generate_npt_mdp()
		self.npt_tpr = self.protein + "_npt.tpr"
		self.execute_grompp("npt.mdp", self.nvt_gro_file,self.npt_tpr)

	def prepare_for_md(self):
		self.gerenate_md_mdp()
		self.md_tpr = self.protein + "_md.tpr"
		self.execute_grompp("md.mdp", self.npt_gro_file, self.md_tpr)

#------------Intesive tasks --------------
	def generate_box(self):
		self.protein_s_gro = self.protein_gro[:-4] + "_s.gro"
		commands = ["genbox","-cp",self.protein_gro,"-cs","spc216.gro","-o", self.protein_s_gro ,"-p",self.topology_file]
		self._exec_command(commands)

	def run_termalization(self):
		nvt_file = os.path.basename(self.protein) + "_nvt"
		self.nvt_gro_file = self.protein + "_nvt.gro"
		self.execute_mdrun(nvt_file)


	def run_em(self):
		em_file = os.path.basename(self.protein) + "_em"
		self.em_gro_file = self.protein + "_em.gro"
		self.execute_mdrun(em_file)

	def run_npt(self):
		npt_file = os.path.basename(self.protein) + "_npt"
		self.npt_gro_file = self.protein + "_npt.gro"
		self.execute_mdrun(npt_file)

	def run_md(self):
		md_file = os.path.basename(self.protein) + "_md"
		self.md_gro_file = self.protein + "_md.gro"
		self.execute_mdrun(md_file)
#------------------



	def select_atoms_of_active_site(self, list_of_residues): #resnr
		parameters = "resnr " + " ".join(list_of_residues)
		self.output_ndx = "output.ndx"
		md_file = os.path.basename(self.protein) + "_md"
		commands = ["g_select","-f",self.md_gro_file,"-s",md_file,"-on",self.output_ndx,"-oi"]
		self._exec_command(commands, parameters)

	def generate_ndx_of_active_site(self):
		commands = ["make_ndx","-f",self.md_gro_file,"-n",self.output_ndx,"-o",self.output_ndx]
		self._exec_command(commands, "name 0 box\nq\n")

	def prepare_ndx_of_active_site(self,pos):
		xtc_file = self.protein + "_md.xtc"
		tpr_file = self.protein + "_md.tpr"
		self.output_ndx = "output.ndx"
		commands = ["g_select","-f",xtc_file,"-s",tpr_file,"-on",self.output_ndx, "-b", "0", "-e", "50","-select",'''"BoxT" group Protein and within 2 of resnr ''' + pos, ] #check BoxT
		self._exec_command(commands)


	def prepare_ndx_of_active_site2(self):
		parameters = "0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25\nq\n"
		commands = ["make_ndx","-f",self.md_gro_file,"-n",self.output_ndx,"-o",self.output_ndx]
		self._exec_command(commands, parameters)

	def prepare_ndx_of_active_site3(self):
		parameters = 'name 26 box\nq\n'
		commands = ["make_ndx","-f",self.md_gro_file,"-n",self.output_ndx,"-o",self.output_ndx]
		self._exec_command(commands, parameters)

	def prepare_ndx_of_active_site4(self):
		parameters = 'del 0-25\nq\n'
		commands = ["make_ndx","-f",self.md_gro_file,"-n",self.output_ndx,"-o",self.output_ndx]
		self._exec_command(commands, parameters)

	def clustering(self):
		xtc_file = self.protein + "_md.xtc"
		tpr_file = self.protein + "_md.tpr"
		self.cluster_pdb_file = self.protein + "_cluster.pdb"
		commands = ["g_cluster","-s",tpr_file,"-f",xtc_file,"-cutoff","0.1","-g","-o","-method","gromos","-cl",self.cluster_pdb_file,"-n",self.output_ndx,"-dist","-tr","-ntr", "-b", "150"]
		self._exec_command(commands)

	def convert_xpm_to_ps(self):
		commands = ["xpm2ps","-f","rmsd-clust.xpm","-o","cluster100ns.eps","-rainbow","blue"]
		self._exec_command(commands)

	def get_pdbs_from_cluster(self):
		file_pdb = file(self.cluster_pdb_file,"r")
		file_in_string = file_pdb.read()
		file_in_list = file_in_string.split("TITLE     frame t= ")
		count = 0
		
		mylist = []
		for eachFile in file_in_list[1:]:
				count = count + 1
				if count < 4:
					pdb_in_list = eachFile.split("\n")
					mylist.append(pdb_in_list[0])
		
		file_pdb.close()
		return mylist

	def fix_no_jump(self):
		tpr_file = self.protein + "_md.tpr"
		commands = ["trjconv","-s",tpr_file,"-f",self.protein + "_md.trr","-pbc","nojump","-o",self.protein + "_md.xtc"]
		self._exec_command(commands, "0\n")

	def concatenate_em_termalization_npt_and_md(self):
		tpr_file = self.protein + "_em.tpr"
		commands = ["trjcat","-f",self.protein + "_em.trr",self.protein + "_nvt.trr",self.protein + "_npt.trr", self.protein + "_md.trr","-settime"]
		# commands = ["trjcat","-s",tpr_file,"-f",self.protein + "_em.gro",self.protein + "_nvt.gro",self.protein + "_npt.gro","-fit", "progressive","-o",self.protein + "_npt.trr"]

		self._exec_command(commands, "c\nc\nc\nc\n")

	def fix_no_rotate(self):
		tpr_file = self.protein + "_md.tpr"
		commands = ["trjconv","-s",tpr_file,"-f","trajout.xtc","-fit", "progressive","-o",self.protein + "_md.xtc"]
		self._exec_command(commands, "0\n0\n")

	def extract_pdb(self,time_folder, protein_name, time):
		commands = ["trjconv","-s",time_folder + os.sep + protein_name + "_md.tpr","-f",time_folder + os.sep + protein_name + "_md.trr",
		"-o",time_folder + os.sep + protein_name + "_md.pdb","-b",time,"-e",time]
		self._exec_command(commands, "0\n0\n")

	def convert_pdb_to_gro(self, time_folder, protein_name):
		commands = ["pdb2gmx","-f",time_folder + os.sep + protein_name + "_md.pdb","-o",time_folder + os.sep + protein_name + "_em.gro","-p",
		time_folder + os.sep + protein_name + "_em.top","-ignh","-ff","gromos53a6","-water","spc", "-missing"]
		self._exec_command(commands)

	def make_ndx(self, folder, protein_name, select_data):
		commands = ["make_ndx","-f", folder + os.sep + protein_name + "_em.gro","-o", folder + os.sep + "Integrin.ndx"]
		self._exec_command(commands, select_data)

	def make_ndx2(self, folder, protein_name, select_data):
		commands = ["make_ndx","-f", folder + os.sep + protein_name + "_em.gro","-n", folder + os.sep + "Integrin.ndx", "-o", folder + os.sep + "Integrin.ndx"]
		self._exec_command(commands, select_data)

	def nao_sei(self, time):
		newfolder = time + "_folder"
		path = os.path.dirname(self.protein) + os.sep + newfolder
		os.mkdir(path)
		self.generate_em_after_md_mdp(path)
		shutil.copyfile(self.protein + "_md.tpr", path + os.sep + os.path.basename(self.protein) + "_md.tpr")
		shutil.copyfile(self.protein + "_md.trr", path + os.sep + os.path.basename(self.protein) +  "_md.trr")

		int_time = time.split(".")[0]

		self.extract_pdb(path, os.path.basename(self.protein), int_time)
		self.convert_pdb_to_gro(path, os.path.basename(self.protein))
		self.make_ndx(path, os.path.basename(self.protein), "\nq\n")
		if 'Other' in open(path + os.sep + 'Integrin.ndx').read():
			self.make_ndx(path, os.path.basename(self.protein), '"Protein" | "Other"\nq\n')
			self.make_ndx2(path, os.path.basename(self.protein), 'name 24 selecionado\nq\n')
		else:
			self.make_ndx2(path, os.path.basename(self.protein), 'name 1 selecionado\nq\n')
		self.execute_grompp_wt_mdp(path + os.sep + "em_after_md.mdp", path + os.sep + os.path.basename(self.protein) + "_em.gro", path + os.sep + os.path.basename(self.protein) + "_em.tpr" ,path + os.sep + "Integrin.ndx", path + os.sep + os.path.basename(self.protein) + "_em.top")
		self.execute_mdrun(path + os.sep + os.path.basename(self.protein) + "_em")
		fstep = self.get_fstep(path + os.sep + os.path.basename(self.protein) + "_em.log")
		commands = ["trjconv","-s",path + os.sep + os.path.basename(self.protein) + "_em.tpr","-n",path + os.sep + "Integrin.ndx","-f",path + os.sep + os.path.basename(self.protein) + "_em.trr","-o",path + os.sep +os.path.basename(self.protein) + "_em.pdb","-b",fstep,"-e",fstep]
		self._exec_command(commands, "selecionado\nq\n")



	def get_fstep(self,log_file):
		read_log_file = file(log_file, "r")
		text_ = read_log_file.readlines()
		for eachLine in text_:
			if eachLine.startswith("Steepest Descents converged to Fmax"):
				return(eachLine[46:49])

#---- Codigo repetido varias vezes -----

	def execute_grompp(self, mdp, gro_file, output_file):
		commands = ["grompp","-f",mdp,"-c",gro_file,"-p",self.topology_file,"-o",output_file]
		self._exec_command(commands)

	def execute_grompp_wt_mdp(self, mdp, gro_file, output_file, ndx_file, topology_file):
		commands = ["grompp","-f",mdp,"-c",gro_file, "-n", ndx_file, "-p",topology_file,"-o",output_file]
		self._exec_command(commands)

	def execute_mdrun(self, input_file):
		commands = ["mdrun","-nt","10","-v","-deffnm",input_file]
		self._exec_command(commands)

	def _exec_command(self, command_line, stdin_data=""):
		protein_path = os.path.dirname(self.protein)
		file_commands = file(protein_path + os.sep + "commands.txt","a")
		file_commands.write(" ".join(command_line) + "\n")
		file_commands.close()
		realpath = os.getcwd()
		os.chdir(protein_path)
		output_data = file(protein_path + os.sep + "last_output.txt", "w")
		process = subprocess.Popen(command_line, stdout=output_data, stderr=output_data, stdin=subprocess.PIPE, universal_newlines=True)
		process.stdin.write(stdin_data)
		process.stdin.close()		
		output_data.close()
		os.chdir(realpath)
		if process.wait() != 0:
			raise LogException(" ".join(command_line),protein_path + os.sep + "last_output.txt")
