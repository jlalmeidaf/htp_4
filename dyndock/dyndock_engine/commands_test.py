import unittest
from should_dsl import should, should_not
import comands 
import os
import shutil

DYNDOCK_ENG_PATH = os.path.dirname(os.path.realpath(__file__)) + "/control_env"
DYNDOCK_TEST_PATH = os.path.dirname(os.path.realpath(__file__)) + "/test_content"

class ComandsExamples(unittest.TestCase):
	def test_foo_command(self):
		comands.foo_command() |should|  equal_to("foobar")

	def test_if_create_control_folder(self):
		clear_control_env()
		os.path.exists(DYNDOCK_ENG_PATH) |should| equal_to(True)


	def test_if_prepare_control_system_returns_control_path(self):
		clear_control_env()
		protein_test_path = DYNDOCK_TEST_PATH + "/foo.pdb"
		protein_path = DYNDOCK_ENG_PATH + "/foo.pdb"
		shutil.copyfile(protein_test_path, protein_path)

		comands.prepare_control(protein_path) |should| equal_to(DYNDOCK_ENG_PATH + os.sep + "Control_folder")

def clear_control_env():
	if os.path.exists(DYNDOCK_ENG_PATH):
		shutil.rmtree(DYNDOCK_ENG_PATH)
	os.mkdir(DYNDOCK_ENG_PATH)


if __name__ == '__main__':
    unittest.main()