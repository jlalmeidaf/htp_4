import unittest
from should_dsl import should, should_not
import shutil,os
import Docking


DYNDOCK_ENG_PATH = os.path.dirname(os.path.realpath(__file__)) + "/control_env"
DYNDOCK_TEST_PATH = os.path.dirname(os.path.realpath(__file__)) + "/test_content/docking"
os.environ['MGLTOOLS'] = "/home/jorgehf/MGLTools-1.5.6"
os.environ['AUTODOCKFOLDER'] = "/home/jorgehf/MGLTools-1.5.6"


class CommandsDockingExamples(unittest.TestCase):
	def test_if_prepare_ligand(self):
		clear_control_env()
		receptor_test_path = DYNDOCK_TEST_PATH + os.sep + "BatroxModelMonomerSemAcidoGraxoESulfato.pdb"
		ligand_test_path = DYNDOCK_TEST_PATH + os.sep + "isolated2.pdbqt"
		ligand_path = DYNDOCK_ENG_PATH + os.sep + "isolated2.pdbqt"
		receptor_path = DYNDOCK_ENG_PATH + os.sep +"BatroxModelMonomerSemAcidoGraxoESulfato.pdb"
		size = "1"
		residue_number = 92
		chain = "A"
		type_docking = "10"

		shutil.copyfile(receptor_test_path, receptor_path)
		shutil.copyfile(ligand_test_path, ligand_path)
		foo = Docking.Docking(receptor_path, ligand_path)
		foo.prepare_ligand()

		foo.remove_heteroatoms()
		foo.prepare_ligand()
		
		foo.prepare_receptor()
		foo.add_hetatms_pdbqt(DYNDOCK_ENG_PATH + os.sep + "receptor.pdbqt")
		# print residue_number
		pos = foo._get_Pos_from_residue(residue_number, chain)
		foo.prepare_GPF(size, ",".join(pos))
		foo.prepare_GPF2(size, ",".join(pos))
		foo.run_autogrid()
		foo.prepare_DPF(type_docking)
		foo.run_autodock()

		

def clear_control_env():
	if os.path.exists(DYNDOCK_ENG_PATH):
		shutil.rmtree(DYNDOCK_ENG_PATH)
	os.mkdir(DYNDOCK_ENG_PATH)

def copy_files(list_of_files, from_folder=""):
	for eachFile in list_of_files:
		shutil.copyfile(DYNDOCK_TEST_PATH + os.sep + from_folder + os.sep + eachFile, DYNDOCK_ENG_PATH + os.sep + eachFile)



if __name__ == '__main__':
    unittest.main()
