import unittest
from should_dsl import should, should_not
import shutil,os
import Dynamics


DYNDOCK_ENG_PATH = os.path.dirname(os.path.realpath(__file__)) + "/control_env"
DYNDOCK_TEST_PATH = os.path.dirname(os.path.realpath(__file__)) + "/test_content"

class ComandsDynamicsExamples(unittest.TestCase):


	def test_if_generate_topology(self):
		clear_control_env()
		protein_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.wMG.pdb"
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		shutil.copyfile(protein_test_path, protein_path)
		foo = Dynamics.Dynamic(protein_path)
		foo.generate_topology()
		os.path.exists(DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb.gro") |should| equal_to(True)
		# show if exists topol.top


	def test_if_generate_box(self):
		
		clear_control_env()
		topology_path = DYNDOCK_TEST_PATH + os.sep + "topol.top"
		protein_test_path = DYNDOCK_TEST_PATH + os.sep + "alphaVbeta3.wMG.pdb"
		gro_test_path = protein_test_path + ".gro"
		protein_gro_path = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb.gro"
		shutil.copyfile(gro_test_path, protein_gro_path)
		shutil.copyfile(topology_path, protein_gro_path)

		foo = Dynamics.Dynamic(protein_test_path)
		foo.protein_gro = protein_gro_path
		foo.topology_file = topology_path
		foo.generate_box()

		os.path.exists(DYNDOCK_ENG_PATH + os.sep + "#alphaVbeta3.wMG.pdb.gro.1#") |should| equal_to(True)



	def test_if_create_mdp_file(self):
		clear_control_env()
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.write_ions_mdp()
		os.path.exists(DYNDOCK_ENG_PATH + os.sep + "ions.mdp") |should| equal_to(True)


	def test_if_genertate_ions(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_ion.tpr", "topol.top",
		"topol_Protein.itp", "topol_Protein2.itp", "topol_Other3.itp" ]
		copy_files(list_of_files)
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.tpr_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_ion.tpr"
		foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"
		foo.generate_ions()

	def test_if_prepare_for_em(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_ion.gro", "topol.top", 
		"topol_Protein.itp", "topol_Protein2.itp", "topol_Other3.itp" ]
		copy_files(list_of_files, "ions")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.ion_gro = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_ion.gro"	
		foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"
		foo.prepare_for_em()

	def test_if_run_em(self):
		clear_control_env()
		list_of_files = ["mdout.mdp", "topol.top", "alphaVbeta3.wMG.pdb_ion.gro", 
		"alphaVbeta3.wMG.pdb_em.tpr", "topol_Protein.itp", "topol_Protein2.itp" ]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.ion_gro = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_ion.gro"	
		foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"		
		foo.run_em()

	def test_if_prepare_termalization(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_em.gro", "topol.top", 
		"topol_Protein.itp", "topol_Protein2.itp", "topol_Other3.itp", "posre_Protein.itp",
		 "posre_Protein2.itp", "posre_Other3.itp"]
		copy_files(list_of_files,"prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		# foo.ion_gro = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_ion.gro"
		foo.em_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_em.gro"
		foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"
		foo.prepare_termalization_at("100")


	def test_if_prepare_npt(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_nvt.gro",  "topol.top", 
		"topol_Protein.itp", "topol_Protein2.itp", "topol_Other3.itp", "posre_Protein.itp",
		 "posre_Protein2.itp", "posre_Other3.itp"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.nvt_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_nvt.gro"
		foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"
		foo.prepare_for_npt()

	def test_if_run_npt(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_npt.tpr", "alphaVbeta3.wMG.pdb_nvt.gro"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.em_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_nvt.gro"
		foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"		
		foo.run_npt()


	def test_if_prepare_md(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_npt.gro",  "topol.top", 
		"topol_Protein.itp", "topol_Protein2.itp", "topol_Other3.itp", "posre_Protein.itp",
		 "posre_Protein2.itp", "posre_Other3.itp"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.npt_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_npt.gro"
		foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"
		foo.prepare_for_md()

	# def test_if_run_md(self):
	# 	clear_control_env()
	# 	list_of_files = ["alphaVbeta3.wMG.pdb_md.tpr", "alphaVbeta3.wMG.pdb_npt.gro"]
	# 	copy_files(list_of_files, "prepare_em")
	# 	protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
	# 	foo = Dynamics.Dynamic(protein_path)
	# 	foo.em_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_npt.gro"
	# 	foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"		
	# 	foo.run_md()

	def test_if_nao_sei(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_md.gro"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.md_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_md.gro"
		foo.select_atoms_of_active_site(["1","2","3","4", "5", "6", "7", "9", "10"])

	def test_if_nao_se_bonita(self):
		clear_control_env()
		list_of_files = ["output.ndx", "alphaVbeta3.wMG.pdb_md.gro"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.md_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_md.gro"
		foo.output_ndx = DYNDOCK_ENG_PATH + os.sep + "output.ndx"
		foo.generate_ndx_of_active_site()

	def test_if_prepare_ndx_of_active_site(self):
		clear_control_env()
		list_of_files = ["output.ndx", "alphaVbeta3.wMG.pdb_md.xtc", "alphaVbeta3.wMG.pdb_md.tpr"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.xtc_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_md.gro"
		foo.tpr_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_md.tpr"
		foo.output_ndx = DYNDOCK_ENG_PATH + os.sep + "output.ndx"
		foo.prepare_ndx_of_active_site("992")

	def test_if_prepare_ndx_of_active_site2(self):
		clear_control_env()
		list_of_files = ["output2.ndx", "alphaVbeta3.wMG.pdb_md.gro"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.md_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_md.gro"
		foo.output_ndx = DYNDOCK_ENG_PATH + os.sep + "output2.ndx"
		foo.prepare_ndx_of_active_site2()

	def test_if_prepare_ndx_of_active_site3(self):
		clear_control_env()
		list_of_files = ["output3.ndx", "alphaVbeta3.wMG.pdb_md.gro"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.md_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_md.gro"
		foo.output_ndx = DYNDOCK_ENG_PATH + os.sep + "output3.ndx"
		foo.prepare_ndx_of_active_site3()

	def test_if_prepare_ndx_of_active_site4(self):
		clear_control_env()
		list_of_files = ["output4.ndx", "alphaVbeta3.wMG.pdb_md.gro"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.md_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_md.gro"
		foo.output_ndx = DYNDOCK_ENG_PATH + os.sep + "output4.ndx"
		foo.prepare_ndx_of_active_site4()

	def test_if_clustering(self):
		clear_control_env()
		list_of_files = ["output5.ndx", "alphaVbeta3.wMG.pdb_md.xtc", "alphaVbeta3.wMG.pdb_md.tpr"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		# foo.md_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_md.gro"
		foo.output_ndx = DYNDOCK_ENG_PATH + os.sep + "output5.ndx"
		foo.clustering(0.5)

	def test_if_convert_to_xpm(self):
		clear_control_env()
		list_of_files = ["rmsd-clust.xpm"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.convert_xpm_to_ps()

	def test_if_get_pdbscluster_pdb_file_from_cluster(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_cluster.pdb", "alphaVbeta3.wMG.pdb_md.tpr", "alphaVbeta3.wMG.pdb_md.trr"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.cluster_pdb_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_cluster.pdb"
		foo.nao_sei("20.000")
		# times = foo.get_pdbs_from_cluster()

	def test_if_get_pdbscluster_pdb_file_from_cluster(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_cluster.pdb"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		foo.cluster_pdb_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_cluster.pdb"
		times = foo.get_pdbs_from_cluster()
		times |should| equal_to(["440.000","100.000","940.000"])

	def test_if_fix_no_jump(self):
		clear_control_env()
		list_of_files = ["alphaVbeta3.wMG.pdb_md.tpr",  "alphaVbeta3.wMG.pdb_md.trr"]
		copy_files(list_of_files, "prepare_em")
		protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
		foo = Dynamics.Dynamic(protein_path)
		# foo.nvt_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_nvt.gro"
		foo.fix_no_jump()

	# def test_if_fix_no_rotate(self):
	# 	clear_control_env()
	# 	list_of_files = ["hACEn.pdb_md.tpr",  "trajout.xtc"]
	# 	copy_files(list_of_files, "xtc_no_rotate")
	# 	protein_path = DYNDOCK_ENG_PATH + os.sep +"hACEn.pdb"
	# 	foo = Dynamics.Dynamic(protein_path)
	# 	# foo.nvt_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_nvt.gro"
	# 	foo.fix_no_rotate2()


	# def test_if_fix_no_rotate1(self):
	# 	clear_control_env()
	# 	list_of_files = [ "hACEn.pdb_nvt.trr", "hACEn.pdb_em.trr",  "hACEn.pdb_npt.trr", "hACEn.pdb_md.trr"]
	# 	copy_files(list_of_files, "xtc_no_rotate1")
	# 	protein_path = DYNDOCK_ENG_PATH + os.sep +"hACEn.pdb"
	# 	foo = Dynamics.Dynamic(protein_path)
	# 	# foo.nvt_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_nvt.gro"
	# 	foo.fix_no_rotate1()


	# def test_if_generate_LogException(self):
	# 	clear_control_env()
	# 	list_of_files = ["alphaVbeta3.wMG.pdb_nvt.gro",  "topol.top", 
	# 	"topol_Protein.itp", "topol_Protein2.itp", "posre_Protein.itp",
	# 	 "posre_Protein2.itp", "posre_Other3.itp"]
	# 	copy_files(list_of_files, "prepare_em")
	# 	protein_path = DYNDOCK_ENG_PATH + os.sep +"alphaVbeta3.wMG.pdb"
	# 	foo = Dynamics.Dynamic(protein_path)
	# 	foo.nvt_gro_file = DYNDOCK_ENG_PATH + os.sep + "alphaVbeta3.wMG.pdb_nvt.gro"
	# 	foo.topology_file = DYNDOCK_ENG_PATH + os.sep + "topol.top"
	# 	foo.prepare_for_npt()

def clear_control_env():
	if os.path.exists(DYNDOCK_ENG_PATH):
		shutil.rmtree(DYNDOCK_ENG_PATH)
	os.mkdir(DYNDOCK_ENG_PATH)

def copy_files(list_of_files, from_folder=""):
	for eachFile in list_of_files:
		shutil.copyfile(DYNDOCK_TEST_PATH + os.sep + from_folder + os.sep + eachFile, DYNDOCK_ENG_PATH + os.sep + eachFile)



if __name__ == '__main__':
    unittest.main()