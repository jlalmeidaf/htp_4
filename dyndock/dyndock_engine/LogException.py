class LogException(Exception):
	"""docstring for LogException"""
	def __init__(self,command_line,path):
		
		self.path = path
		log_file = file(self.path, "r")
		text = "Error caused by: "+ command_line +"\n\nOutput:\n\n" + "".join(log_file.readlines())
		super(LogException, self).__init__(text)
		