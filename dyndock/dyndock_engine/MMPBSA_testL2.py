import Mmpbsa

def main():
	#complex_protein_ligand = "/home/joao/UENF/temp/dyndock_engine/test_mmbpsa/MproSaquinavir_worldxx510_Control_11.pdb"
	complex_protein_ligand = "/home/joao/Temp/tmpjp8waH_test_MMPBSA/receptor.pdb"
	mmbpsa_analisys = Mmpbsa.MMBPSA(complex_protein_ligand)
#	mmbpsa_analisys.sep_ligand_from_receptor()
#	mmbpsa_analisys.add_hidrogen_in_ligand()
#	mmbpsa_analisys.get_ligand_topology()
#	mmbpsa_analisys.get_receptor_topology()
#	mmbpsa_analisys.generate_complex_gro()
#	mmbpsa_analisys.include_lig_topology_in_topol_top()
#	mmbpsa_analisys.prepare_solvatation_box()
#	mmbpsa_analisys.generate_box()
#	mmbpsa_analisys.prepare_for_generate_ions()
#	mmbpsa_analisys.generate_ions()
#	mmbpsa_analisys.prepare_for_em()
#	mmbpsa_analisys.run_em()
#	mmbpsa_analisys.make_ndx_for_restriction()
#	mmbpsa_analisys.run_restrictions_on_ligand()
#	mmbpsa_analisys.add_restriction_to_topol()
#	mmbpsa_analisys.make_ndx_protein_lig()
#	mmbpsa_analisys.prepare_for_nvt()
#	mmbpsa_analisys.run_nvt()
#	mmbpsa_analisys.prepare_for_npt()
#	mmbpsa_analisys.run_npt()
#	mmbpsa_analisys.prepare_for_md()
#	mmbpsa_analisys.run_md()
	mmbpsa_analisys.run_mmbpsa_analisys()
	mmbpsa_analisys.generate_graphs()




if __name__ == '__main__':
	main()
