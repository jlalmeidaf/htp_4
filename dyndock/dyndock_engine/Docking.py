import subprocess, os, shutil, time, sys
import psutil
import threading
from LogException import LogException
import math
from Bio.PDB import PDBParser
from zipfile import ZipFile, ZIP_DEFLATED
from dyndock import settings


os.environ['MGLTOOLS'] = settings.MGLTOOLS
os.environ['AUTODOCKFOLDER'] = settings.AUTODOCKFOLDER
#os.environ['MGLTOOLS'] = "/home/joao/MGLTools-1.5.6/"
#os.environ['AUTODOCKFOLDER'] = "/home/joao/MGLTools-1.5.6/"

sys.path.append(os.environ['MGLTOOLS'] + '/MGLToolsPckgs/')
from AutoDockTools.DlgParser import DlgParser

class Docking():
	def __init__(self, receptor_path, ligand_path):
		self.receptor_path = receptor_path
		self.ligand_path = ligand_path
		self.workdir = os.path.dirname(self.receptor_path)
		# print "------>" + self.workdir
		self.MGTOOLSPATH = os.environ["MGLTOOLS"]
		self.PYTHONSH = self.MGTOOLSPATH + "/bin/pythonsh"
		self.UTILITIESFOLDER = self.MGTOOLSPATH + "/MGLToolsPckgs/AutoDockTools/Utilities24"
		self.AUTODOCKFOLDER = os.environ['AUTODOCKFOLDER']


	def prepare_ligand(self):
		commands = ["prepare_ligand4.py","-l",self.ligand_path,"-o", self.workdir + os.sep + "ligant.pdbqt"]
		self._run_ADT_command(commands)

	def remove_heteroatoms(self): #escrever teste para isso
		
		input_pdb = self.receptor_path
		output_pdb = self.receptor_path + ".cas"
		output_het_pdb = self.receptor_path + ".het"
		input_file = file(input_pdb, "r")
		file_in_list = input_file.readlines()
		input_file.close()

		output_file = file(output_pdb, 'w')
		list_of_lines_with_het = []
		for eachLine in file_in_list:
			if (eachLine.startswith("ATOM") or eachLine.startswith("HETATM")) and not self.had_a_residue(eachLine):
				# if self.get_resName(eachLine) != "CL" and self.get_resName(eachLine) != "NA":
					list_of_lines_with_het.append(self.line_in_pdbqt_format(eachLine))
			else:
				output_file.write(eachLine)
		output_file.close()
		
		if os.path.exists(input_pdb):
			os.remove(input_pdb)
		os.rename(output_pdb, input_pdb)

		if None in list_of_lines_with_het:
			list_of_lines_with_het.remove(None)

		if len(list_of_lines_with_het)>0:
			output_het_file = file(output_het_pdb,'w')
			# print list_of_lines_with_het
			output_het_file.writelines("".join(list_of_lines_with_het))
			output_het_file.close()


	def line_in_pdbqt_format(self,line):
		if ('Mg' in line) or ('MG' in line):
			return line[0:66] + '{0:{fill}{align}10}'.format('1.200', fill=' ',align='>') + '{0:{fill}{align}3}'.format('Mg', fill=' ',align='>') + "\n"
		if ('Zn' in line) or ('ZN' in line):
			return line[0:66] + '{0:{fill}{align}10}'.format('1.2', fill=' ',align='>') + '{0:{fill}{align}3}'.format('Zn', fill=' ',align='>') + "\n"
		if ('Ca' in line) or ('CA' in line):
			return line[0:66] + '{0:{fill}{align}10}'.format('0.8', fill=' ',align='>') + '{0:{fill}{align}3}'.format('Ca', fill=' ',align='>') + "\n"
		if ('Cl' in line) or ('CL' in line):
			return line[0:66] + '{0:{fill}{align}10}'.format('-0.8', fill=' ',align='>') + '{0:{fill}{align}3}'.format('Cl', fill=' ',align='>') + "\n"

	def had_a_residue(self,line):
				resList = ['ALA', 'ARG','ASN','ASP','CYS','GLN','GLU','GLY','HIS','ILE','LEU','LYS','MET','PHE','PRO',
				'SER','THR','TRP','TYR','VAL','ASX','GLX']
				if self.get_resName(line) in resList:
					return True
				else:
					return False

	def get_resName(self,line):
				return line[17:20].strip()


	def prepare_receptor(self):
		commands = ["prepare_receptor4.py","-r",self.receptor_path,"-A","hydrogens","-o", self.workdir + os.sep + "receptor.pdbqt"]

		self._run_ADT_command(commands)

	def add_hetatms_pdbqt(self, pdbqt_path):
		if os.path.exists(self.receptor_path + ".het"):
			pdbqt_file = file(pdbqt_path,'a')
			hetatm_pdbqt_file = file(self.receptor_path + ".het",'r')
			hetatm_lines = hetatm_pdbqt_file.readlines()
			for eachLine in hetatm_lines:
				pdbqt_file.write(eachLine)
			pdbqt_file.close()
			hetatm_pdbqt_file.close()
			os.remove(self.receptor_path + ".het")

	def _get_Pos_from_residue(self, residue, chain):
			input_pdb = self.receptor_path
			# chain = sys.argv[3]
			if chain == "":
				chain = " "
			residue_number = residue
			structure = PDBParser(QUIET=True).get_structure(None, input_pdb)
			try:
				atom = structure[0][chain][residue_number]['CA']
			except KeyError:
				# raise e
				listOfChains = structure.get_list()[0].get_list()
				for eachChain in listOfChains:
					try:
						letterOfChain = eachChain.get_id()
						atom = structure[0][letterOfChain][residue_number]['CA']
						break
					except KeyError:
						pass

	
			residueX = str(atom.coord[0])
			residueY = str(atom.coord[1])
			residueZ = str(atom.coord[2])
			return [residueX, residueY, residueZ]


	def prepare_GPF(self, size, pos):
		my_size = float(size)*60
		my_size = int(math.ceil(my_size))
		commands = ["prepare_gpf4.py","-l",self.ligand_path,"-r", self.workdir + os.sep + "receptor.pdbqt","-o", self.workdir + os.sep + "grid.gpf","-p","npts="+ str(my_size) + ","+ str(my_size) + ","+ str(my_size) ,"-p","gridcenter=" + pos]
		self._run_ADT_command(commands)

	def prepare_GPF2(self, size, pos):
		my_size = float(size)*60
		my_size = int(math.ceil(my_size))
		commands = ["prepare_gpf4.py","-l",self.ligand_path,"-r", self.workdir + os.sep + "receptor.pdbqt","-d",".","-i", self.workdir + os.sep + "grid.gpf","-p","npts="+ str(my_size) + ","+ str(my_size) + ","+ str(my_size) ,"-p","gridcenter=" + pos]
		self._run_ADT_command(commands)

	def run_autogrid(self):
		commands = [ self.AUTODOCKFOLDER + os.sep + "autogrid4","-p", self.workdir + os.sep + "receptor.gpf","-l", self.workdir + os.sep + "autogrid_out.glg"]
		self._exec_command(commands)

	def prepare_DPF(self, dock_qty):
		commands = ["prepare_dpf4.py","-l",self.ligand_path,"-r", self.workdir + os.sep + "receptor.pdbqt","-p","ga_num_evals=2500000","-p","ga_run="+dock_qty,"-o","ligant_receptor.dpf"]
		self._run_ADT_command(commands)

	def run_autodock(self):
		commands = [self.AUTODOCKFOLDER + os.sep + "autodock4","-p", self.workdir + os.sep + "ligant_receptor.dpf","-l", self.workdir + os.sep + "ligant_receptor.dlg"]
		self._exec_command(commands)

	def compact_maps(self): #new
		zipObj = ZipFile(self.workdir + os.sep + "maps.zip", mode='w',
			compression=ZIP_DEFLATED)
		for eachFile in os.listdir(self.workdir):
			if eachFile.endswith(".map"):
				path = self.workdir + os.sep + eachFile
				zipObj.write(path, eachFile)
				os.remove(path)
		zipObj.close()
	def convert_mol2_to_pdbqt(self, mol2File):
		# print mol2File
		commands = ['prepare_ligand4.py', '-l', mol2File]
		self._run_ADT_command(commands)



	def _run_ADT_command(self, command_line):
		commands = [self.PYTHONSH] + [self.UTILITIESFOLDER + os.sep + command_line[0]] + command_line[1:]
		self._exec_command(commands)

	def _exec_command(self, command_line, stdin_data=""):
		receptor_path = os.path.dirname(self.receptor_path)
		realpath = os.getcwd()
		os.chdir(receptor_path)
		output_data = file(receptor_path + os.sep + "last_output.txt", "w")
		file_commands = file(receptor_path + os.sep + "commands.txt","a")
		file_commands.write(" ".join(command_line) + "\n")
		file_commands.close()

		process = subprocess.Popen(command_line, stdout=output_data, stderr=output_data, stdin=subprocess.PIPE, universal_newlines=True)
		process.stdin.write(stdin_data)
		watchdog = threading.Thread(target=self.watchdog_monitor, args=(process.pid,))
		watchdog.daemon=True
		watchdog.start()
		process.stdin.close()		
		output_data.close()
		os.chdir(realpath)
		if process.wait() != 0:
			raise LogException(" ".join(command_line),receptor_path + os.sep + "last_output.txt")

	def watchdog_monitor(self, pid):
		# print os.path.dirname(os.path.dirname(self.workdir))
		# if "//" in self.workdir:
		# 	workdir = self.workdir.split("//")[0]
		# else:
		# 	workdir = self.workdir
		while True:
			# print "laco"

			if os.path.exists(os.path.dirname(os.path.dirname(self.workdir)) + os.sep + 'venon.txt') or os.path.exists(self.workdir.split("//")[0] + os.sep + 'venon.txt'):
				# print "killing " + str(pid)
				try:
					os.kill(pid, 3)
					break
				except OSError:
					break
			if pid not in psutil.pids():
				break
			time.sleep(5)

	def generate_table(self):
			# post = get_object_or_404(Process, name=name)
			name = "None"
			list_of_compounds = []
			for eachFolder in os.listdir(self.workdir):
				path_name = self.workdir + os.sep + eachFolder
				if os.path.isdir(path_name) and eachFolder != "Logs":
					for eachFile in os.listdir(path_name):
						int_path = path_name + os.sep + eachFile

						if os.path.isdir(int_path):
							if os.path.exists(int_path + os.sep +  os.sep + "ligant_receptor.dlg"):


										dlg_file = int_path + os.sep + "ligant_receptor.dlg"
										list_of_folder_name =  dlg_file.split(os.sep)
										ligand_name = list_of_folder_name[-2]
										time_name = list_of_folder_name[-3]
										parser = DlgParser(dlg_file)
										mylist = {}
										for eachStruct in parser.clist:
											if 'inhib_constant_units' in eachStruct:
												if eachStruct['inhib_constant_units'] == 'uM':
													mag = math.pow(10,-6)
										 		elif eachStruct['inhib_constant_units'] == 'mM':
													mag = math.pow(10,-3)
												elif eachStruct['inhib_constant_units'] == 'nM':
													mag = math.pow(10,-9)
												elif eachStruct['inhib_constant_units'] == 'pM':
													mag = math.pow(10,-12)
												elif eachStruct['inhib_constant_units'] == 'fM':
													mag = math.pow(10,-15)
												elif eachStruct['inhib_constant_units'] == 'aM':
													mag = math.pow(10,-18)
												mylist[eachStruct['run']] = (eachStruct['inhib_constant']*mag,eachStruct['binding_energy'])
										better_Ki = math.pow(10,0)
										docking_id_better_Ki = None
										better_binding_energy = 100
										for eachElem in mylist:

											ki = mylist[eachElem][0]
											binding_energy = mylist[eachElem][1]

											if binding_energy <  better_binding_energy:
												docking_id_better_Ki = eachElem
												better_Ki = ki
												better_binding_energy = binding_energy
										if os.path.exists(os.path.dirname(int_path) + os.sep + os.path.basename(self.receptor_path) + "_em.pdb"):
											z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(self.receptor_path) + "_em.pdb"}
										else:
											z = {'ki': better_Ki, "conformation": time_name[0:-7], "compound": os.path.basename(int_path),   'select':True, 'experiment_id': name, 'pose_number' : docking_id_better_Ki, 'download_complex' : int_path, 'binding_energy': better_binding_energy, 'receptor' : os.path.dirname(int_path) + os.sep + os.path.basename(self.receptor_path)}
										list_of_compounds.append(z)

										compound_list = file(self.workdir + os.sep + "compound_list.csv", 'w')
										for eachCompound in list_of_compounds:
											compound_list.write(eachCompound["compound"] + "\t" + str(eachCompound["ki"]) + "\t" + 
											str(eachCompound["select"]) + "\t" +  eachCompound["experiment_id"] + "\t" +  str(eachCompound["pose_number"]) + "\t" + 
											eachCompound["download_complex"] + "\t" +  eachCompound["receptor"] + "\t" +  eachCompound["conformation"] + "\t" +  str(eachCompound["binding_energy"]) + "\n")


										compound_list.close()


	# def _exec_command(self, command_line, stdin_data=""):
	# 	protein_path = os.path.dirname(self.protein)
	# 	realpath = os.getcwd()
	# 	os.chdir(protein_path)
	# 	output_data = file(protein_path + os.sep + "last_output.txt", "w")
	# 	process = subprocess.Popen(command_line, stdout=output_data, stderr=output_data, stdin=subprocess.PIPE, universal_newlines=True)
	# 	process.stdin.write(stdin_data)
	# 	process.stdin.close()		
	# 	output_data.close()
	# 	os.chdir(realpath)
	# 	if process.wait() != 0:
	# 		raise LogException(" ".join(command_line),protein_path + os.sep + "last_output.txt")
