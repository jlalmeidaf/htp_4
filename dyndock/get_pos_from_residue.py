from Bio.PDB import PDBParser
import sys
def main():
	input_pdb = sys.argv[1]
	chain = sys.argv[3]
	if chain == "":
		chain = " "
	residue_number = int(sys.argv[2])
	structure = PDBParser(QUIET=True).get_structure(None, input_pdb)
	try:
		atom = structure[0][chain][residue_number]['CA']
	except KeyError:
		# raise e
		listOfChains = structure.get_list()[0].get_list()
		for eachChain in listOfChains:
			try:
				letterOfChain = eachChain.get_id()
				atom = structure[0][letterOfChain][residue_number]['CA']
				break
			except KeyError:
				pass

	
	residueX = atom.coord[0]
	residueY = atom.coord[1]
	residueZ = atom.coord[2]


	print str(residueX) + "," + str(residueY) + "," + str(residueZ)
if __name__ == '__main__':
	main()
